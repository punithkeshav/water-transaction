/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.AdHocNonComplianceIntervention_PageObjects;

/**
 *
 * @author smabe
 */
public class AdHocNonComplianceIntervention_PageObjects
{
    public static String Record_Number;
    public static String Window_1;

    public static String getWindow()
    {
        return Window_1;
    }

    public static void setWindow(String Window)
    {
        Window_1 = Window;
    }

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String recordSaved_popup_2()
    {
        return "(//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')])[2]";
    }

    public static String SearchOption()
    {
        return "//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String Text4(String text)
    {
        return "//a[contains(text(),'" + text + "')]";

    }

    public static String Text5(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";

    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String linkADoc_buttonxpath()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String ContainsTextBox()
    {
        return "//input[@class='txt border']";
    }

    public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String BusinessUnitDropDownOption(String text)
    {
        return "(//div[contains(@class,'transition visible')]//ul[@class='jstree-children']//a[text()='" + text + "'])[1]";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String SaveButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Save']";
    }

    public static String SaveButton2()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[1]";
    }

    public static String SaveButton3()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[2]";
    }

    public static String SaveButton4()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[text()='Save'])[1]";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String SupportingDocumentsTab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }
     public static String Check_box(String data)
    {
        return"//a[text()='"+data+"']//i[@class='jstree-icon jstree-checkbox']";
    }
     
      public static String AddButton()
    {
        return"//div[@class='active form transition visible']//div[@id='btnActAddNew']";
    }
     
     public static String AdHocNonComplianceInterventionTab()
    {
       return "//label[text()='Ad-Hoc Non-Compliance Intervention']";
    }

    public static String ProcessFlowButton()
    {
       return "//div[@id='btnProcessFlow_form_2D5AF480-CF73-4CC6-81CB-4EA140EBEE29']";
    }

    public static String Button_Save()
    {
        return"//div[@id='btnSave_form_2D5AF480-CF73-4CC6-81CB-4EA140EBEE29']";
    }

    public static String BusinessUnitDropDown()
     {
           return "//div[@id='control_935AB1D2-0C5A-4277-8ADD-790F414D76E6']//ul";
    }

    public static String FunctionalLocationDropDown()
    {
       return "//div[@id='control_FA1D6CD9-718A-49B5-80FE-D87F692FCDA9']//ul";
    }

    public static String AdHocNonComplianceInterventionAdd()
    {
        return"//div[@id='control_8EA5E8FA-14C0-485C-A236-A6954B5DA2D6']//div[@id='btnAddNew']";
    }

    public static String ProcessFlowButton2()
    {
        return"//div[@id='btnProcessFlow_form_BEF2B3D0-F159-4348-8E3B-8F0ABCFD006B']";
    }

    public static String DateOfNonCompliance()
    {
        return"//div[@id='control_E81D5D30-D0CF-4B4D-A45B-9D6A2759166A']//input";
    }

    public static String ReportedDateOfNonCompliance()
    {
       return "//div[@id='control_817B1336-983B-4C72-BAD5-304A992D37A1']//input";
    }

    public static String ReportedByDropDown()
    {
       return "//div[@id='control_7CC7802D-F601-479A-A368-EA7E27A2B502']//ul";
    }

    public static String NonComplianceInterventionDropDown()
    {
        return"//div[@id='control_8A50209C-3EDB-40C6-A225-6EB5DA2AE0DF']//ul";
    }

    public static String TypeOfNonComplianceDropDown()
    {
        return"//div[@id='control_C92CA60D-7027-4F48-B9B4-60ABFDA93A62']//ul";
    }

    public static String StoppageInitiatedDropDown()
    {
       return "//div[@id='control_FDBF77F4-AEAF-4F79-B2E2-7E4DB11A1DE5']//ul";
    }

    public static String WorkStoppageDropDown()
    {
       return "//div[@id='control_89EDFE37-2C85-45CD-9C73-338374164EF0']//ul";
    }

    public static String Description()
    {
       return "//div[@id='control_BFA70286-6BD9-4063-ABA8-82FE45B96A6A']//textarea";
    }

    public static String StopNoteClassificationDropDown()
    {
        return"//div[@id='control_0870BCBF-A0B7-40B2-A18F-D088477B890C']//ul";
    }

    public static String StoppageDueToDropDown()
    {
       return "//div[@id='control_F3E89F54-2B95-4D1F-9AB4-25A3A4B0BEBC']//ul";
    }

    public static String StoppageOutcomeDropDown()
    {
        return"//div[@id='control_AB93D552-65AF-4AB1-AB2B-612D4C2F78DF']//ul";
    }

    public static String Button_Save2()
    {
       return "//div[@id='btnSave_form_BEF2B3D0-F159-4348-8E3B-8F0ABCFD006B']";
    }

    public static String PersonResponsibleDropDown()
    {
       return "//div[@id='control_F36DD606-5017-4C7B-9E0E-B4F354F7E45B']//ul";
    }

    public static String DoesThisRelateToAPUEDropDown()
    {
        return"//div[@id='control_CF6DDDD8-9A6F-4A5A-BA6B-442F776941C2']//ul";
    }

    public static String PersonResponsibleForNonCompliancecheckbox()
    {
       return "//div[@id='control_6E56607C-4977-40A3-BCF9-FA9B34563FB4']//div[@class='c-chk']";
    }

    public static String PriorityUnwantedEventDropDown()
    {
        return"//div[@id='control_3D1C4A5D-23A7-4CAF-985C-764613D05A4B']//ul";
    }

    public static String DateWorkStoppageLifted()
    {
       return "//div[@id='control_808DB111-2965-4BA8-8CAD-00260501E7E1']//input";
    }

    public static String DateUpliftedByDMROffice()
    {
       return "//div[@id='control_5EEC9226-063D-4202-9DC2-0A19DFF92B39']//input";
    }

    public static String UnitsLost()
    {
       return "(//div[@id='control_83A2AFC6-308C-4F6D-8AA9-A1EB293EC2D7']//input)[1]";
    }

    public static String ProductiOnShiftsLost()
    {
       return "(//div[@id='control_252FBA07-27E0-4C60-8197-C331F5FABEAF']//input)[1]";
    }

    public static String CommitmentsMadeToDMR()
    {
        return"(//div[@id='control_35640193-B35F-4C51-B698-928DD263D278']//textarea)";
    }

    public static String InterventionActionsAdd()
    {
        return"//div[@id='control_65B0C328-45FB-4802-9B5C-5489519241C3']//div[@id='btnAddNew']";
    }

    public static String ActionsprocessFlow()
    {
        return"//div[@id='btnProcessFlow_form_564BEF7A-6AE9-42D3-9B42-B4EB7CC255BB']";
    }

    public static String TypeOfActionDropDown()
    {
        return"//div[@id='control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE']//ul";
    }

    public static String ActionDescription()
    {
       return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String EntityDropdown()
    {
       return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }

    public static String ResponsiblePersonDropDown()
    {
        return"//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }

    public static String ActionDueDate()
    {
        return"//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String Action_Save()
    {
       return "//div[@id='btnSave_form_564BEF7A-6AE9-42D3-9B42-B4EB7CC255BB']";
    }

    public static String NonComplianceInterventionRecord()
    {
       return "(//div[@id='control_8EA5E8FA-14C0-485C-A236-A6954B5DA2D6']//div[@id='grid']//tbody//tr)[1]";
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Biodiversity_Monitoring;

/**
 *
 * @author smabe
 */
public class Biodiversity_Monitoring
{

    public static String Record_Number;
    public static String Window_1;

    public static String getWindow()
    {
        return Window_1;
    }

    public static void setWindow(String Window)
    {
        Window_1 = Window;
    }

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }
     public static String recordSaved_popup1()
    {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])";
    }
     
      public static String DeleteButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Delete']";
    }

    public static String EnvironmentalSustainabilitLabel()
    {
        return "//label[text()='Environmental Sustainability']/..//i";
    }

    public static String businessUnitDropdown()
    {
        return "//div[@id='control_0A34F3A8-33FC-49F8-BC66-F024D376FBAD']//ul";
    }

    public static String businessUnitexpand(String text)
    {
        return "//a[text()='" + text + "']/../i";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String ContainsTextBox()
    {
        return "(//input[@class='txt border'])[1]";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String recordSaved_popup_2()
    {
        return "(//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')])[2]";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String linkADoc_buttonxpath()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String BusinessUnitDropDownOption(String text)
    {
        return "(//div[contains(@class,'transition visible')]//ul[@class='jstree-children']//a[text()='" + text + "'])[1]";
    }

    public static String BiodiversityMonitoringTab()
    {
       return "//label[text()='Biodiversity Monitoring']";
    }

    public static String AddButton()
    {
        return"//div[@class='active form transition visible']//div[@id='btnActAddNew']";
    }

    public static String processFlow()
    {
        return"//div[@id='btnProcessFlow_form_FDA4AB9B-C01C-46D8-8B28-32599E7EB20C']";
    }

    public static String saveButton()
    {
       return "//div[@id='btnSave_form_FDA4AB9B-C01C-46D8-8B28-32599E7EB20C']";
    }

    public static String MonitoringTypeDropDown()
    {
       return "//div[@id='control_36075F35-DAB1-4FC2-B2D1-AB14ADE295D7']//ul";
    }

    public static String MonthDropDown()
    {
       return "//div[@id='control_50C453F4-08EA-4260-A516-FB66F7A31B26']//ul";
    }

    public static String YearDropDown()
    {
       return "//div[@id='control_3D6D5440-22D5-49BD-8487-7DC13D7FD682']//ul";
    }

    public static String MonitoringPointDropDown()
    {
       return "//div[@id='control_F94557BC-A37F-43F9-8A40-3A11BE9D90A9']//ul";
    }

    public static String LinkToEnvironmentalPermitCheckBox()
    {
        return"//div[@id='control_A784C4B6-3584-4E40-B0A7-98AE843FAC08']//div[@class='c-chk']";
    }

    public static String LinkToProjectCheckBox()
    {
       return "//div[@id='control_2ADFA838-9686-4196-A907-EBC43CA3785C']//div[@class='c-chk']";
    }

    public static String LinkToProjectDropdown()
    {
       return "//div[@id='control_869F982F-5A16-4607-AD2E-10F456BDEDA6']//ul";
    }

    public static String LinkToEnvironmentalPermitDropdown()
    {
       return "//div[@id='control_1C8FE451-F691-4689-A2E8-2DA250B8CE06']//ul";
    }

   public static String BiodiversityMeasurements_Dropdown()
    {
       return "//span[text()='Biodiversity Measurements']";
    }

   public static String BiodiversityMeasurementsAdd_button()
    {
        return"//div[@id='control_C86D34B1-3385-49A3-B8B3-B315ADAB8D0C']//div[@id='btnAddNew']";
    }

   public static String Comments()
    {
       return "//div[@id='control_3E01B7D5-1805-4FDA-BFCC-FA4FA77F8F03']//textarea";
    }

   public static String MonitoringTakenByDropDown()
    {
        return"//div[@id='control_E4EB297F-7917-4077-BA32-C3E00CEAAC35']//ul";
    }

  public  static String MonitoringOn_Dropdown()
    {
        return"//div[@id='control_C6D5514D-338D-4D8C-BD01-10C1253885A5']//ul";
    }

  public  static String Parameter_Dropdown()
    {
       return "//div[@id='control_6DAF2B80-5DB9-45CA-86B2-9A25058F0F56']//ul";
    }

  public  static String MonitoringFindingsPanel()
    {
        return"//span[text()='Biodiversity Monitoring Findings']";
    }

  public  static String MonitoringFindingsAdd_button()
    {
       return "//div[@id='control_C86D34B1-3385-49A3-B8B3-B315ADAB8D0C']//div[@id='btnAddNew']";
    }

    public static String findingsAddButton()
    {
       return "//div[@id='control_4211AA24-7221-428C-9C92-2F54B2F6BE6B']//div[@id='btnAddNew']";
    }

    public static String findingsProcessflow()
    {
        return"//div[@id='btnProcessFlow_form_0E7CADBB-8D75-42EA-BB3A-2BD553AB2749']";
    }

    public static String Findings_desc()
    {
        return"//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }

    public static String Findings_owner_dropdown()
    {
        return"//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']//ul";
    }

    public static String Findings_class_dropdown()
    {
        return"//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']//ul";
    }

    public static String SaveButtonFindings()
    {
       return "//div[@id='btnSave_form_0E7CADBB-8D75-42EA-BB3A-2BD553AB2749']";
    }
    
     public static String SearchOption()
    {
        return"//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String RelocationPointDropDown()
    {
       return "//div[@id='control_2947BE29-3B0A-4F30-8C8D-6EE0EBCB9651']//ul";
    }

   

  
}

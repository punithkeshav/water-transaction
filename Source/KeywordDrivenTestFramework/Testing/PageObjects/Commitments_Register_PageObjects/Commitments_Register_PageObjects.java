/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Commitments_Register_PageObjects;

/**
 *
 * @author smabe
 */
public class Commitments_Register_PageObjects
{

    public static String Record_Number;

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String linkToADocument()
    {
        return "//div[@id='control_21DCCD54-4136-49E3-899C-EAE0E7F7B5C8']//b[@original-title='Link to a document']";
    }
    
    public static String linkToADocument1()
    {
        return "//div[@id='control_12812464-3714-483D-9522-5A1AF9F90CFE']//b[@original-title='Link to a document']";
    }
    
    public static String linkToADocument2()
    {
        return "//div[@id='control_2E0EC5DF-FA18-4D8A-A190-C3E6F77A5279']//b[@original-title='Link to a document']";
    }

    public static String SupportingDocumentsTab()
    {
        return "//li[@id='tab_6BA55006-A40C-4CF5-8608-A167C083A40F']//div[text()='Supporting Documents']";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }
    
    public static String recordSaved_popup_1()
    {
        return "(//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')])[2]";
    }

    public static String linkADoc_Add_button()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String MaskBlock()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String MaskNone()
    {
        return "//div[@class='ui inverted dimmer']";
    }

    public static String SearchOption()
    {
        return "//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String measurementsPanel()
    {
        return "//span[text()='Measurements']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String ContainsTextBox()
    {
        return "(//input[@class='txt border'])[1]";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String BusinessUnitDropDownOption2(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String Text5(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }
//

    public static String searchBtn()
    {
        return  "//div[@id='btnActApplyFilter']";
    }
    
    public static String Button_Save()
    {
        return "//div[@id='btnSave_form_DCB70D8A-B79E-4968-9607-29115B4FCDC2']";
    }
    
    public static String delete_btn()
    {
        return "//div[@id='btnDelete_form_7D7FA005-B236-4D95-B304-726C81824281']";
    }
    
    public static String btn_confirmYes()
    {
        return "//div[@class='confirm-popup popup']//div[@title='Yes']";
    }
    
    public static String confirm_OK()
    {
        return "//div[@id='btnHideAlert']";
    }
    
//
    public static String search_Btn()
    {
        return "//div[@id='btnActApplyFilter']";
    }
    
    public static String Text(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String Text4(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[contains(text(),'" + text + "')]";
        //div[contains(@class,'transition visible')]//a[contains(text(),'Environmental Impact')]
    }

    public static String linkToADocument_2()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String UrlInput_TextArea()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextArea()
    {
        return "//input[@id='urlTitle']";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String ReasonsForReviewInclude_CheckBox(String data)
    {
        return "(//a[text()='" + data + "']//i)[1]";
    }

    public static String formview()
    {
        return "//i[@class='icon edit on paper icon ten six grid-icon-edit grid-icon-edit-active']";
    }

    public static String commitments_registerTab()
    {
        return "//div[@id='section_a177662e-d9d9-42c1-92ce-d9a507079951']";
    }
    
    public static String commitmentsRegister_addBtn()
    {
        return "//div[@id='btnActAddNew']";
    }
    
    public static String commitmentsRegister_searchBtn()
    {
        return "//div[@id='btnActApplyFilter']";
    }
    
    public static String lastResults_pageBtn()
    {
        return "//a[@title='Go to the last page']";
    }
    
    public static String commitmentsRegister_recordSelection()
    {
        return "//div[@title='Tim Tester 1']";
    }
    
    public static String commitmentsRegister_pFlowBtn()
    {
        return "//div[@id='btnProcessFlow_form_DCB70D8A-B79E-4968-9607-29115B4FCDC2']";
    }
    
    public static String businessUnit_dd()
    {
        return "//div[@id='control_21A07758-2945-467A-92CF-F571AE83FEFA']//ul";
    }
    
    public static String businessUnit_ddSearch()
    {
        return "/html/body/div[1]/div[3]/div/div[2]/div[4]/div[1]/input";
    }
    
    public static String businessUnit_ddTree1()
    {
        return "//li[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c']/i";
    }
    
    public static String businessUnit_ddTree2()
    {
        return "//li[@id='b7710db3-0f46-4d9e-a5a8-725640256348']/i";
    }
    
    public static String businessUnit_ddTree3()
    {
        return "//li[@id='81882223-cc16-41f0-877d-493ba9741689']/i";
    }
    
    public static String businessUnit_ddTree4()
    {
        return "//li[@id='5765410a-6b85-4388-8d67-acf35f245139']/i";
    }
    
    public static String businessUnit_selection()
    {
        return "//a[@id='9bc2a04c-d086-4575-b099-2e421702ca31_anchor']";
    }
    
    public static String funcLocation_dd()
    {
        return "//div[@id='control_D3AF1650-B305-44CF-8463-1B003793EB72']//ul";
    }
    
    public static String funcLocation_selection()
    {
        return "//a[@id='0f80e930-e7f5-4fc3-b157-552ff5dbf0da_anchor']";
    }
    
    public static String commitRegisterTitle_input()
    {
        return "//div[@id='control_804F813A-B603-4ED8-A416-3571D2146714']//input";
    }
    
    public static String commitRegisterOwner_dd()
    {
        return "//div[@id='control_189B6C8F-E360-4339-A9B7-BF2C517A69E0']//ul";
    }
    
    public static String commitRegisterOwner_ddSearch()
    {
        return "/html/body/div[1]/div[3]/div/div[2]/div[7]/div[1]/input";
    }
    
    public static String commitRegisterOwner_selection()
    {
        return "//a[@id='b8ad5f90-d582-46c4-b186-d99649824acd_anchor']";
    }
    
    public static String relatedPermits_dd()
    {
        return "//div[@id='control_B0A1AA18-B21E-4AF6-B521-9B93DF06530F']//ul";
    }
    
    public static String relatedPermits_selection()
    {
        return "//a[@id='abbca0c5-c9d6-45d9-bc04-4a150e65cc5c_anchor']/i[1]";
    }
    
    public static String linkedCommitments_tab()
    {
        return "//li[@id='tab_6ED1A6C4-370E-48A1-AED9-B7EDD864F2C5']/div[1]";
    }
    
    public static String commitments_tab()
    {
        return "//li[@id='tab_6C9A58EA-1C99-4D55-BC4A-9FBCDD833A0F']/div[1]";
    }

    public static String commitment_addBtn()
    {
        return "//div[@id='btnAddNew']";
    }
    
    public static String CondiCommitment_pFlow()
    {
        return "//div[@id='btnProcessFlow_form_7C062343-94C0-4691-9D16-7E07735C641C']";
    }
    
    public static String CondiCommitment_categoryDd()
    {
        return "//div[@id='control_052B3A60-874C-4F7A-88EA-E2019BA50262']//ul";
    }
    
    public static String CondiCommitment_categoryDdSelection()
    {
        return "//a[@id='8c16865e-ed88-4b0e-bc92-baa1a299b5ad_anchor']";
    }
    
    public static String CondiCommitment_titleInput()
    {
        return "//div[@id='control_06D6617B-3C38-4008-A3E5-00043F9E3202']//input";
    }
    
    public static String CondiCommitment_descriptionInput()
    {
        return "//div[@id='control_B0946509-4F5B-40C9-AB6E-82561AB2D23B']//textarea";
    }
    
    public static String CondiCommitment_natureDd()
    {
        return "//div[@id='control_77A36D3E-D562-4D3E-9363-0B77E0D94DAF']//ul";
    }
    
    public static String CondiCommitment_natureDdSelect()
    {
        return "//a[@id='d80b1107-424d-45fc-8282-30695aa91dbd_anchor']";
    }
    
    public static String CondiCommitment_dateSetInput()
    {
        return "//div[@id='control_23CDBFD5-0F35-46D5-8015-31D3E2DB422C']//input";
    }
    
    public static String onceOffOrPerm_dd()
    {
        return "//div[@id='control_5F236875-4D6D-4334-A15A-5D080B00DA3D']//li";
    }
    
    public static String onceOffOrPerm_ddSelection()
    {
        return "//a[@id='cc8d21b7-5b1d-4fcf-b760-cbd686d1be3c_anchor']";
    }
    
    public static String originalCompDate_input()
    {
        return "//div[@id='control_52D20E4C-B2A8-4FB9-A7E8-7339AE323EC2']//input";
    }
    
    public static String expectedCompDate_input()
    {
        return "//div[@id='control_D13613DC-B308-4ABE-B939-3DFBA746BEF8']//input";
    }
    
    public static String daysSinceCommit_input()
    {
        return "//div[@id='control_049D830F-8A6B-4B58-AD2B-775D666460F1']//input";
    }
    
    public static String condiCommit_OwnerDd()
    {
        return "//div[@id='control_AED68E6D-3039-4916-8599-E42B7A7C9EE1']//li";
    }
    
    public static String condiCommit_OwnerDdSearch()
    {
        return "/html/body/div[1]/div[3]/div/div[2]/div[27]/div[1]/input";
    }
    
    public static String condiCommit_OwnerDdSelection()
    {
        return "//a[@id='b8ad5f90-d582-46c4-b186-d99649824acd_anchor']";
    }
    
    public static String condiCommit_approverDd()
    {
        return "//div[@id='control_B5A54D2B-72E4-4C3D-AFC8-E6DFE3029AAE']//ul";
    }
    
    public static String condiCommit_approverDdSearch()
    {
        return "/html/body/div[1]/div[3]/div/div[2]/div[28]/div[1]/input";
    }
    
    public static String condiCommit_approverDdSelection()
    {
        return "//a[@id='2b5f9582-d365-46fa-90ca-4d0c975d0dcc_anchor']";
    }
    
    public static String progress_dd()
    {
        return "//div[@id='control_04EB8BA9-C8F2-403D-8CA5-461C0389C3FF']//li[@id='654f3aca-90ae-4b27-ba5b-94919aaba1cb']";
    }
    
    public static String progress_ddSelection()
    {
        return "//a[@id='93fb3f35-d453-4f6b-98ba-4338d7d64aa0_anchor']";
    }
    
    public static String condiCommit_saveBtn()
    {
        return "//div[@id='btnSave_form_7C062343-94C0-4691-9D16-7E07735C641C']";
    }
    
    public static String commitments_deleteBtn()
    {
        return "//div[@id='btnDelete_form_DCB70D8A-B79E-4968-9607-29115B4FCDC2']";
    }
}

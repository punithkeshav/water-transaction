/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.ComplianceAssessment_PageObjects;

/**
 *
 * @author smabe
 */
public class ComplianceAssessment_PageObjects
{
     public static String Record_Number;
    public static String Window_1;

    public static String getWindow()
    {
        return Window_1;
    }

    public static void setWindow(String Window)
    {
        Window_1 = Window;
    }

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String recordSaved_popup_2()
    {
        return "(//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')])[2]";
    }

    public static String SearchOption()
    {
        return "//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String Text4(String text)
    {
        return "//a[contains(text(),'" + text + "')]";

    }

    public static String Text5(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";

    }
    
      public static String Text6(String text)
    {
        return "(//li[@title='"+text+"']//a[text()='"+text+"'])[2]";

    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String linkADoc_buttonxpath()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String ContainsTextBox()
    {
        return "//input[@class='txt border']";
    }

    public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String BusinessUnitDropDownOption(String text)
    {
        return "(//div[contains(@class,'transition visible')]//ul[@class='jstree-children']//a[text()='" + text + "'])[1]";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String SaveButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Save']";
    }

    public static String SaveButton2()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[1]";
    }

    public static String SaveButton3()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[2]";
    }

    public static String SaveButton4()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[text()='Save'])[1]";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String SupportingDocumentsTab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String ComplianceAssessmentLabel()
    {
        return"//label[text()='Compliance Assessment']";
    }

    public static String AddButton()
    {
       return "//div[@id='btnActAddNew']";
    }

    public static String processFlow()
    {
       return "//div[@id='btnProcessFlow_form_37462362-6F37-4C31-9AF6-3C08C76624D5']";
    }

    public static String businessUnitDropdown()
    {
       return "//div[@id='control_2862357D-9B00-4C8D-B8BF-D3559597D090']//ul";
    }

    public static String AssessmentConductedByDropdown()
    {
       return "//div[@id='control_A0380CE5-DD5B-43DB-92B3-439F5FF846AB']//ul";
    }

    public static String AssessmentDate()
    {
       return "//div[@id='control_D2DE76A1-BFDA-4012-A762-D6F1016DAED4']//input";
    }

    public static String ApplicableConditionsAndCommitmentsTab()
    {
        return"//div[text()='Applicable Conditions and Commitments']";
    }

    public static String ConditionsAndCommitmentsAddButton()
    {
        return"//div[@id='control_F8D2B32B-EDCE-44AB-BFC9-E7554B6658BB']//div[@id='btnAddNew']";
    }

    public static String ConditionsAndCommitmentsProcessFlow()
    {
        return"//div[@id='btnProcessFlow_form_C31131AD-DC8B-4750-8F01-3656548E1A75']";
    }

    public static String TitleDropdown()
    {
        return"//div[@id='control_FC9F0C32-1FE3-4FD7-B436-73CECDBB6DAB']//ul";
    }

    public static String findingsAddButton()
    {
        return"//div[@id='control_781DE377-85AC-44EC-829E-4BE2AD5AD082']//div[@id='btnAddNew']";
    }

    public static String findingsProcessflow()
    {
       return "//div[@id='btnProcessFlow_form_C5F26152-30ED-47D5-9261-3CFCDDF0E911']";
    }

    public static String Findings_desc()
    {
       return"//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }

    public static String Findings_owner_dropdown()
    {
       return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']//ul";
    }

    public static String Findings_class_dropdown()
    {
        return"//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']//ul";
    }

    public static String SaveButtonFindings()
    {
        return"//div[@id='btnSave_form_C5F26152-30ED-47D5-9261-3CFCDDF0E911']";
    }
    
}

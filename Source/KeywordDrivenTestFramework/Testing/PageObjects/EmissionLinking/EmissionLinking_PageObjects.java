/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.EmissionLinking;

/**
 *
 * @author smabe
 */
public class EmissionLinking_PageObjects
{

    public static String Record_Number;
    public static String Window_1;

    public static String getWindow()
    {
        return Window_1;
    }

    public static void setWindow(String Window)
    {
        Window_1 = Window;
    }

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String recordSaved_popup_2()
    {
        return "(//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')])[2]";
    }

    public static String SearchOption()
    {
        return "//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String Text4(String text)
    {
        return "//a[contains(text(),'" + text + "')]";

    }

    public static String Text5(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";

    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String linkADoc_buttonxpath()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String ContainsTextBox()
    {
        return "//input[@class='txt border']";
    }

    public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String BusinessUnitDropDownOption(String text)
    {
        return "(//div[contains(@class,'transition visible')]//ul[@class='jstree-children']//a[text()='" + text + "'])[1]";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String SaveButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Save']";
    }

    public static String SaveButton2()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[1]";
    }

    public static String SaveButton3()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[2]";
    }

    public static String SaveButton4()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[text()='Save'])[1]";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String SupportingDocumentsTab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String EnvironmentalSustainabilitLabel()
    {
        return "//label[text()='Environmental Sustainability']/..//i";
    }

    public static String ECO2ManLabel()
    {
        return "//label[text()='ECO2Man']";
    }

    public static String MonitoringMaintence()
    {
        return "//div[@original-title='Monitoring Maintenance']";
    }

    public static String AddButton()
    {
        return"//div[contains(@class,'active form transition visible')]//div[@id='btnActAddNew']";
    }

    public static String processFlow()
    {
       return "//div[@id='btnProcessFlow_form_8D1C41B6-81BE-4765-B7A7-AC44E3D9DA98']";
    }

    public static String businessUnitDropdown()
    {
        return"//div[@id='control_D16E76B1-4B04-48EF-BF7C-BCD73D729EE1']";
    }
    
     public static String EmissionFactorDatabaseDropDown()
    {
        return"//div[@id='control_7D8DD5E3-FAAC-4D39-8EB7-DE88C50D49DB']";
    }

    public static String TableRecord()
    {
       return "(//div[@id='control_7937D760-9402-4C09-896B-83C7ACC6881C']//table//tbody)[2]//tr[1]";
    }

    public static String RecordprocessFlow()
    {
        return"//div[@id='btnProcessFlow_form_5EC57CD7-AC03-4C01-8885-9C5392F189FA']";
    }

    public static String CO2eFactorUnitDropDown()
    {
        return"//div[@id='control_6658AAC9-2F62-4AB7-AE78-B917650C6C10']";
    }

    public static String YearOfComplianceDropdown()
    {
       return "//div[@id='control_8455965D-CEE6-412E-ABD6-EA623E308E38']";
    }

    public static String EmissionLinkingValuesAddButton()
    {
       return "//div[@id='btnAddNew']";
    }

    public static String EmissionLinking()
    {
       return "//label[text()='Emission Linking']";
    }

    public static String DeleteButton()
    {
      return  "//div[@id='btnDelete_form_8D1C41B6-81BE-4765-B7A7-AC44E3D9DA98']";
    }

    public static String CarbonPrice()
    {
        return"//label[text()='Carbon Price']";
    }

    public static String EmissionSourceDropdown()
    {
       return "//div[@id='control_CF972741-28BF-436F-B4ED-CAB608060B06']//ul";
    }

}

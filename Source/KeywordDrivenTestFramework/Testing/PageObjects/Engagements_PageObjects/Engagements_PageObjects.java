/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Engagements_PageObjects;

/**
 *
 * @author smabe
 */
public class Engagements_PageObjects
{
    
     public static String Record_Number;
    public static String Window_1;

    public static String getWindow()
    {
        return Window_1;
    }

    public static void setWindow(String Window)
    {
        Window_1 = Window;
    }

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String recordSaved_popup_2()
    {
        return "(//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')])[2]";
    }

    public static String SearchOption()
    {
        return "//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String Text4(String text)
    {
        return "//a[contains(text(),'" + text + "')]";

    }

    public static String Text5(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";

    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String linkADoc_buttonxpath()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String ContainsTextBox()
    {
        return "//input[@class='txt border']";
    }

    public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String BusinessUnitDropDownOption(String text)
    {
        return "(//div[contains(@class,'transition visible')]//ul[@class='jstree-children']//a[text()='" + text + "'])[1]";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String SaveButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Save']";
    }

    public static String SaveButton2()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[1]";
    }

    public static String SaveButton3()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[2]";
    }

    public static String SaveButton4()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[text()='Save'])[1]";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String SupportingDocumentsTab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String EventManagemantTab()
    {
       return "//label[text()='Engagement Management']";
    }
     public static String AddButton()
    {
        return"//div[@class='active form transition visible']//div[@id='btnActAddNew']";
    }

    public static String ProcessFlowButton()
    {
        return"//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

    public static String EngagementDescription()
    {
        return"//div[@id='control_1C19AE65-23A1-4ADC-A631-D9273FC0CE9F']//textarea";
    }

    public static String EngagementTitle()
    {
       return "(//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input)[1]";
    }

    public static String EngagementDate()
    {
       return "(//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input)[1]";
    }

    public static String BusinessUnitDropDown()
    {
        return"//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//ul";
    }

    public static String LinkToProjectCheckBox()
    {
       return "//div[@id='control_29AB36D5-E83F-43EF-AFF5-F7353A5353E9']//div[@class='c-chk']";
    }

    public static String ProjectCheckBox(String data)
    {
        return"//a[text()='"+data+"']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String ProjectDropDown()
    {
        return"//div[@id='control_963F5190-1317-42C1-AD7A-B277FCBA7101']//ul";
    }

    public static String ConfidentialCheckBox()
    {
        return"//div[@id='control_C108E1A8-9B60-4E8B-B85A-08E47E5C6A7D']//div[@class='c-chk']//div";
    }

    public static String ResponsiblePersonDropDown()
    {
        return"//div[@id='control_213251A2-010A-4BBF-A65A-A1FC8C6F7033']//ul";
    }
    
     public static String ActionsResponsiblePersonDropDown()
    {
        return"//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }

    public static String MethodOfEngagementDropDown()
    {
        return"//div[@id='control_4A471537-8229-4E54-A86C-DCEB99BA24D0']//li";
    }

    public static String FunctionOfEngagementDropDown()
    {
        return"//div[@id='control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB']//ul";
    }

    public static String Check_box(String data)
    {
        return"//a[text()='"+data+"']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String ContactInquiryTopicDropDown()
    {
       return "//div[@id='control_2D1B5E8D-BBF2-448A-9765-F03FA8C31019']//ul";
    }

    public static String Button_Save()
    {
       return "//div[@id='btnSave_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

    public static String GeographicLocationDropDown()
    {
        return"//div[@id='control_F703A144-D0B6-4D4D-B5E2-D4E186427A43']//ul";
    }

    public static String AdHocNonComplianceInterventionTab()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static String AttendeesTab()
    {
        return"//div[text()='Attendees']";
    }

    public static String TotalnonListedAttendees()
    {
        return"(//div[@id='control_8A28B08F-2857-4EB7-833E-3C7929A500B7']//input)[1]";
    }
    
     public static String EventTitle()
    {
        return "//div[@id='control_E9C32F4B-AB3C-4ABB-B8E6-E5D5E34F139B']//input";
    }

    public static String EventDescription()
    {
        return "//div[@id='control_E887FEB2-18C9-444E-A023-B05D5F08BC28']//textarea";
    }

    public static String FunctionalLocationDropDown()
    {
        return "//div[@id='control_C492928D-54AF-49E5-B173-A427BC1A2BE5']";
    }

    public static String SpecificLocation()
    {
        return "//div[@id='control_0E9F8B1D-C36C-4999-9185-A5658E72A9DB']//input";
    }

    public static String PinToMap()
    {
        return "//div[@id='control_82B50C4C-22B2-46DB-8F8D-677EC2CA7D33']";
    }

    public static String DateOfEvent()
    {
        return "//div[@id='control_A68454D1-B0FB-4EB7-B861-2AF37ACAC8DF']//input";
    }

    public static String DateReported()
    {
        return "//div[@id='control_991FEA22-9C36-4EC7-B385-744259EB6599']//input";
    }

    public static String TimeOfEvent()
    {
        return "//div[@id='control_2CE711B1-4A48-4381-916B-17F5FD255123']//input";
    }

    public static String TimeReported()
    {
        return "//div[@id='control_B623B7E6-DB94-4A72-933F-4847D48066D5']//input";
    }

    public static String ImmediateActionTaken()
    {
        return "//div[@id='control_58E573F1-1421-4B47-B9F9-68668A0AA36A']//textarea";
    }

    public static String ReportedByDropDown()
    {
        return "//div[@id='control_F2A12EB0-344C-47E6-BB85-B74B9B53CEFB']//li";
    }
    
       public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String Verification_Additional_Detail_Tab()
    {
        return "//div[text()='2.Verification & Additional Detail']";
    }

    public static String WhatHappenDropDown()
    {

        return "//div[@id='control_430B3444-0F67-42BB-A9CA-C17E14AE7448']//ul";
    }

    public static String HowDidItHappenDropDown()
    {
        return "//div[@id='control_58A81103-5F88-4AAF-8783-54D3BACD3403']//ul";
    }

    public static String ComplaintAnonymousDropDown()
    {
        return "//div[@id='control_E1D4CAF6-9C41-4F91-A2F0-2B606FEA59F4']//ul";
    }

    public static String PotentialConsequencesDropDown()
    {
        return "//div[@id='control_1397EF94-CA3A-477F-A6EF-F8C59398559A']//ul";
    }

    public static String PotentialConsequencesSelectAll()
    {
        return "//div[@id='control_1397EF94-CA3A-477F-A6EF-F8C59398559A']//b[@class='select3-all']";
    }

    public static String WhatHappenedToCauseTheHazardDropDown()
    {
        return "//div[@id='control_7D3026FE-8849-4A8D-87D9-0444E8203766']//ul";
    }

    public static String HazardTypeDropDown()
    {
        return "//div[@id='control_A5E685BD-32AF-448F-86B9-213D8095D0C4']//ul";
    }

    public static String RiskControlledDropDown()
    {
        return "//div[@id='control_C7EBA116-4565-4CF0-93DC-33B828686A59']//ul";
    }

    public static String WhatWasDoneToControlTheHazard()
    {
        return "//div[@id='control_1DCE3D07-28C8-4E56-BC03-41C3780E5278']//textarea";
    }
    
      public static String LinkToSite()
    {
        return "//div[@id='control_198375A9-1B33-46D1-A93F-180F33F21D25']";
    }

    public static String LinkToSiteDropDown()
    {
        return "//div[@id='control_3544E0CE-09D7-408E-8CB9-EC387D9B3675']//li";
    }

    public static String LinkToProjects()
    {
        return "//div[@id='control_26DC542D-8D63-4388-B320-03B9C9ED7C88']";
    }

    public static String LinkToProjectsDropDown()
    {
        return "//div[@id='control_1D125078-AF69-492D-887E-0BE99AF4D528']//li";
    }
    
      public static String Specific_location()
    {
        return"//div[@id='control_0E9F8B1D-C36C-4999-9185-A5658E72A9DB']//input";
    }
      
        public static String TypeofEventDropDown()
    {
        return "//div[@id='control_9C6B5F1B-AF54-4B1D-BF12-0C076D64A87F']";
    }
        
          public static String businessUnitDropDown()
    {
        return "//div[@id='control_8FC01D3C-93D7-4570-BAEA-765533CDCB75']";
    }

    public static String ResponsibleSupervisorDropDown()
    {
        return "//div[@id='control_4DC8AFD7-836E-4681-9FB8-99FAF564054C']//li";
    }
      public static String ValidatorDropDown()
    {
        return "//div[@id='control_783CFD1D-ACE7-4A2B-B0CE-7B6883272992']//li";
    }
      
        public static String OtherDescription()
    {

        return "//div[@id='control_00545A60-F93C-46B0-973E-62C056E887C4']//textarea";
    }
        
          public static String InterimDropDown()
    {
        return "//div[@id='control_F9FB9C47-6255-4615-BE88-4813D5F46BF2']//ul";
    }

    public static String TeamAttendeesAdd()
    {
       return "//div[@id='control_5D1D4343-6774-40DF-AAA5-4BE09A85DDCE']//div[@id='btnAddNew']";
    }

    public static String TeamAttendeesDropDown()
    {
       return "(//div[@id='control_B5CF2925-9A5E-4F93-A5D4-462B76CFEBF9'])//ul";
    }

    public static String IndividualTab()
    {
       return "//div[text()='Individuals']";
    }

    public static String IndividualAttendeesDropDown()
    {
        return"//div[@id='control_DA9B0184-991D-4267-B01B-36EF4792AF67']//ul";
    }

    public static String IndividualAttendeesAdd()
    {
        return"//div[@id='control_2E2CA02B-9570-4DC8-89A7-8EE214D2F06E']//div[@id='btnAddNew']";
    }

    public static String GroupTab()
    {
        return"(//div[text()='Group Attendees'])[1]";
    }

    public static String GroupAttendeesAdd()
    {
        return"//div[@id='control_FBBF4EE8-0B0B-43B6-9B13-642704FDA74B']//div[@id='btnAddNew']";
    }

    public static String GroupAttendeesDropDown()
    {
        return"//div[@id='control_BCCAA4BD-BD02-428C-A1E3-3BFE31AF66BF']//ul";
    }

    public static String ActionsAdd()
    {
        return"//div[@id='control_5C3A47DC-4956-4D6E-BF97-A3967E24667B']//div[@id='btnAddNew']";
    }

    public static String ActionsprocessFlow()
    {
        return"//div[@id='btnProcessFlow_form_5110F491-CFCA-42F7-87A2-001109988E70']";
    }

    public static String TypeOfActionDropDown()
    {
        return"//div[@id='control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE']//ul";
    }

    public static String ActionDescription()
    {
       return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String EntityDropdown()
    {
        return"//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }

    public static String ActionDueDate()
    {
       return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String Actions_Tab()
    {
        return"//div[@class='tabpanel_move_content']//div[text()='Actions']";
    }

    public static String ActionButton_Save()
    {
        return"//div[@id='btnSave_form_5110F491-CFCA-42F7-87A2-001109988E70']";
    }

    public static String EngagementStatusDropDown()
    {
        return"//div[@id='control_C072FF42-2D88-4B84-866B-FE7E9436460F']//ul";
    }

    public static String Reports()
    {
       return "//div[@id='btnReports_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

    public static String EngagementReport()
    {
        return"(//span[text()='Engagement Report'])[2]";
    }
    
      public static String continueButton()
    {
        return "//div[@title='CONTINUE']";
    }

    public static String LinkedGrievancesDropDown()
    {
        return"//div[@id='control_ADF3F830-49E4-47FE-AB04-75C0890DA968']//ul";
    }

    public static String LinkedCommitmentsDropDown()
    {
       return "//div[@id='control_1E01C557-92EF-43AB-956D-3F00669EB13F']//ul";
    }

    public static String RelatedPermitsDropDown()
    {
        return"//div[@id='control_F5863D2C-E246-40FA-8715-A0BFC670CEC6']//ul";
    }

    public static String GrievanceAddButton()
    {
        return"//div[text()='Add new grievance']";
    }
    
     public static String ReportedByCheckBox()
    {
        return "//div[@id='control_0F48D804-ECD1-4700-B480-72338CD49E6F']";
    }

    public static String KeyPersonInvolved()
    {
        return "//div[@id='control_1D3E03CD-D399-4FEB-8EEE-839857F408EA']";
    }

    public static String ExternalPartiesInvolved()
    {
        return "//div[@id='control_D086CF05-958D-4916-ADC3-BD45703467A5']";
    }

    public static String WasEquipmentInvolved()
    {
        return "//div[@id='control_811EB188-F2C5-4BD7-A01C-85A65C04F355']";
    }

    public static String WasEquipmentInvolvedDropDownCheckBox(String data)
    {
        return "(//a[text()='" + data + "']/..//a//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String StoppageDueToDropDownCheckBox(String data)
    {
        return "(//a[text()='" + data + "']/..//a//i[@class='jstree-icon jstree-checkbox'])";
    }

    public static String WasEquipmentInvolvedDropDown()
    {
        return "//div[@id='control_69BDF7EA-72B5-420E-B8CF-86D5DB6DF657']//ul";
    }

    public static String WasEquipmentInvolvedSelectAll()
    {
        return "//div[@id='control_69BDF7EA-72B5-420E-B8CF-86D5DB6DF657']//b[@class='select3-all']";
    }

    public static String KeyPersonInvolvedDropDown()
    {
        return "//div[@id='control_75D58974-F17E-49F1-9A75-ACC9F66F161B']//ul";
    }

    public static String KeyPersonInvolvedSelectAll()
    {
        return "//div[@id='control_75D58974-F17E-49F1-9A75-ACC9F66F161B']//b[@class='select3-all']";
    }

    public static String ExternalPartiesInvolvedSelectAll()
    {
        return "//div[@id='control_12497702-FCCB-47A2-9267-BE9A324421EC']//b[@class='select3-all']";
    }

    public static String ExternalPartiesInvolvedDropDown()
    {
        return "//div[@id='control_12497702-FCCB-47A2-9267-BE9A324421EC']//ul";

    }

    public static String UploadImage()
    {
        return "//div[@id='control_E39283CD-AF7A-4775-B1D7-8CE676E49D65']//b[@original-title='Upload an image']";
    }

    public static String EventProcessFlowButton()
    {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String Event_Save()
    {
       return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String CommitmentAddButton()
    {
        return"//div[text()='Add new commitment']";
    }

    public static String CommitmentProcessFlowButton()
    {
        return"//div[@id='btnProcessFlow_form_DCB70D8A-B79E-4968-9607-29115B4FCDC2']";
    }

    public static String CommitmentRegisterTitle()
    {
       return "(//div[@id='control_804F813A-B603-4ED8-A416-3571D2146714']//input)[1]";
    }

    public static String CommitmentRegisterOwner()
    {
        return"(//div[@id='control_189B6C8F-E360-4339-A9B7-BF2C517A69E0']//ul)";
    }

    public static String CommitmentFunctionalLocationDropDown()
    {
       return "(//div[@id='control_D3AF1650-B305-44CF-8463-1B003793EB72']//ul)";
    }

    public static String Commitment_Save()
    {
        return"//div[@id='btnSave_form_DCB70D8A-B79E-4968-9607-29115B4FCDC2']";
    }

    public static String CommitmentBusinessUnitDropDown()
    {
        return"//div[@id='control_21A07758-2945-467A-92CF-F571AE83FEFA']//ul";
    }

    public static String PermitAddButton()
    {
       return "//div[text()='Add new permit']";
    }

    public static String PermitProcessFlowButton()
    {
       return "//div[@id='btnProcessFlow_form_06A92B5E-4170-473C-B9F9-CFF06AB14C74']";
    }

    public static String PermitBusinessUnitDropDown()
    {
       return "//div[@id='control_5614586C-E586-4742-9423-A40BFA7F4E1B']//ul";
    }

    public static String CommodityDropDown()
    {
        return"//div[@id='control_6DAED832-BAB3-44D2-9B27-AFFBCD771B7F']//ul";
    }

    public static String Permit_Save()
    {
       return "//div[@id='btnSave_form_06A92B5E-4170-473C-B9F9-CFF06AB14C74']";
    }
    
}

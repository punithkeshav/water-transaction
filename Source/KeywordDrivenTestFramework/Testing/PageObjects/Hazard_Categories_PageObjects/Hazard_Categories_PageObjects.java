/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Hazard_Categories_PageObjects;

/**
 *
 * @author smabe
 */
public class Hazard_Categories_PageObjects
{

    public static String Record_Number;

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String linkToADocument()
    {
        return "//div[@id='control_FE252726-8FFC-4A2B-841E-48697ED8E5B8']//b[@original-title='Link to a document']";
    }
    
    public static String linkToADocument1()
    {
        return "//div[@id='control_12812464-3714-483D-9522-5A1AF9F90CFE']//b[@original-title='Link to a document']";
    }
    
    public static String linkToADocument2()
    {
        return "//div[@id='control_2E0EC5DF-FA18-4D8A-A190-C3E6F77A5279']//b[@original-title='Link to a document']";
    }

    public static String SupportingDocumentsTab()
    {
        return "//li[@id='tab_56C86DEE-96E7-42E0-8FF2-B3EA105AA3C5']//div[text()='Supporting Documents']";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }
    
    public static String recordSaved_popup_1()
    {
        return "(//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')])[2]";
    }

    public static String linkADoc_Add_button()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String MaskBlock()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String MaskNone()
    {
        return "//div[@class='ui inverted dimmer']";
    }

    public static String SearchOption()
    {
        return "//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String measurementsPanel()
    {
        return "//span[text()='Measurements']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String ContainsTextBox()
    {
        return "(//input[@class='txt border'])[1]";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String BusinessUnitDropDownOption2(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[contains(text(),'" + text + "')]";

    }

    public static String Text5(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }
//
    
      public static String enviroSustainabilityTab()
    {
        return "//div[label='Environmental Sustainability']";
    }
    
    public static String evaluatePerformanceMaintenanceTab()
    {
        return "//div[label='Evaluate Performance Maintenance']";
    }
    
    public static String hazardCategoriesTab()
    {
        return "//div[@id='section_b74e0a1a-4888-47e8-b331-b8344b51c119']/label";
    }
    
    public static String Button_Save()
    {
        return "//div[@id='btnSave_form_664FCCCE-1B21-4993-8A0A-7ECF4C2B755A']";
    }
    
    public static String level2Button_Save()
    {
        return "//div[@id='btnSave_form_00BB707A-F08E-4857-BDFF-308CD57F7F73']";
    }
    
    public static String delete_btn()
    {
        return "//div[@id='btnDelete_form_7D7FA005-B236-4D95-B304-726C81824281']";
    }
    
    public static String btn_confirmYes()
    {
        return "//div[@class='confirm-popup popup']//div[@title='Yes']";
    }
    
    public static String confirm_OK()
    {
        return "//div[@id='btnHideAlert']";
    }
    
//
    public static String hazardCategories_AddBtn()
    {
        return "//div[@id='btnActAddNew']";
    }
    
    public static String search_Btn()
    {
        return "//div[@id='btnActApplyFilter']";
    }
    
    public static String nextResult_page()
    {
        return "//a[@title='Go to the next page']";
    }
    
    public static String record_Selection()
    {
        return "//div[@title='Timmy Test Type']";
    }
    
    public static String addPoints_Btn()
    {
        return "//div[@id='btnAddNew']";
    }
    
    public static String points_processFlow()
    {
        return "//div[@id='btnProcessFlow_form_24F62D40-8970-4FEF-84C5-B0FFB645C2DE']";
    }
    
    public static String points_refInput()
    {
        return "//div[@id='control_D1A25D38-5B21-4A5B-9019-0C08F1549DDE']//input";
    }
    
    public static String pointDescription_input()
    {
        return "//div[@id='control_FCB5573F-C6EC-4096-B32F-995B4D88463A']//input";
    }
    
    public static String level2_addBtn()
    {
        return "//div[@id='btnAddNew']";
    }
    
    public static String level2_recSelection()
    {
        return "/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[9]/div[1]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[1]";
    }
    
    public static String level2_processFlow()
    {
        return "//div[@id='btnProcessFlow_form_00BB707A-F08E-4857-BDFF-308CD57F7F73']";
    }
    
    public static String level2_inputField()
    {
        return "//div[@id='control_0F53A4B9-55E6-4E34-ADF4-1A917BB097D1']//input";
    }
    
    public static String level2_descriptionField()
    {
        return "//div[@id='control_5505B5C3-5782-4CCA-B6C0-C277D2947968']//textarea";
    }
    
    public static String level2_linkField()
    {
        return "//div[@id='control_3384201B-5BC4-4F18-8540-A8A5878BBFB0']//input";
    }
    
    public static String level2_linkToBusiness()
    {
        return "//div[@id='control_0317D82F-E409-4A20-8E39-97747E4C7F6E']/div/div";
    }
    
    public static String level2_linkToBusinessTab()
    {
        return "//li[@id='tab_689BA247-CCA9-4F0A-A760-A3C12A89E531']";
    }
    
    public static String businessUnit_dd()
    {
        return "//div[@id='control_E70E5259-A6E9-4FEA-B1E6-441EF9886AD4']//li";
    }
    
    public static String businessUnit_ddSelection()
    {
        return "//*[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c_anchor']/i[1]";
    }
    
    public static String impactType_dd()
    {
        return "//div[@id='control_561BB31E-0173-4466-BA76-A3064B885464']//li";
    }
    
    public static String impactType_ddSelection()
    {
        return "//*[@id='2d05206f-b706-475b-b686-840a9b892278_anchor']/i[1]";
    }
    
    public static String level3_addBtn()
    {
        return "//div[@id='control_0DDFBC37-AB26-4888-A0C6-6DDC7825FE03']//div[@id='btnAddNew']";
    }
    
    public static String level3_processFlow()
    {
        return "//div[@id='btnProcessFlow_form_D817294B-D88F-4F92-BF22-6C098FEC1651']";
    }
    
    public static String level3_inputField()
    {
        return "//div[@id='control_177643DE-3CC8-4564-9D96-1BD0E364B887']/div[1]//input";
    }
    
    public static String level3_descriptionField()
    {
        return "//div[@id='control_AC332A54-C0DD-416E-8184-76BE57E26649']//textarea";
    }
    
    public static String level3_consequenceField()
    {
        return "//div[@id='control_C19B8CE4-E41D-4B51-BE8E-791392EE2114']//textarea";
    }
    
    public static String level3_linkField()
    {
        return "//div[@id='control_581AE8F8-2DC4-47A9-BBAB-06666EB0DDAE']//input";
    }
    
    public static String level3_linkToBusiness()
    {
        return "//div[@id='control_3DA05B4C-B275-42DB-8ED5-6B86C1901BE5']/div/div";
    }
    
    public static String level3Button_Save()
    {
        return "//div[@id='btnSave_form_D817294B-D88F-4F92-BF22-6C098FEC1651']";
    }
    
    public static String level3_linkToBusinessTab()
    {
        return "//li[@id='tab_55C34144-F622-40B8-B4CF-A2434C3BF831']/div[1]";
    }
    
    public static String level3BusinessUnit_dd()
    {
        return "//div[@id='control_24AA777B-A0C9-49D5-950A-0D7C24CCA6DE']//li";
    }
    
    public static String level3BusinessUnit_ddSelection()
    {
        return "//*[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c_anchor']/i[1]";
    }
    
    public static String levl3ImpactType_dd()
    {
        return "//div[@id='control_F30B1C2C-6BAC-4FC7-9A31-C23252CE2E5C']//li";
    }
    
    public static String level3ImpactType_ddSelection()
    {
        return "//*[@id='2d05206f-b706-475b-b686-840a9b892278_anchor']/i[1]";
    }
    
    public static String level3_recSelection()
    {
        return "/html/body/div[1]/div[3]/div/div[2]/div[11]/div[4]/div[13]/div[9]/div[2]/div[1]/div/div/div[1]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[1]";
    }
    
    public static String level4_addBtn()
    {
        return "//div[@id='control_743CDC5E-48AD-4602-B250-20D3E1C9F317']//div[@id='btnAddNew']";
    }
    
    public static String level4_processFlow()
    {
        return "//div[@id='btnProcessFlow_form_02964BA3-7BC5-4010-A471-A35ECEFCB21B']";
    }
    
    public static String level4_inputField()
    {
        return "//div[@id='control_61A676AF-4658-4548-B2B3-1C8BCD8681DE']/div[1]//textarea";
    }
    
    public static String level4_descriptionField()
    {
        return "//div[@id='control_1544DA2A-A009-4F39-A666-71EECC887915']//textarea";
    }
    
    public static String level4_linkField()
    {
        return "//div[@id='control_3B1D2549-D533-496A-85A5-CE74185A4249']//input";
    }
    
    public static String level4_linkToBusiness()
    {
        return "//div[@id='control_3FB82601-BC8D-448D-AD31-50F764C63110']/div/div";
    }
    
    public static String level4Button_Save()
    {
        return "//div[@id='btnSave_form_02964BA3-7BC5-4010-A471-A35ECEFCB21B']";
    }
    
    public static String level4_linkToBusinessTab()
    {
        return "//li[@id='tab_1C7DF75C-9864-4001-851D-FCDD70D67782']/div[1]";
    }
    
     public static String level4BusinessUnit_dd()
    {
        return "//div[@id='control_855724C2-444B-4581-A234-9C961C5866E5']//li";
    }
     
    public static String level4BusinessUnit_ddSelection()
    {
        return "//*[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c_anchor']/i[1]";
    } 
     
    public static String levl4ImpactType_dd()
    {
        return "//div[@id='control_A3E9AEB5-9882-4F93-8396-5A733480C0E7']//li";
    } 
     
    public static String level4ImpactType_ddSelection()
    {
        return "//*[@id='2d05206f-b706-475b-b686-840a9b892278_anchor']/i[1]";
    }
     
    public static String level4_recSelection()
    {
        return "/html/body/div[1]/div[3]/div/div[2]/div[16]/div[4]/div[15]/div[9]/div[2]/div[1]/div/div/div[1]/div[2]/div[2]/div[1]/div[3]/table/tbody/tr[1]";
    }
    
    public static String level5_addBtn()
    {
        return "//div[@id='control_95B5EC72-4B82-49C3-99EF-9F2BAC3F7534']//div[@id='btnAddNew']";
    }
    
    public static String level5_processFlow()
    {
        return "//div[@id='btnProcessFlow_form_CAD43B9C-D4A4-45D3-AD27-F5770BA01CF3']";
    }
    
    public static String level5_inputField()
    {
        return "//div[@id='control_60FBC914-7786-4FA6-8FE6-1431A0D3F6DC']/div[1]//textarea";
    }
    
    public static String level5_descriptionField()
    {
        return "//div[@id='control_48BE6C69-473B-46B5-B4DB-1C8C524745A5']//textarea";
    }
    
    public static String level5_linkField()
    {
        return "//div[@id='control_959F35FE-FD71-4044-8B0B-922A8C19B06C']//textarea";
    }
    
    public static String level5_linkToBusiness()
    {
        return "//div[@id='control_427B41E4-7F02-46F2-AE55-FDF0351CE483']/div/div";
    }
    
    public static String level5Button_Save()
    {
        return "//div[@id='btnSave_form_CAD43B9C-D4A4-45D3-AD27-F5770BA01CF3']";
    }
    
    public static String level5_linkToBusinessTab()
    {
        return "//li[@id='tab_09062D62-D812-475C-ACB3-39AB49E6740F']";
    }
    
     public static String level5BusinessUnit_dd()
    {
        return "//div[@id='control_6F86675D-C73B-413C-B89C-BE0C1B2EAA70']//li";
    }
     
    public static String level5BusinessUnit_ddSelection()
    {
        return "//*[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c_anchor']/i[1]";
    } 
     
    public static String levl5ImpactType_dd()
    {
        return "//div[@id='control_04F88275-F5DA-4B96-9261-1A41502FDC1F']//li";
    } 
     
    public static String level5ImpactType_ddSelection()
    {
        return "//*[@id='2d05206f-b706-475b-b686-840a9b892278_anchor']/i[1]";
    } 
     
    public static String level5_recSelection()
    {
        return "//div[@id='control_8D8083F6-CF6B-4CBC-ACCC-D4873B50CFA6']//div[@id='grid']/div[3]/table/tbody/tr[1]";
    } 
     
    public static String obligations_addBtn()
    {
        return "//div[@id='control_5EC2C055-9D5E-4111-B577-95F7925BCB9F']//div[@id='btnAddNew']/div";
    }
    
    public static String inLine_Btn()
    {
        return "//div[@id='control_5EC2C055-9D5E-4111-B577-95F7925BCB9F']//*[@id=\"grid\"]/div[4]/div/div[2]/div[1]/div/div";
    } 
    
    public static String obligations_processFlow()
    {
        return "//div[@id='btnProcessFlow_form_537B2350-DE67-4ABF-BEA1-85B0C2C4174C']";
    }
    
    public static String obligations_inputField()
    {
        return "//div[@id='control_8E22304C-342B-417D-BE07-B0CD0865D53F']/div[1]//textarea";
    }
    
    public static String obligationsBtn_Save()
    {
        return "//div[@id='btnSave_form_537B2350-DE67-4ABF-BEA1-85B0C2C4174C']";
    }
    
    
    

    public static String hazardCategories_processFlow()
    {
        return "//div[@id='btnProcessFlow_form_664FCCCE-1B21-4993-8A0A-7ECF4C2B755A']";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String Text(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String Text4(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[contains(text(),'" + text + "')]";
        //div[contains(@class,'transition visible')]//a[contains(text(),'Environmental Impact')]
    }

    public static String businessUnit_dropDown()
    {
        return "//div[@id='control_985F11A4-20D0-4B38-9C5D-A2FBA77FEE4F']";
    }
    
    public static String closeBusinessUnit_dropDown()
    {
        return "//div[@id='control_985F11A4-20D0-4B38-9C5D-A2FBA77FEE4F']//b[@class='select3-down drop_click']";
    }
    
    public static String businessUnit_expand1()
    {
        return "//li[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c']/i";
    }
    
    public static String businessUnit_expand2()
    {
        return "//li[@id='7b0159f1-23bb-4579-ae4a-86a751efc2ca']/i";
    }
    
    public static String businessUnit_expand3()
    {
        return "//li[@id='e0c53028-7283-4577-acd8-4f6d681f1b62']/i";
    }
    
    public static String businessUnit_selection()
    {
        return "//a[@id='01c13f84-4e67-4a72-99fd-347a8d9b2c32_anchor']/i";
    }
    
    public static String business_area()
    {
        return "//div[@id='control_9E44085F-2156-465D-A127-E92B86A051D3']//input";
    }
    
    public static String process_description()
    {
        return "//div[@id='control_41CB5EDC-EE75-4FF6-AF84-80495D16E11C']//textarea";
    }
    
    public static String type_inputField()
    {
        return "//div[@id='control_C56E686E-2AB6-492D-B404-14ADD73C1183']//input";
    }
    
    public static String description_inputField()
    {
        return "//div[@id='control_8E6F725F-3104-4EA0-B07D-3EE3999BF5AA']//textarea";
    }
    
    public static String parameterComponents_selection1()
    {
        return "//a[@id='1275bd1b-345f-4310-aec1-233dabf34f62_anchor']";
    }
    
    public static String reference()
    {
        return "//div[@id='control_FDFBF4A6-5416-4A36-AF6A-8D52D62F05D8']//input";
    }
    
    public static String lifecycle_Dd()
    {
        return "//div[@id='control_15FAB4B6-7F13-435B-9462-A177DC54EECE']//li";
    }
    
    public static String lifecycle_Selection()
    {
        return "//a[@id='1dcb259e-29b1-4b4c-8f00-79ac009bba82_anchor']/i[1]";
    }

    public static String linkTo_process()
    {
        return "//div[@id='control_0FFB79D4-A97A-4D02-8C25-DDE2AF9D4936']//input";
    }

    
    
    
    
    
    
    
    
    
    
    
    public static String Title()
    {
        return "//div[@id='control_8F5FF883-CB77-4608-A392-1E884CD70EA3']//input";
    }

    public static String Introduction()
    {
        return "//div[@id='control_D3A159E1-ABA7-4BCB-9B0F-1AA0AD824A1F']//textarea";
    }

    public static String Objectives()
    {
        return "//div[@id='control_0AE872B5-DC1D-499D-B221-2B61E9AAC7F9']//textarea";
    }

    public static String Boundaries()
    {
        return "//div[@id='control_3AD68745-08C7-4032-B2A6-22601195A655']//textarea";
    }

    public static String Assumptions()
    {
        return "//div[@id='control_81BE1D76-F3D6-4B5E-AC9F-BFF442130283']//textarea";
    }

    public static String Methodology()
    {
        return "//div[@id='control_6486B17A-AFC5-4950-9ABC-34BA4C7F0DD1']//textarea";
    }

    

    public static String linkToADocument_2()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String UrlInput_TextArea()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextArea()
    {
        return "//input[@id='urlTitle']";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String BaselineChangeLog_Add()
    {
        return "//div[@id='control_5386C778-75CD-417E-A2F3-BA0232227F1D']//div[@id='btnAddNew']";
    }

    public static String BaselineChangeLog_processflow()
    {
        return "//div[@id='btnProcessFlow_form_B1AFB460-A6D1-43BD-A463-3C2E0ECABF9E']";
    }

    public static String ReasonsForReviewInclude_dropdown()
    {
        return "//div[@id='control_94BF8E2B-025E-4533-BFE0-EC7E765A6FF9']//ul";
    }

    public static String ReasonsForReviewInclude_CheckBox(String data)
    {
        return "(//a[text()='" + data + "']//i)[1]";
    }

    public static String DateOfChange()
    {
        return "//div[@id='control_2F345D95-2937-4E24-9266-A0E63D87FF43']//input";
    }

    public static String Comments()
    {
        return "//div[@id='control_FE522CCE-1E11-40B0-8F3C-933E636DE194']//textarea";
    }

    public static String PersonResponsibleDropDown()
    {
        return "//div[@id='control_654B1273-9E8C-4E6E-834D-2B589E3311E0']//ul";
    }

    public static String Baseline_Change_Save()
    {
        return "//div[@id='btnSave_form_B1AFB460-A6D1-43BD-A463-3C2E0ECABF9E']";
    }

    public static String LinkTostakeholderEngagementDropDown()
    {
        return "//div[@id='control_D3DD4627-9AF6-4275-BAA3-49A9E1FAAC91']//ul";
    }

    public static String LinkToEventDropDown()
    {
        return "//div[@id='control_8D1B7234-2CFC-43E7-B1BE-E770A1D237DD']//ul";
    }

    public static String LinkToAuditDropDown()
    {
        return "//div[@id='control_A3A2D058-C269-4274-8808-EF35ABD57BD9']//li";
    }

    public static String CompletedDropDown()
    {
        return "//div[@id='control_434D611E-A016-4089-9481-276A22F57435']//ul";
    }

    public static String Risk_Assessment_Team_Add()
    {
        return "//div[@id='control_97D1292B-D58E-45AA-9DBE-97593E367493']//div[@id='btnAddNew']";
    }

    public static String ExperienceRoleDropDown()
    {
        return "//div[@id='control_A1BF9E59-A685-4589-A3B6-9519333C11D1']//ul";
    }

    public static String FullNameDropDown()
    {
        return "//div[@id='control_E0535A21-FBC8-4F90-B767-B9C02D121842']//ul";
    }

    public static String HazardInventoryTab()
    {
        return "//div[text()='Hazard Inventory']";
    }

    public static String HazardInventory_Add()
    {
        return "//div[@id='control_1E0FDFE9-E40B-43C5-806C-BD0D00E4F92A']//div[@id='btnAddNew']";
    }

    public static String IssueBasedRequiredDropDown()
    {
        return "//div[@id='control_2D85EE31-5B84-437D-A025-C2104D3FE89C']//ul";
    }

    public static String HazardInventory_Save()
    {
        return "//div[@id='btnSave_form_6466008A-FF5F-4688-927C-9AF7A36EFB85']";
    }

    public static String Assumptions_Uncertainty()
    {
        return "//div[@id='control_0A583F03-2A7B-4AB8-B2EF-6B1AA8AA3C22']//textarea";
    }

    public static String MechanismofRelease()
    {
        return "//div[@id='control_A52EA721-EF81-4F1D-9DAA-D420B1AEF2EA']//textarea";
    }

    public static String MagnitudeofHazard_Aspect()
    {
        return "//div[@id='control_45569A36-F2BC-4368-A5D9-59F4A5AE406D']//textarea";
    }

    public static String RelatedActivitiesDropDown()
    {
        return "//div[@id='control_EEE339F2-0229-4468-8A52-D42CBD13ACB4']//ul";
    }

    public static String HazardRiskSourceDescription()
    {
        return "//div[@id='control_B92B79A4-9D5E-438B-900D-F394022151A2']//textarea";
    }

    public static String HazardRiskSourceClassificationDropDown()
    {
        return "//div[@id='control_C02EFACC-99FB-401E-80EA-8E005C953F8E']//ul";
    }

    public static String Button_Save_And_Close_DropDown()
    {
        return "//div[@id='btnSave_form_B1AFB460-A6D1-43BD-A463-3C2E0ECABF9E']//div[@class='options toggle']";
    }

    public static String Button_Save_And_Close()
    {
        return "//div[@id='btnSave_form_B1AFB460-A6D1-43BD-A463-3C2E0ECABF9E']//div[text()='Save and close']";
    }

    public static String RegisterTab()
    {
        return "//div[text()='Risk Register']";
    }

    public static String Baseline_WRAC_Add()
    {
        return "//div[@id='control_2754129C-2D56-4234-981C-4EBE34B936B2']//div[@id='btnAddNew']";
    }

    public static String RDropDown()
    {
        return "//div[@id='control_0A89D80F-7751-4302-B3F3-7C58979642C3']//ul";
    }

    public static String MDropDown()
    {
        return "//div[@id='control_5E7F0F5C-D8BF-45D3-B0BE-A9F0E22F94A7']//ul";
    }

    public static String LRDropDown()
    {
        return "//div[@id='control_20ACD9B4-6108-48B5-9210-A10D5F4F58EC']//ul";
    }

    public static String CDropDown()
    {
        return "//div[@id='control_17D20227-55EC-4C12-B640-5AB6A4851A02']//ul";
    }

    public static String EDropDown()
    {
        return "//div[@id='control_A9129CB1-6C1B-4561-ACE9-D9BA3ABA8B8E']//ul";
    }

    public static String HDropDown()
    {
        return "//div[@id='control_3A056D59-36AD-442B-94AC-A4E3F522B225']//ul";
    }

    public static String SDropDown()
    {
        return "//div[@id='control_E11280D2-318C-47C8-85D9-BB5D60B000E7']//ul";
    }

    public static String LikelihoodDropDown()
    {
        return "//div[@id='control_28F53CB1-A517-42DB-B823-C30363EC2E85']//ul";
    }

    public static String CurrentControls()
    {
        return "//div[@id='control_D7506E25-9243-4D5C-B65A-49A4A1E94195']//textarea";
    }

    public static String DescriptionofUnwantedEvent()
    {
        return "//div[@id='control_91BDE3F6-3D3C-4896-BC63-A11A919E12BB']//textarea";
    }

    public static String FunctionalOwnershipDropDown()
    {
        return "//div[@id='control_7F4B3A56-E129-4012-89A2-B4EC9774858A']//ul";
    }

    public static String HazardRiskSourceClassificationDropDown2()
    {
        return "//div[@id='control_F3AC037D-2695-4593-8A4D-DAF11B894FC6']//ul";
    }

    public static String HazardrisksourcedescriptionDropDown()
    {
        return "//div[@id='control_797FE442-E3F9-4DF7-A18B-343103F801A2']//ul";
    }

    public static String BaselineDropDownOption(String data)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + data + "']/..//i)[1]";
    }

    public static String DeleteButton()
    {
        return "//div[@id='btnDelete_form_664FCCCE-1B21-4993-8A0A-7ECF4C2B755A']";
    }

    public static String formview()
    {
        return "//i[@class='icon edit on paper icon ten six grid-icon-edit grid-icon-edit-active']";
    }

    public static String BaslineWracProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_C573AFA6-72FA-4A61-9551-C9097D40EB0E']";
    }

    public static String PossibleImprovementsOrAdditionalControlsDropDown()
    {
        return "(//div[@id='control_09A3F461-BDF3-435B-A22A-829B8C2607DB']//div//i)[1]";
    }

    public static String PossibleImprovementsOrAdditionalControlsAddButton()
    {
        return "//div[@id='control_8D9C923F-9846-4AC9-B738-7DC7A887B8E8']//div[@id='btnAddNew']";
    }

    public static String PossibleImprovementsProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_12CDB0B0-0838-4B45-A570-6F3674D9EEC2']";
    }

    public static String TypeOfAction()
    {
        return "//div[@id='control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE']//ul";
    }

    public static String ActionDescription()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String Entity()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }

    public static String PosibleImrovementsResponsibleperson()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }

    public static String PossibleImprovementsAgency()
    {
        return "//div[@id='control_5B580F56-394D-4695-8AB2-C2CB9AAE9EB9']//ul";
    }

    public static String PossibleImprovementsActionDueDate()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String PossibleImprovements_Save()
    {
        return "//div[@id='btnSave_form_12CDB0B0-0838-4B45-A570-6F3674D9EEC2']";
    }

    public static String SavingMask()
    {
        return "//div[@class='form active transition visible']//div[text()='Saving...']";
    }

    public static String Basline_Save_And_Close_DropDown()
    {
        return "//div[@id='btnSave_form_6466008A-FF5F-4688-927C-9AF7A36EFB85']//div[@class='more options icon chevron down']";
    }

    public static String Basline_Save_And_Close()
    {
        return "//div[@id='btnSaveClose_form_6466008A-FF5F-4688-927C-9AF7A36EFB85']//div[text()='Save and close']";
    }

    public static String TeamNameDropDown()
    {
        return "//div[@id='control_EF255594-8116-4EA8-AE68-413E62CF23E8']//ul";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.PermitManagement_PageObjects;

/**
 *
 * @author smabe
 */
public class PermitManagement_PageObjects
{

    public static String Record_Number;
    public static String Window_1;

    public static String getWindow()
    {
        return Window_1;
    }

    public static void setWindow(String Window)
    {
        Window_1 = Window;
    }

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String recordSaved_popup_2()
    {
        return "(//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')])[2]";
    }

    public static String SearchOption()
    {
        return "//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String Text4(String text)
    {
        return "//a[contains(text(),'" + text + "')]";

    }

    public static String Text5(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";

    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String linkADoc_buttonxpath()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String ContainsTextBox()
    {
        return "//input[@class='txt border']";
    }

    public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String BusinessUnitDropDownOption(String text)
    {
        return "(//div[contains(@class,'transition visible')]//ul[@class='jstree-children']//a[text()='" + text + "'])[1]";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String SaveButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Save']";
    }

    public static String SaveButton2()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[1]";
    }
    
     public static String SavePermit()
    {
        return "//div[@id='btnSave_form_29DAE2B0-C71B-4599-8899-9A431EA65963']";
    }

    public static String SaveButton3()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[2]";
    }

    public static String SaveButton4()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[text()='Save'])[1]";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String SupportingDocumentsTab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String PermitManagementLabel()
    {
        return "//label[contains(text(),'Permit Management')]";
    }

    public static String addButtonXpath()
    {
        return "//div[contains(@class,'active form transition visible')]//div[@id='btnActAddNew']";
    }

    public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_06A92B5E-4170-473C-B9F9-CFF06AB14C74']";
    }

    public static String processFlow1()
    {
        return "//div[contains(@class,'form active transition visible')]//span[@original-title='Process flow']/..";
    }

    public static String businessUnitDropdown()
    {
        return "//div[@id='control_5614586C-E586-4742-9423-A40BFA7F4E1B']//ul";
    }

    public static String CommodityDropdown()
    {
        return "//div[@id='control_6DAED832-BAB3-44D2-9B27-AFFBCD771B7F']";
    }

    public static String SiteTextBox()
    {
        return "(//div[@id='control_F964B79A-0F36-4E4A-8F69-F2116BF96A8C']//input)[1]";
    }

    public static String businessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String deleteRecordButton()
    {
        return "//div[@id='btnDelete_form_06A92B5E-4170-473C-B9F9-CFF06AB14C74']";
    }

    public static String Permits_Tab()
    {
        return "(//div[text()='Permits'])[1]";
    }

    public static String PermitsAdd()
    {
        return "//div[@id='control_C8B6963A-9D58-49B5-BDFD-5D85C78FA6EF']//div[@id='btnAddNew']";
    }

    public static String NewApprovedPermitDropdown()
    {
        return "//div[@id='control_26672207-9205-46BF-A24C-61A01CF2ED75']//ul";
    }

    public static String PermitprocessFlow()
    {
        return "//div[@id='btnProcessFlow_form_29DAE2B0-C71B-4599-8899-9A431EA65963']";
    }

    public static String Description()
    {
        return "//div[@id='control_CB867600-1E37-48F6-BF8D-141F88D60649']//textarea";
    }

    public static String Title()
    {
        return "(//div[@id='control_8322E2A1-35A5-402D-BAAA-81E7C6E3A027']//input)[1]";
    }

    public static String CreateFromTemplateCheckbox()
    {
        return "//div[@id='control_F1A8D376-02E3-44A6-ADB0-B7A2135840B9']//div[@class='c-chk']//div";
    }

    public static String AccountablePersonDropdown()
    {
        return "//div[@id='control_4FAD5338-F20C-47C4-9004-53FA19040D27']//ul";
    }

    public static String PermitOwnerDepartmentDropdown()
    {
        return "//div[@id='control_8DF0FB3A-98D6-4495-979A-78D3C54FF1B6']//ul";
    }

    public static String PermitNatureDropdown()
    {
        return "//div[@id='control_2488F522-73AF-462B-93AF-FA1412616C62']//ul";
    }

    public static String LegalStakeholderGroupDropdown()
    {
        return "//div[@id='control_FB659290-C2E9-43CA-B1A8-3F7FCD14EF85']//ul";
    }

    public static String InspectingAuthorityDropdown()
    {
        return "//div[@id='control_6B142D02-F252-4AF1-96D9-924B1B7D950B']//ul";
    }

    public static String GrantingAuthorityDropdown()
    {
        return "//div[@id='control_0FA5100D-5A4F-479F-91A5-322A8AAD456D']//ul";
    }

    public static String PermitOwnerDropdown()
    {
        return "//div[@id='control_2A8DF6D3-B366-4781-A532-2D0C61CD617B']//ul";
    }

    public static String PermitLibraryDropdown()
    {
        return "//div[@id='control_5691A7F4-2609-45A9-8872-F7F047922F69']//ul";
    }

    public static String OtherNatureDescription()
    {
        return "//div[@id='control_F9075D09-72ED-449B-A663-4A3E719E253D']//textarea";
    }

    public static String DoesThisPermitExpire()
    {
        return "//div[@id='control_CDFE0251-ADD3-4D9B-BB1A-D757DEB9BB1C']//ul";
    }

    public static String ExpirationDate()
    {
        return "//div[@id='control_E34D1BEA-5054-462A-A9CA-F3576A727670']//input";
    }

    public static String PermitStatusDropdown()
    {
        return "//div[@id='control_AF65CC98-9C2A-46C9-8234-2DED2F2DC6E5']//ul";
    }

    public static String ApplicationInformation_Tab()
    {
        return "(//div[text()='Application Information'])[1]";
    }

    public static String ApplicationInformationAdd()
    {
        return "//div[@id='control_F9225C7D-DA11-4247-9BB3-A31E2134C49B']//div[@id='btnAddNew']";
    }

    public static String ApplicationInformationprocessFlow()
    {
        return "//div[@id='btnProcessFlow_form_0442D267-D4EF-4C12-B981-ACEF6758A2A8']";
    }

    public static String CurrencyDropDown()
    {
        return "//div[@id='control_5894186A-6651-405C-8B91-FE46FC04B850']//ul";
    }

    public static String ApplicationNotes()
    {
        return "//div[@id='control_BBD2F47A-3DAC-4894-85E4-BF8126557E83']//textarea";
    }

    public static String ApplicationDate()
    {
        return "//div[@id='control_F863A839-9BFD-403E-9A10-152DC3467F4A']//input";
    }

    public static String uploadLinkbox()
    {
        return "(//b[@original-title='Link to a document'])[2]";
    }

    public static String ApplicationSaveButton()
    {
        return "//div[@id='btnSave_form_0442D267-D4EF-4C12-B981-ACEF6758A2A8']";
    }

    public static String RiskandEvents_Tab()
    {
        return "//div[text()='Risk and Events']";
    }

    public static String RiskPanel()
    {
        return "//span[text()='Risks']/..//i";
    }

    public static String ConditionsAndCommitments_Tab()
    {
        return "(//div[text()='Conditions and Commitments'])[1]";
    }

    public static String ConditionsAndCommitmentsAddButton()
    {
        return "//div[@id='control_541B13BC-1AAC-4E6A-B780-C68D36B26444']//div[@id='btnAddNew']";
    }

    public static String ConditionsAndCommitmentsprocessFlow()
    {
        return "//div[@id='btnProcessFlow_form_8592CE8F-CC84-4525-A6FA-7F8F1EB1B5B2']";
    }

    public static String OwnerDropdown()
    {
        return "//div[@id='control_AED68E6D-3039-4916-8599-E42B7A7C9EE1']//ul";
    }

    public static String ApproverDropdown()
    {
        return "//div[@id='control_B5A54D2B-72E4-4C3D-AFC8-E6DFE3029AAE']//ul";
    }

    public static String CategoryDropdown()
    {
        return "//div[@id='control_052B3A60-874C-4F7A-88EA-E2019BA50262']//ul";
    }

    public static String TypeDropDown()
    {
        return "//div[@id='control_C5F88EC4-5A93-4F83-8CAC-4F03DDF3781F']//ul";
    }

    public static String ConditionsAndCommitmentsTitle()
    {
        return "(//div[@id='control_06D6617B-3C38-4008-A3E5-00043F9E3202']//input)[1]";
    }

    public static String NatureDropdown()
    {
        return "//div[@id='control_77A36D3E-D562-4D3E-9363-0B77E0D94DAF']//ul";
    }

    public static String FrequencyDropdown()
    {
        return "//div[@id='control_19FEC809-E9E2-462E-9EDE-B967898FA289']//ul";
    }

    public static String DurationDropdown()
    {
        return "//div[@id='control_645CD6B3-43F6-4FB3-920A-96F692D9115D']//ul";
    }

    public static String SaveConditionsAndCommitments()
    {
        return "//div[@id='btnSave_form_8592CE8F-CC84-4525-A6FA-7F8F1EB1B5B2']";
    }

    public static String Issues_Tab()
    {
        return "//div[text()='Issues']";
    }

    public static String IssuesAdd()
    {
        return "//div[@id='control_617FA18F-52C9-4D56-9D1C-E9101EC2178C']//div[@id='btnAddNew']";
    }

    public static String IssuesprocessFlow()
    {
        return "//div[@id='btnProcessFlow_form_43109051-94EE-4827-897A-54037E7AC8F7']";
    }

    public static String IssueTitle()
    {
        return "//div[@id='control_8D940508-CD89-4C56-B623-583558CF0AEC']//input";
    }

    public static String WhatWasDoneInTheInterim()
    {
        return "//div[@id='control_AADE623D-768C-439B-8EE7-45CB6AABE254']//textarea";
    }

    public static String WhatWasDoneToControlTheRisk()
    {
        return "//div[@id='control_23C8FD67-79D2-43E5-B278-571E02920D87']//textarea";
    }

    public static String HasTheRiskBeenControlledDropDown()
    {
        return "(//div[@id='control_6DF8E7F7-14A7-4C18-A49D-73F3D663EFC0']//b)[1]";
    }

    public static String IssueCategoryDropDown()
    {
        return "//div[@id='control_674CDB39-A910-4FD3-90CD-D271E2291EBC']//ul";
    }

    public static String IssuesManagementSaveButton()
    {
        return "//div[@id='btnSave_form_43109051-94EE-4827-897A-54037E7AC8F7']";
    }

    public static String SupportingDocuments_Tab()
    {
        return "(//div[text()='Supporting Documents'])[1]";
    }

    public static String NextButton()
    {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String Actions_Tab()
    {
        return "(//div[text()='Actions'])[2]";
    }

    public static String ActionsAdd()
    {
        return "//div[@id='control_D9646D6E-C645-45D1-83C7-5E76791EF9C1']//div[@id='btnAddNew']";
    }

    public static String ActionsprocessFlow()
    {
        return "//div[@id='btnProcessFlow_form_63A73C97-9EE6-4DF9-B469-8E97B6AB083C']";
    }

    public static String TypeOfActionDropDown()
    {
        return "//div[@id='control_F1D9D484-9182-4B8C-95F9-AFFB440EB3CE']//ul";
    }

    public static String ActionDescription()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String EntityDropdown()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }

    public static String ResponsiblePersonDropDown()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }

    public static String ActionDueDate()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String IssuesManagementSaveButton2()
    {
        return "//div[@id='btnSave_form_63A73C97-9EE6-4DF9-B469-8E97B6AB083C']";
    }

    public static String CreateANewEngagement()
    {
        return "(//div[contains(text(),'Create a new engagement')])";
    }

    public static String EngagementsprocessFlow()
    {
        return "//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

    public static String EngagementTitle()
    {
        return "//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input";
    }

    public static String EngagementDate()
    {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }

    public static String BusinessUnitDropdown()
    {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//ul";
    }

    public static String FunctionOfEngagementDropDown()
    {
        return "//div[@id='control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB']//ul";
    }

    public static String EngagementDescription()
    {
        return "//div[@id='control_1C19AE65-23A1-4ADC-A631-D9273FC0CE9F']//textarea";
    }

    public static String ResponsiblePersonDropDown2()
    {
        return "//div[@id='control_213251A2-010A-4BBF-A65A-A1FC8C6F7033']//ul";
    }

    public static String MethodOfEngagementDropDown()
    {
        return "//div[@id='control_4A471537-8229-4E54-A86C-DCEB99BA24D0']//ul";
    }

    public static String EngagementsSaveButton()
    {
        return "//div[@id='btnSave_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

    public static String GeographiclocationDropdown()
    {
        return "//div[@id='control_F703A144-D0B6-4D4D-B5E2-D4E186427A43']//ul";
    }

    public static String Engagements_Tab()
    {
        return "(//div[contains(text(),'Engagements')])[1]";
    }

    public static String ColapsArrow()
    {
        return"//div[@class='select3 select3-ddl select3_1b24a4f1 c-ddl select3-drop-above']//b[@class='select3-down drop_click']";
    }

    public static String ColapsArrow1()
    {
       return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//b[@class='select3-down drop_click']";
    }

    public static String ContactInquiryTopicDropDown()
    {
        return"//div[@id='control_2D1B5E8D-BBF2-448A-9765-F03FA8C31019']//ul";
    }

    public static String FunctionColapsArrow()
    {
       return "//div[@id='control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB']//b[@class='select3-down drop_click']";
    }

    public static String SavePermitSoftCopy()
    {
        return"(//div[contains(text(),'Save permit soft copy')])[1]";
    }

    public static String LinkedConditionsAndCommitmentsTab()
    {
       return "//div[text()='Linked Conditions and Commitments']";
    }

    public static String ConditionsAndCommitmentsFromRegister()
    {
       return "//span[text()='Conditions and Commitments from Register']";
    }

    public static String RecordResults()
    {
        return"//div[@id='control_2FB001D7-69BF-4A8D-82DB-7F7183D0BB5D']//div[text()='No results returned']";
    }

    public static String LinkedConditionsAndCommitmentsfromLinkedPermitsTab()
    {
       return "//span[text()='Conditions and Commitments from linked Permits']";
    }

    public static String ApplicationFee()
    {
       return "(//div[@id='control_FE05421C-84FE-40B5-AA38-715F504BE6A5']//input)[1]";
    }
}

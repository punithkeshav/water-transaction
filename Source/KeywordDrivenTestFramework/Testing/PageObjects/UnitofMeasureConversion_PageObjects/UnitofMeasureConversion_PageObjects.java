/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.UnitofMeasureConversion_PageObjects;

/**
 *
 * @author smabe
 */
public class UnitofMeasureConversion_PageObjects
{
    
    public static String Record_Number;
    public static String Window_1;

    public static String getWindow()
    {
        return Window_1;
    }

    public static void setWindow(String Window)
    {
        Window_1 = Window;
    }

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String recordSaved_popup_2()
    {
        return "(//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')])[2]";
    }

    public static String SearchOption()
    {
        return "//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String Text4(String text)
    {
        return "//a[contains(text(),'" + text + "')]";

    }

    public static String Text5(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";

    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String linkADoc_buttonxpath()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String ContainsTextBox()
    {
        return "//input[@class='txt border']";
    }

    public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String BusinessUnitDropDownOption(String text)
    {
        return "(//div[contains(@class,'transition visible')]//ul[@class='jstree-children']//a[text()='" + text + "'])[1]";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TypeSearchCheckBox_2(String data)
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String SaveButton()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Save']";
    }

    public static String SaveButton2()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[1]";
    }

    public static String SaveButton3()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[text()='Save'])[2]";
    }

    public static String SaveButton4()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[text()='Save'])[1]";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String SupportingDocumentsTab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }
    
     public static String ECO2ManLabel()
    {
        return "//label[text()='ECO2Man']";
    }

    public static String MonitoringMaintence()
    {
        return "//div[@original-title='Monitoring Maintenance']";
    }

    public static String AddButton()
    {
        return "//div[contains(@class,'active form transition visible')]//div[@id='btnActAddNew']";
    }
    
      public static String DeleteButton()
    {
        return"//div[contains(@class,'form active transition visible')]//div[text()='Delete']";
    }

    public static String UOMConversionLabel()
    {
       return "//label[text()='UOM Conversion']";
    }

    public static String processFlow()
    {
        return"//div[@id='btnProcessFlow_form_F3D5D4E9-6C99-446B-981B-D4A232012689']";
    }
    
       public static String ConversionToDropDown()
    {
       return "//div[@id='control_BD2058FA-0AD6-4008-A351-BAD7592278F2']//ul";
    }

    public static String UnitOfMeasureDropDown()
    {
       return "//div[@id='control_7AA7C6EA-6770-4835-9AC8-C407C3089663']//ul";
    }

    public static String ConvertToDropDown()
    {
       return "//div[@id='control_FF681D40-8F3D-469C-B4A6-B0A9B9AD47C9']//ul";
    }

    public static String ConversionFactor()
    {
       return "(//div[@id='control_0B24CC0A-26D3-47A8-A70F-782EF4BD382F']//input)[1]";
    }

    public static String UnitOfMeasureAbbreviation()
    {
        return"(//div[@id='control_6CB3E0A4-2D2B-43BF-A4CA-9B5CFC2252B5']//input)[1]";
    }

    public static String ConvertToAbbreviation()
    {
       return "(//div[@id='control_9A422914-A5F7-4CFC-A742-2AE17286DF96']//input)[1]";
    }

    public static String ConversionFromDropDown()
    {
        return"(//div[@id='control_AFE63240-7D3D-4670-B787-5A47243AAA10']//ul)[1]";
    }

}

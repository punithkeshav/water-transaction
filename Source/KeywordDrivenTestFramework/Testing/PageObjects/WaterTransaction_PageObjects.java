/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import KeywordDrivenTestFramework.Testing.PageObjects.WaterTransaction_PageObjects.*;
import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class WaterTransaction_PageObjects extends BaseClass
{ 
    public static String FindingsOwnerDropDown()
    {
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']//li";
    }

    public static String FindingsRiskSourceSelectAll()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//b[@original-title='Select all']";
    }

    public static String SaveButtonFindings()
    {
        return "//div[@id='btnSave_form_D02673BB-369F-4EB7-906D-3C76FD3B0897']";
    }

    
    
    public static String RecordEdind()
    {

        return "//i[@class='icon edit on paper icon ten six grid-icon-edit grid-icon-edit-active']";
    }

    public static String CostIncurredTransport()
    {
        return "//div[@id='control_11F014C5-B9D5-45F5-9856-4C46C7FAABF5']//input";
    }

    public static String RecordDelete()
    {
        return"//div[@id='control_95FA633D-2148-4CA4-84EE-7A576E2F7567']//table//tbody//i[@class='icon bin icon ten six grid-icon-delete']";
    }

    public static String ButtonConfirm()
    {
        return"//div[@id='btnConfirmYes']";
    }

    public static String CloseCurrentModule()
    {
        return"(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
      return  "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String ContainsTextBox()
    {
        return"(//input[@class='txt border'])[1]";
    }
    
    
    public static String SearchButton()
    {
       return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return"//span[text()='"+string+"']";
    }

    public static String DeleteButton()
    {
       return "//div[@id='btnDelete_form_F517B4C9-16D1-4734-9DAB-8AEDA8426F53']";
    }

    public static String ButtonOK()
    {
       return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }
    //FR1 MS,AS1,AS2
    public static String addButtonXpath()
    {
        return "//div[@id='btnActAddNew']";
    }

    public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_F517B4C9-16D1-4734-9DAB-8AEDA8426F53']";
    }

    public static String processFlowAddText()
    {
        return "(//div[text()='Add phase'])[2]";
    }

    public static String processFlowEditText()
    {
        return "(//div[text()='Edit phase'])[2]";
    }

    public static String processFlowStatus(String phase)
    {
        return "(//div[text()='" + phase + "'])[2]/parent::div";
    }

    public static String processFlowStatusChild(String phase)
    {
        return "(//div[text()='" + phase + "'])[3]/parent::div";
    }

    public static String businessUnitDropdown()
    {
        return "//div[@id='control_2DA9BFE1-0DFB-49D8-B8FA-91C41588F17F']//li";
    }

    public static String businessUnitexpand(String text)
    {
        return "//a[text()='" + text + "']/../i";
    }

    public static String businessUnitValue(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String waterTypeDropdown()
    {
        return "//div[text()='Water type']/../..//div[@id='control_ECCB6A13-45D6-4733-B5BA-19D53E478387']//li";
    }

    public static String waterTypeDropdownValue(String option)
    {
        return "(//div[contains(@class,'transition visible')]//li[@title='" + option + "']//i[@class='jstree-icon jstree-ocl'])[1]";
    }

    public static String waterTypeDropdownChildValue(String option)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }

    public static String measurementTypeDropdown()
    {
        return "//div[@id='control_500DF708-222A-4E1F-ADE3-03595ED3CF8C']";
    }

    public static String singleSelect(String value)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + value + "']";
    }

    public static String monitoringPointDropdown()
    {
        return "//div[@id='control_C14FC30B-429E-464A-91BF-32E867450C35']//li";
    }

    public static String monthDropdown()
    {
        return "//div[@id='control_249D04EA-CF17-4906-B04B-85B8918F5250']//li";
    }

    public static String yearDropdown()
    {
        return "//div[@id='control_AA23CE3F-66BA-4D1C-A149-564C368B1452']//li";
    }

    public static String saveButton()
    {
        return " //div[@id='btnSave_form_F517B4C9-16D1-4734-9DAB-8AEDA8426F53']";
    }

    public static String parameterReadingsPanel()
    {
        return "//span[text()='Parameter Readings']";
    }

    public static String waterTransactionFindingsPanel()
    {
        return "//div[@id='control_EA8F8DFA-A1BE-4891-9B68-0712F0AA8995']";
    }

    public static String waterTransactionRecordNumber_xpath()
    {
        return "(//div[@id='form_F517B4C9-16D1-4734-9DAB-8AEDA8426F53']//div[contains(text(),'- Record #')])[1]";
    }

    public static String levelReadingsPanel()
    {
        return "//span[text()='Level Readings']";
    }

    public static String measurementsPanel()
    {
        return "//span[text()='Measurements']";
    }

    //FR1 OS
    public static String uploadDocument()
    {
        return "//div[@id='control_C440202B-AFE6-4EB9-B180-57BBF25C57D6']//b[@original-title='Upload a document']";
    }

    public static String linkToADocument()
    {
        return "//div[@id='control_C440202B-AFE6-4EB9-B180-57BBF25C57D6']//b[@original-title='Link to a document']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String iframeName()
    {
        return "ifrMain";
    }

    //FR2
    public static String parameterReadingsAddButton()
    {
        return "//div[@id='control_68472F51-EB8F-4C07-8241-B0117B849667']//div[text()='Add']";
    }

    public static String parameterReadingsProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_157CB51D-1345-4FC1-B081-C5D8B63AF413']";
    }

    public static String PRprocessFlowAddText()
    {
        return "(//div[text()='Add phase'])[3]";
    }

    public static String parameterDropdown()
    {
        return "//div[@id='control_F11D69C1-F470-4A98-AA57-C7DA50E56DA4']//li";
    }

    public static String measurementTextbox(int i)
    {
        return "(//div[contains(@class,'k-reorderable editable')]//div[@name='txb_n2'])["+i+"]//div//div//input[@class='txt']";
    }

    public static String sampleDateXpath(int i)
    {
        return "(//div[contains(@class,'k-reorderable editable')]//input[@data-role='datepicker'])["+i+"]";
    }

    public static String sampleTakenByTextbox()
    {
        return "(//div[@id='control_293D62D6-7F8B-4F4C-935C-8D9E57CB23B0']//input)[1]";
    }

    public static String commentsTextbox(int i)
    {
        return "(//div[contains(@class,'k-reorderable editable')]//textarea[@class='txt inline-multiline translatable'])["+i+"]";
    }

    public static String PRsaveButton()
    {
        return "//div[@id='btnSave_form_157CB51D-1345-4FC1-B081-C5D8B63AF413']";
    }

    public static String PRprocessFlowEditText()
    {
        return "(//div[text()='Edit phase'])[3]";
    }

    public static String parameterReadingsRecordNumber_xpath()
    {
        return "(//div[@id='form_157CB51D-1345-4FC1-B081-C5D8B63AF413']//div[contains(text(),'- Record #')])[1]";
    }

    //FR3
    public static String measurementsAddButton()
    {
        return "//div[@id='control_4A032425-32B1-4E08-B0F7-E86D5ACABF83']//div[text()='Add']";
    }

    public static String measurementsProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_E60DE3EB-1D9E-4A62-9B93-FADE792523F2']";
    }

    public static String QMprocessFlowAddText()
    {
        return "(//div[text()='Add phase'])[3]";
    }

    public static String QMmonitoringPointDropdown()
    {
        return "//div[@id='control_A6D81C10-F563-40CE-83BF-19B9B5F432F4']//li";
    }

    public static String QMmeasurementTextbox()
    {
        return "(//div[@id='control_0F289C2C-8DDB-4551-A052-E90F299940E2']//input)[1]";
    }

    public static String unitDropdown()
    {
        return "//div[@id='control_E743CAC3-3341-4CB7-9EF3-4863E15E63DA']//li";
    }

    public static String QMsaveButton()
    {
        return "//div[@id='btnSave_form_E60DE3EB-1D9E-4A62-9B93-FADE792523F2']";
    }

    public static String QMprocessFlowEditText()
    {
        return "(//div[text()='Edit phase'])[3]";
    }

    public static String quantityMeasurementRecordNumber_xpath()
    {
        return "(//div[@id='form_E60DE3EB-1D9E-4A62-9B93-FADE792523F2']//div[contains(text(),'- Record #')])[1]";
    }

    //FR4
    public static String waterLevelsAddButton()
    {
        return "//div[@id='control_71CD3BBA-DEF6-47DF-BD73-6BBAB501AF2B']//div[text()='Add']";
    }

    public static String WLmonitoringPointDropdown()
    {
        return "//div[@id='control_77375701-5318-45BF-9AF5-49350A441A62']//li";
    }

    public static String levelInputTextbox()
    {
        return "(//div[@id='control_94F6F331-C4DF-4BFF-A225-72FF7DFF65C2']//input)[1]";
    }

    public static String waterTransactionsaveButton()
    {
        return "//div[@id='btnSave_form_F517B4C9-16D1-4734-9DAB-8AEDA8426F53']";
    }

    //FR5
    public static String findingsAddButton()
    {
        return "//div[@id='control_ABB0D9CA-1CE8-4048-91AF-3F49CE7F3FA1']//div[text()='Add']";
    }

    public static String findingsProcessflow()
    {
        return "//div[@id='btnProcessFlow_form_D02673BB-369F-4EB7-906D-3C76FD3B0897']";
    }

    public static String findingsProcessFlowAddText()
    {
        return "(//div[text()='Add phase'])[3]";
    }

    public static String Findings_desc()
    {
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }

    public static String Findings_owner_dropdown()
    {
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']//li";
    }

    public static String Findings_owner_select(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[1]";
    }

    public static String Findings_risksource_dropdown()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//li";
    }

    public static String Findings_risksource_select(String option)
    {
        return "(//div[contains(@class,'transition visible')]//li[@title='" + option + "']//i[@class='jstree-icon jstree-ocl'])[1]";
    }

    public static String Findings_risksource_select2(String text)
    {
        return "(//a[text()='" + text + "']/i[1])";
    }

    public static String Findings_risksource_arrow()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//b[@class='select3-down drop_click']";
    }

    public static String Findings_class_dropdown()
    {
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']//li";
    }

    public static String Findings_class_select(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[1]";
    }

    public static String Findings_risk_dropdown()
    {
        return "//div[@id='control_1D7F3DE4-F9C4-4626-80C9-0E600A0A8FF8']//li";
    }

    public static String Findings_risk_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String Findings_stc()
    {
        return "(//div[text()='Save to continue'])[2]";
    }

    public static String findingsProcessFlowEditText()
    {
        return "(//div[text()='Edit phase'])[3]";
    }

    public static String findingsRecordNumber_xpath()
    {
        return "(//div[@id='form_D02673BB-369F-4EB7-906D-3C76FD3B0897']//div[contains(text(),'- Record #')])[1]";
    }

    public static String EnvironmentalSustainabilitLabel()
    {
        return "//label[text()='Environmental Sustainability']/..//i";
    }

    public static String WaterMonitoringTab()
    {
        return "//label[text()='Water Monitoring']";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String BusinessUnitDropDownOption(String text)
    {
        return "(//div[contains(@class,'transition visible')]//ul[@class='jstree-children']//a[text()='" + text + "'])[1]";
    }

    public static String BusinessUnitDropDownOption1()
    {
        return "(//a[text()='Base Metals']/..//i)[1]";
    }

    public static String LinkToSite()
    {
        return "//div[@id='control_9C1B43B9-A328-4BE0-9976-B44107BF56D9']";
    }

    public static String LinkToProjects()
    {
        return "//div[@id='control_A6816DC7-08FC-4758-9725-2B804AA05EC5']";
    }

    public static String LinkToSiteDropDown()
    {
        return "//div[@id='control_E37D1E64-0419-4224-9F55-C9B39A9688FC']";
    }

    public static String LinkToProjectsDropDown()
    {
        return "//div[@id='control_83BF57EA-12CC-4366-8FF8-EFE002834C08']";
    }

    public static String linkADoc_buttonxpath()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    
     public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }
    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }
    
      public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }
    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }
      public static String getActionRecord()
    {
        return "//div[@class='record']";
    }
      
      public static String recordSaved_popup_2(){
          return"(//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')])[2]";
      }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String projectTitle()
    {
        return "IsoMetrix";
    }

    public static String AuditManagementLabel()
    {
        return "//label[text()='Audit Management']";
    }

    public static String SampleTakenByDropDown()
    {
        return "//div[@id='control_79E5D7E3-FE75-4E2A-BE85-2DB98ABB4B05']//li";
    }

    public static String InputOutDropdown(int i)
    {
       return "(//div[contains(@class,'k-reorderable editable')]//div[@name='ddl9'])["+i+"]";
    }

    public static String UnitDropdown(int i)
    {
       return "(//div[contains(@class,'k-reorderable editable')]//div[@name='ddl22'])["+i+"]";
    }

    public static String InputOutputDropdown()
    {
        return"//div[@id='control_4144FC6E-0CF0-45B5-B388-041977308C39']";
    }

    public static String LevelMeasureDropdown()
    {
      return  "//div[@id='control_63459111-4A32-4382-9EC2-FBC106421B10']";
    }

    public static String waterTransactionFindingsDropDown()
    {
       return "//div[@id='control_EA8F8DFA-A1BE-4891-9B68-0712F0AA8995']//i[@class='toggle']";
    }

    public static String ParameterTable()
    {
        return"//div[contains(@class,'k-reorderable editable')]//i[@class='icon bin icon ten six grid-icon-delete']";
    }

    public static String ParameterTableDeleteButton(int i)
    {
        return"(//div[contains(@class,'k-reorderable editable')]//i[@class='icon bin icon ten six grid-icon-delete'])["+i+"]";
    }

    public static String MeasurementTextBox(int i)
    {
        return"(//div[contains(@class,'k-reorderable editable')]//div[@name='txb_n2'])["+i+"]";
    }
     public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }
    
     public static String MeasurementTextBox()
    {
        return"(//div[contains(@class,'k-reorderable editable')]//div[@name='txb_n2'])";
    }

    public static String SearchOption()
    {
        return"//span[@class='advanced-options icon more_horiz transition visible']";
    }

    public static String businessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String TeamNameDropDown()
    {
        return"//div[@id='control_24680815-BC19-42E5-BF9C-9D524848831D']//ul";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.AdHocNonComplianceIntervention_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.AdHocNonComplianceIntervention_PageObjects.AdHocNonComplianceIntervention_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR2–Capture Non-Compliance Intervention details Main Scenario",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class FR2_CaptureNonComplianceInterventionDetails_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_CaptureNonComplianceInterventionDetails_MainScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NonComplianceInterventionDetails())
        {
            return narrator.testFailed("Failed To Capture Ad-Hoc Non-Compliance Intervention Due To :" + error);
        }
        if (getData("Execute").equalsIgnoreCase("True"))
        {
            uploadSupportingDocuments();
        }

        if (getData("Person responsible for non-compliance intervention upliftment").equalsIgnoreCase("True"))
        {
            PersonResponsibleForNonComplianceInterventionUpliftment();
        }

        return narrator.finalizeTest("Successfully Captured Ad-Hoc Non-Compliance Intervention");
    }

    public boolean NonComplianceInterventionDetails()
    {

        //
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.DoesThisRelateToAPUEDropDown()))
        {
            error = "Failed to wait for Does this relate to a PUE dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.DoesThisRelateToAPUEDropDown()))
        {
            error = "Failed to click Does this relate to a PUE dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Does this relate to a PUE text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2(), getData("Does this relate to a PUE")))
        {
            error = "Failed to wait for Does this relate to a PUE option :" + getData("Does this relate to a PUE");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.businessUnitOption1(getData("Does this relate to a PUE"))))
        {
            error = "Failed to wait for Does this relate to a PUE :" + getData("Does this relate to a PUE?");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Text(getData("Does this relate to a PUE"))))
        {
            error = "Failed to click Does this relate to a PUE Option :" + getData("Does this relate to a PUE");
            return false;
        }

        narrator.stepPassedWithScreenShot("Does this relate to a PUE :" + getData("Does this relate to a PUE"));

        if (getData("Does this relate to a PUE").endsWith("Yes"))
        {
            //Priority unwanted event
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.PriorityUnwantedEventDropDown()))
            {
                error = "Failed to wait for Priority unwanted event dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.PriorityUnwantedEventDropDown()))
            {
                error = "Failed to click Priority unwanted event dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Priority unwanted event text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2(), getData("Priority unwanted event")))
            {
                error = "Failed to wait for Priority unwanted event option :" + getData("Priority unwanted event");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Priority unwanted event"))))
            {
                error = "Failed to wait for Priority unwanted event:" + getData("Priority unwanted event");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Priority unwanted event"))))
            {
                error = "Failed to click Does this relate to a PUE Option :" + getData("Priority unwanted event");
                return false;
            }

            narrator.stepPassedWithScreenShot("Priority unwanted event :" + getData("Priority unwanted event"));

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        AdHocNonComplianceIntervention_PageObjects.setRecord_Number(record[2]);
        String record_ = AdHocNonComplianceIntervention_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

    public boolean uploadSupportingDocuments()
    {

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.linkADoc_button()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(AdHocNonComplianceIntervention_PageObjects.linkADoc_button()))
        {
            error = "Failed to scroll to 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.linkADoc_button()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.urlInput_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.urlInput_TextAreaxpath(), getData("Document Link")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.tile_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.tile_TextAreaxpath(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(AdHocNonComplianceIntervention_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        AdHocNonComplianceIntervention_PageObjects.setRecord_Number(record[2]);
        String record_ = AdHocNonComplianceIntervention_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;

    }

    public boolean PersonResponsibleForNonComplianceInterventionUpliftment()
    {

        //Person responsible for non-compliance intervention upliftment
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.PersonResponsibleForNonCompliancecheckbox()))
        {
            error = "Failed to wait for Person responsible for non-compliance intervention upliftment check box";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.PersonResponsibleForNonCompliancecheckbox()))
        {
            error = "Failed to click Person responsible for non-compliance intervention upliftment check box";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        AdHocNonComplianceIntervention_PageObjects.setRecord_Number(record[2]);
        String record_ = AdHocNonComplianceIntervention_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        if (getData("Execute person responsible drop down").equalsIgnoreCase("True"))
        {
            //Person responsible for non-compliance intervention upliftment

            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.PersonResponsibleDropDown()))
            {
                error = "Failed to wait for Person responsible for non-compliance intervention upliftmentdropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.PersonResponsibleDropDown()))
            {
                error = "Failed to click Person responsible for non-compliance intervention upliftment dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Person responsible for non-compliance intervention upliftment text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch(), getData("Person responsible for non-compliance intervention upliftment option")))
            {
                error = "Failed to wait for Person responsible for non-compliance intervention upliftment option :" + getData("Person responsible for non-compliance intervention upliftment option");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Person responsible for non-compliance intervention upliftment option"))))
            {
                error = "Failed to wait for Person responsible for non-compliance intervention upliftment option :" + getData("Person responsible for non-compliance intervention upliftment option");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Person responsible for non-compliance intervention upliftment option"))))
            {
                error = "Failed to click Person responsible for non-compliance intervention upliftment option :" + getData("Person responsible for non-compliance intervention upliftment option");
                return false;
            }

            narrator.stepPassedWithScreenShot("Person responsible for non-compliance intervention upliftment option :" + getData("Person responsible for non-compliance intervention upliftment option"));

            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
            {
                error = "Failed to wait for button save";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
            {
                error = "Failed to click button save";
                return false;
            }

            saved = "";

            if (SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
            {
                saved = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup());
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
                {
                    error = "Failed to wait for 'Record Saved' popup.";
                    return false;
                }
            }

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            acionRecord = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.getActionRecord());
            record = acionRecord.split(" ");
            AdHocNonComplianceIntervention_PageObjects.setRecord_Number(record[2]);
            record_ = AdHocNonComplianceIntervention_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        }

        return true;

    }

}

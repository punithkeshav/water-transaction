/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.AdHocNonComplianceIntervention_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.AdHocNonComplianceIntervention_PageObjects.AdHocNonComplianceIntervention_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR3–Stop Note Finalization Main Scenario",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class FR3_StopNoteFinalization_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_StopNoteFinalization_MainScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!StopNoteFinalization())
        {
            return narrator.testFailed("Failed To Stop Note Finalization Due To :" + error);
        }
    
        return narrator.finalizeTest("Successfully Captured Stop Note Finalization");
    }

    public boolean StopNoteFinalization()
    {

        
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.DoesThisRelateToAPUEDropDown()))
        {
            error = "Failed to wait for Is the work stoppage lifted dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.DoesThisRelateToAPUEDropDown()))
        {
            error = "Failed to click Is the work stoppage lifted dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Is the work stoppage lifted text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2(), getData("Is the work stoppage lifted")))
        {
            error = "Failed to wait for Is the work stoppage lifted option :" + getData("Is the work stoppage lifted");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Text(getData("Is the work stoppage lifted"))))
        {
            error = "Failed to wait for Is the work stoppage lifted :" + getData("Is the work stoppage lifted");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Text(getData("Is the work stoppage lifted"))))
        {
            error = "Failed to click Is the work stoppage lifted Option :" + getData("Is the work stoppage lifted");
            return false;
        }

        narrator.stepPassedWithScreenShot("Is the work stoppage lifted :" + getData("Is the work stoppage lifted"));

        if (getData("Is the work stoppage lifted").endsWith("Yes"))
        {
            //Date work stoppage lifted
            pause(3000);
           
//            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.DateWorkStoppageLifted()))
//            {
//                error = "Failed to wait for Date work stoppage lifted text box.";
//                return false;
//            }

            if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.DateWorkStoppageLifted(), startDate))
            {
                error = "Failed to enter Date work stoppage lifted :" + startDate;
                return false;
            }

            narrator.stepPassedWithScreenShot("Date work stoppage lifted :" + startDate);

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";
        pause(2000);

        if (SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        AdHocNonComplianceIntervention_PageObjects.setRecord_Number(record[2]);
        String record_ = AdHocNonComplianceIntervention_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

   

}

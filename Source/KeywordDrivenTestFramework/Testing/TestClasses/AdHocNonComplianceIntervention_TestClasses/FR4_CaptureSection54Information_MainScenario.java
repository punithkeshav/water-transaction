/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.AdHocNonComplianceIntervention_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.AdHocNonComplianceIntervention_PageObjects.AdHocNonComplianceIntervention_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR4–Capture Section 54 Information Main Scenario",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class FR4_CaptureSection54Information_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_CaptureSection54Information_MainScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureSection54Information())
        {
            return narrator.testFailed("Failed To Capture Section 54 Information Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Capture Section 54 Information");
    }

    public boolean CaptureSection54Information()
    {

        //Production shifts lost
        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.ProductiOnShiftsLost(), ""+12))
        {
            error = "Failed to enter Production shifts lost :" + ""+12;
            return false;
        }

        //Units lost
        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.UnitsLost(), ""+12))
        {
            error = "Failed to enter Units lost:" + ""+12;
            return false;
        }

        //Commodity lost
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.DoesThisRelateToAPUEDropDown()))
        {
            error = "Failed to wait for Commodity lost dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.DoesThisRelateToAPUEDropDown()))
        {
            error = "Failed to click Commodity lost dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Commodity lost text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2(), getData("Commodity lost")))
        {
            error = "Failed to wait for Commodity lost option :" + getData("Commodity lost");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Commodity lost"))))
        {
            error = "Failed to wait for Is the work stoppage lifted :" + getData("Is the work stoppage lifted");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Commodity lost"))))
        {
            error = "Failed to click Commodity lost Option :" + getData("Commodity lost");
            return false;
        }

        narrator.stepPassedWithScreenShot("Commodity lost :" + getData("Commodity lost"));

        //Date work stoppage lifted
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.DateUpliftedByDMROffice()))
        {
            error = "Failed to wait for Date uplifted by DMR office text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.DateUpliftedByDMROffice(), startDate))
        {
            error = "Failed to enter Date uplifted by DMR office :" + startDate;
            return false;
        }

        narrator.stepPassedWithScreenShot("Date uplifted by DMR office :" + startDate);
        
        
        //Commitments made to DMR during upliftment presentation
        
           if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.CommitmentsMadeToDMR()))
        {
            error = "Failed to wait for Commodity lost text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.CommitmentsMadeToDMR(), getData("Commitments made to DMR during upliftment presentation")))
        {
            error = "Failed to enter Commitments made to DMR during upliftment presentation :" + getData("Commitments made to DMR during upliftment presentation");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Commitments made to DMR during upliftment presentation :"+ getData("Commitments made to DMR during upliftment presentation"));

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        AdHocNonComplianceIntervention_PageObjects.setRecord_Number(record[2]);
        String record_ = AdHocNonComplianceIntervention_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}

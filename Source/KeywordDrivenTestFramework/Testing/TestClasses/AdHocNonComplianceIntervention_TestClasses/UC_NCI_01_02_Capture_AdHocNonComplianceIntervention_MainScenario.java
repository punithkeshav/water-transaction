/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.AdHocNonComplianceIntervention_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.AdHocNonComplianceIntervention_PageObjects.AdHocNonComplianceIntervention_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "UC NCI 01-02: Capture Non-Compliance Intervention",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class UC_NCI_01_02_Capture_AdHocNonComplianceIntervention_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_NCI_01_02_Capture_AdHocNonComplianceIntervention_MainScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureTheAdHocNonComplianceIntervention_())
        {
            return narrator.testFailed("Failed To Capture Ad-Hoc Non-Compliance Intervention Due To :" + error);
        }
        if (getData("Execute").equalsIgnoreCase("True"))
        {
            uploadSupportingDocuments();
        }

        return narrator.finalizeTest("Successfully Captured Ad-Hoc Non-Compliance Intervention");
    }

    public boolean CaptureTheAdHocNonComplianceIntervention_()
    {
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.AdHocNonComplianceInterventionAdd()))
        {
            error = "Failed to wait for Ad-Hoc Non-Compliance Intervention add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.AdHocNonComplianceInterventionAdd()))
        {
            error = "Failed to click Ad-Hoc Non-Compliance Intervention add button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked  Ad-Hoc Non-Compliance Intervention add button");

        pause(8000);

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.ProcessFlowButton2()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.ProcessFlowButton2()))
        {
            error = "Failed to click the process flow button";
            return false;
        }

        //Date of non-compliance
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.DateOfNonCompliance()))
        {
            error = "Failed to wait for Date of non-compliance text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.DateOfNonCompliance(), startDate))
        {
            error = "Failed to enter Date of non-compliance :" + startDate;
            return false;
        }

        // Reported date of non-compliance
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.ReportedDateOfNonCompliance()))
        {
            error = "Failed to wait for Reported Date of non-compliance text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.ReportedDateOfNonCompliance(), startDate))
        {
            error = "Failed to enter Reported Date of non-compliance :" + startDate;
            return false;
        }

        //Reported by
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.ReportedByDropDown()))
        {
            error = "Failed to wait for Reported by dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.ReportedByDropDown()))
        {
            error = "Failed to click Reported by dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for  text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch(), getData("Reported by")))
        {
            error = "Failed to wait for  :" + getData("Reported by");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(60000);
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Reported by"))))
        {
            error = "Failed to wait for Reported by drop down option : " + getData("Reported by") +" "+"After 60 seconds";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Reported by"))))
        {
            error = "Failed to click  Reported by drop down option : " + getData("Reported by");
            return false;
        }

        narrator.stepPassedWithScreenShot("Reported by :" + getData("Reported by"));

       

        //Person responsible for non-compliance intervention upliftment
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.PersonResponsibleDropDown()))
        {
            error = "Failed to wait for Person responsible for non-compliance intervention upliftment dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.PersonResponsibleDropDown()))
        {
            error = "Failed to click Person responsible for non-compliance intervention upliftment dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Person responsible for non-compliance intervention upliftment text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch(), getData("Person responsible for non-compliance intervention upliftment")))
        {
            error = "Failed to enter Person responsible for non-compliance intervention upliftment :" + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Person responsible for non-compliance intervention upliftment"))))
        {
            error = "Failed to wait for Person responsible for non-compliance intervention upliftment drop down option : " + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Person responsible for non-compliance intervention upliftment"))))
        {
            error = "Failed to click  Person responsible for non-compliance intervention upliftment drop down option : " + getData("Person responsible for non-compliance intervention upliftment");
            return false;
        }
        narrator.stepPassedWithScreenShot("Person responsible for non-compliance intervention upliftment :" + getData("Person responsible for non-compliance intervention upliftment"));

        
         //Non-compliance intervention issued to
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.NonComplianceInterventionDropDown()))
        {
            error = "Failed to wait for Non-compliance intervention issued to dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.NonComplianceInterventionDropDown()))
        {
            error = "Failed to click Non-compliance intervention issued to dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Non-compliance intervention text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2(), getData("Non-compliance intervention issued to")))
        {
            error = "Failed to enter  Non-compliance intervention issued to :" + getData("Non-compliance intervention issued to");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Non-compliance intervention issued to"))))
        {
            error = "Failed to wait for Non-compliance intervention issued to drop down option : " + getData("Non-compliance intervention issued to");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Non-compliance intervention issued to"))))
        {
            error = "Failed to click  Non-compliance intervention issued to drop down option : " + getData("Non-compliance intervention issued to");
            return false;
        }

        narrator.stepPassedWithScreenShot("Non-compliance intervention issued to :" + getData("Non-compliance intervention issued to"));
        //Type of non-compliance intervention
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeOfNonComplianceDropDown()))
        {
            error = "Failed to wait for Type of non-compliance intervention dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.TypeOfNonComplianceDropDown()))
        {
            error = "Failed to click Type of non-compliance intervention dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for  text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2(), getData("Type of non-compliance intervention")))
        {
            error = "Failed to wait for Type of non-compliance intervention :" + getData("Type of non-compliance intervention");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Type of non-compliance intervention"))))
        {
            error = "Failed to wait for Type of non-compliance intervention drop down option : " + getData("Type of non-compliance intervention");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Type of non-compliance intervention"))))
        {
            error = "Failed to click  Type of non-compliance intervention drop down option : " + getData("Type of non-compliance intervention");
            return false;
        }

        narrator.stepPassedWithScreenShot("Type of non-compliance intervention :" + getData("Type of non-compliance intervention"));

        //Stoppage initiated by
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.StoppageInitiatedDropDown()))
        {
            error = "Failed to wait for Stoppage initiated by dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.StoppageInitiatedDropDown()))
        {
            error = "Failed to click Stoppage initiated by dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Stoppage initiated by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2(), getData("Stoppage initiated by")))
        {
            error = "Failed to wait for Stoppage initiated by :" + getData("Stoppage initiated by");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Stoppage initiated by"))))
        {
            error = "Failed to wait for Stoppage initiated by drop down option : " + getData("Stoppage initiated by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Stoppage initiated by"))))
        {
            error = "Failed to click  Stoppage initiated by drop down option : " + getData("Stoppage initiated by");
            return false;
        }

        narrator.stepPassedWithScreenShot("Stoppage initiated by :" + getData("Stoppage initiated by"));

        //Work stoppage
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.WorkStoppageDropDown()))
        {
            error = "Failed to wait for  dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.WorkStoppageDropDown()))
        {
            error = "Failed to click  dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for  text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2(), getData("Work stoppage")))
        {
            error = "Failed to wait for Work stoppage :" + getData("Work stoppage");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Work stoppage"))))
        {
            error = "Failed to wait for Work stoppage drop down option : " + getData("Work stoppage");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Work stoppage"))))
        {
            error = "Failed to click  Work stoppage drop down option : " + getData("Work stoppage");
            return false;
        }
        narrator.stepPassedWithScreenShot("Work stoppage :" + getData("Work stoppage"));

        //Description of reason for work stoppage
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Description()))
        {
            error = "Failed to wait for Description of reason for work stoppage text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.Description(), getData("Description of reason for work stoppage")))
        {
            error = "Failed to enter  Description of reason for work stoppage :" + getData("Description of reason for work stoppage");
            return false;
        }
        narrator.stepPassedWithScreenShot("Description of reason for work stoppage :" + getData("Description of reason for work stoppage"));

        //Stop note classification
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.StopNoteClassificationDropDown()))
        {
            error = "Failed to wait for Stop note classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.StopNoteClassificationDropDown()))
        {
            error = "Failed to click Stop note classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Stop note classification text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2(), getData("Stop note classification")))
        {
            error = "Failed to wait for Stop note classification :" + getData("Stop note classification");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Stop note classification"))))
        {
            error = "Failed to wait for Stop note classification drop down option : " + getData("Stop note classification");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Stop note classification"))))
        {
            error = "Failed to click  Stop note classification drop down option : " + getData("Stop note classification");
            return false;
        }
        narrator.stepPassedWithScreenShot("Stop note classification :" + getData("Stop note classification"));

        //Stoppage due to
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.StoppageDueToDropDown()))
        {
            error = "Failed to wait for Stoppage due to dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.StoppageDueToDropDown()))
        {
            error = "Failed to click Stoppage due to dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for  text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2(), getData("Stoppage due to")))
        {
            error = "Failed to wait for Stoppage due to :" + getData("Stoppage due to");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Check_box(getData("Stoppage due to"))))
        {
            error = "Failed to wait for Project :" + getData("Stoppage due to");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Check_box(getData("Stoppage due to"))))
        {
            error = "Failed to click Project :" + getData("Stoppage due to");
            return false;
        }
        narrator.stepPassedWithScreenShot("Stoppage due to :" + getData("Stoppage due to"));

        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.StoppageDueToDropDown()))
        {
            error = "Failed to click Stoppage due to dropdown.";
            return false;
        }

        //Stoppage outcome
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.StoppageOutcomeDropDown()))
        {
            error = "Failed to wait for  dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.StoppageOutcomeDropDown()))
        {
            error = "Failed to click  dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for  text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.TypeSearch2(), getData("Stoppage outcome")))
        {
            error = "Failed to wait for Stoppage outcome :" + getData("Stoppage outcome");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(AdHocNonComplianceIntervention_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Stoppage outcome"))))
        {
            error = "Failed to wait for Stoppage outcome drop down option : " + getData("Stoppage outcome");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Text2(getData("Stoppage outcome"))))
        {
            error = "Failed to click  Stoppage outcome drop down option : " + getData("Stoppage outcome");
            return false;
        }
        narrator.stepPassedWithScreenShot("Stoppage outcome :" + getData("Stoppage outcome"));

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save2()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        AdHocNonComplianceIntervention_PageObjects.setRecord_Number(record[2]);
        String record_ = AdHocNonComplianceIntervention_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

    public boolean uploadSupportingDocuments()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to wait for Supporting Documents button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to click on Supporting Documents button.";
            return false;
        }

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.linkADoc_button()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(AdHocNonComplianceIntervention_PageObjects.linkADoc_button()))
        {
            error = "Failed to scroll to 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.linkADoc_button()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.urlInput_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.urlInput_TextAreaxpath(), getData("Document Link")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.tile_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(AdHocNonComplianceIntervention_PageObjects.tile_TextAreaxpath(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(AdHocNonComplianceIntervention_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AdHocNonComplianceIntervention_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(AdHocNonComplianceIntervention_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(AdHocNonComplianceIntervention_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        AdHocNonComplianceIntervention_PageObjects.setRecord_Number(record[2]);
        String record_ = AdHocNonComplianceIntervention_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;

    }

}

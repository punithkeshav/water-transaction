/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.BiodiversityMonitoring;

import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Biodiversity_Monitoring.Biodiversity_Monitoring;
import KeywordDrivenTestFramework.Testing.PageObjects.IsometricsPOCPageObjects;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR1- Capture Biodiversity Monitoring",
        createNewBrowserInstance = false
)

public class FR1_CaptureBiodiversityMonitoring_MainScenario extends BaseClass
{

    String error = "";

    public FR1_CaptureBiodiversityMonitoring_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!CaptureWaterMonitoring())
        {
            return narrator.testFailed("Failed To Biodiversity Monitoring Due To : " + error);
        }

        if (getData("Upload Document").equalsIgnoreCase("True"))
        {

            if (!UploadDocument())
            {
                return narrator.testFailed("Failed to upload document: " + error);
            }
        }
        return narrator.finalizeTest("Successfully Capture Biodiversity Monitoring record");
    }

    public boolean CaptureWaterMonitoring()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.EnvironmentalSustainabilitLabel()))
        {
            error = "Failed to wait for 'Environmental Sustainabilit'";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.EnvironmentalSustainabilitLabel()))
        {
            error = "Failed to click on 'Environmental Sustainabilit'";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Environmental Sustainabilit'");

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.BiodiversityMonitoringTab()))
        {
            error = "Failed to wait for 'Biodiversity Monitoring'";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.BiodiversityMonitoringTab()))
        {
            error = "Failed to click on 'Biodiversity Monitoring'";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Biodiversity Monitoring");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.AddButton()))
        {
            error = "Failed to locate add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.AddButton()))
        {
            error = "Failed to click on add button";
            return false;
        }

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow in Add phase");

        //Business Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.businessUnitDropdown()))
        {
            error = "Failed to locate Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.businessUnitDropdown()))
        {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to wait for Business Unit :" + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        pause(4000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait to expand Business Unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to expand Business Unit";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.businessUnitOption1(getData("Business unit 2"))))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.businessUnitOption1(getData("Business unit"))))
            {
                error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 2");
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.businessUnitOption1(getData("Business unit 2"))))
            {
                error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 2");
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text3(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.Text3(getData("Business unit option"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Monitoring point
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.MonitoringPointDropDown()))
        {
            error = "Failed to wait for Monitoring point.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.MonitoringPointDropDown()))
        {
            error = "Failed to click Monitoring point";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.TypeSearch2(), getData("Monitoring point")))

        {
            error = "Failed to enter Monitoring point :" + getData("Monitoring point");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text2(getData("Monitoring point"))))
        {
            error = "Failed to wait for Monitoring point drop down option : " + getData("Monitoring point");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Biodiversity_Monitoring.Text2(getData("Monitoring point"))))
        {
            error = "Failed to click Monitoring point drop down option : " + getData("Monitoring point");
            return false;
        }
        narrator.stepPassedWithScreenShot("Monitoring point");

        //Monitoring type
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.MonitoringTypeDropDown()))
        {
            error = "Failed to wait for Monitoring type.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.MonitoringTypeDropDown()))
        {
            error = "Failed to click Monitoring type";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.TypeSearch2(), getData("Monitoring type")))

        {
            error = "Failed to enter Monitoring type :" + getData("Monitoring type");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text2(getData("Monitoring type"))))
        {
            error = "Failed to wait for Monitoring type drop down option : " + getData("Monitoring type");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Biodiversity_Monitoring.Text2(getData("Monitoring type"))))
        {
            error = "Failed to click Monitoring type drop down option : " + getData("Monitoring type");
            return false;
        }
        narrator.stepPassedWithScreenShot("Monitoring type");

        //Month/Year
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.MonthDropDown()))
        {
            error = "Failed to wait for Month month drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.MonthDropDown()))
        {
            error = "Failed to click Month month date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.TypeSearch2(), getData("Month")))

        {
            error = "Failed to enter Month month :" + getData("Month month");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text2(getData("Month"))))
        {
            error = "Failed to wait for Month month drop down option : " + getData("Month");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Biodiversity_Monitoring.Text2(getData("Month"))))
        {
            error = "Failed to click Month month drop down option : " + getData("Month");
            return false;
        }
        narrator.stepPassedWithScreenShot("Month");

        //year
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.YearDropDown()))
        {
            error = "Failed to wait for Year year drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.YearDropDown()))
        {
            error = "Failed to click year date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.TypeSearch2(), getData("Year")))

        {
            error = "Failed to enter Year date year :" + getData("Year");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text2(getData("Year"))))
        {
            error = "Failed to wait for Month/Year year drop down option : " + getData("Year");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Biodiversity_Monitoring.Text2(getData("Year"))))
        {
            error = "Failed to click Month/Year year drop down option : " + getData("Year");
            return false;
        }
        narrator.stepPassedWithScreenShot("Year :" + getData("Year"));

        if (getData("Link to environmental permit execute").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.LinkToEnvironmentalPermitCheckBox()))
            {
                error = "Failed to wait for Link to environmental permit check box";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.LinkToEnvironmentalPermitCheckBox()))
            {
                error = "Failed to click Link to environmental permit check box";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.LinkToEnvironmentalPermitDropdown()))
            {
                error = "Failed to wait for  Link to environmental permit Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.LinkToEnvironmentalPermitDropdown()))
            {
                error = "Failed to click on Link to environmental permit Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.TypeSearch2()))
            {
                error = "Failed to wait for Business Unit text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.TypeSearch2(), getData("Link to environmental permit")))
            {
                error = "Failed to wait for Link to environmental permit :" + getData("Link to environmental permit");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(Biodiversity_Monitoring.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text2(getData("Link to environmental permit"))))
            {
                error = "Failed to wait for Month/Year year drop down option : " + getData("Link to environmental permit");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.Text2(getData("Link to environmental permit"))))
            {
                error = "Failed to Link to environmental permit drop down option : " + getData("Link to environmental permit");
                return false;
            }
            narrator.stepPassedWithScreenShot("Link to environmental permit :" + getData("Link to environmental permit"));

        }

        if (getData("Link to project execute").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.LinkToEnvironmentalPermitCheckBox()))
            {
                error = "Failed to wait for Link to project check box";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.LinkToProjectCheckBox()))
            {
                error = "Failed to click Link to project check box";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.LinkToProjectDropdown()))
            {
                error = "Failed to wait for  Link to project Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.LinkToProjectDropdown()))
            {
                error = "Failed to click on Link to project Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.TypeSearch2()))
            {
                error = "Failed to wait for Business Unit text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.TypeSearch2(), getData("Link to project")))
            {
                error = "Failed to wait for Link to project :" + getData("Link to project");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(Biodiversity_Monitoring.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text2(getData("Link to project"))))
            {
                error = "Failed to wait for Link to project drop down option : " + getData("Link to project");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.Text2(getData("Link to project"))))
            {
                error = "Failed to click Link to project drop down option : " + getData("Link to project");
                return false;
            }
            narrator.stepPassedWithScreenShot("Link to project :" + getData("Link to project"));

        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.getActionRecord());
        String[] record = acionRecord.split(" ");
        Biodiversity_Monitoring.setRecord_Number(record[2]);
        String record_ = Biodiversity_Monitoring.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;
    }

    public boolean UploadDocument() throws InterruptedException
    {

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.linkADoc_button()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(Biodiversity_Monitoring.linkADoc_button()))
        {
            error = "Failed to scroll to 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.linkADoc_button()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.urlInput_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.urlInput_TextAreaxpath(), getData("Document Link")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.tile_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.tile_TextAreaxpath(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Biodiversity_Monitoring.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.getActionRecord());
        String[] record = acionRecord.split(" ");
        Biodiversity_Monitoring.setRecord_Number(record[2]);
        String record_ = Biodiversity_Monitoring.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;

    }

}

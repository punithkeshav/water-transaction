/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.BiodiversityMonitoring;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Biodiversity_Monitoring.Biodiversity_Monitoring;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Biodiversity Measurement - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_CaptureBiodiversityMeasurement_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR3_CaptureBiodiversityMeasurement_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!BiodiversityMeasurements())
        {
            return narrator.testFailed("Capture Biodiversity Measurement - Flora Failed Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Biodiversity Measurement - Flora");
    }

    public boolean BiodiversityMeasurements()
    {

        //Biodiversity Measurements
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.BiodiversityMeasurements_Dropdown()))
        {
            error = "Failed to wait for Biodiversity Measurements dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.BiodiversityMeasurements_Dropdown()))
        {
            error = "Failed to click Biodiversity Measurements dropdown.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.BiodiversityMeasurementsAdd_button()))
        {
            error = "Failed to wait for Biodiversity Measurements Add  button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.BiodiversityMeasurementsAdd_button()))
        {
            error = "Failed to click on Biodiversity Measurements Add button.";
            return false;
        }

        narrator.stepPassed("Successfully Clicked Biodiversity Measurements Add");

        //Monitoring on
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.MonitoringOn_Dropdown()))
        {
            error = "Failed to wait for Monitoring on dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.MonitoringOn_Dropdown()))
        {
            error = "Failed to click Monitoring on dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to wait to Monitoring on text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.TypeSearch2(), getData("Monitoring on")))
        {
            error = "Failed to enter Monitoring on";
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text2(getData("Monitoring on"))))
        {
            error = "Failed to wait for Monitoring on:" + getData("Monitoring on");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.Text2(getData("Monitoring on"))))
        {
            error = "Failed to click Monitoring on Option drop down :" + getData("Monitoring on");
            return false;
        }
        narrator.stepPassedWithScreenShot("Monitoring on" + getData("Monitoring on"));

        //Parameter
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Parameter_Dropdown()))
        {
            error = "Failed to wait for Parameter dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.Parameter_Dropdown()))
        {
            error = "Failed to click Parameter dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.TypeSearch()))
        {
            error = "Failed to wait to Parameter text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.TypeSearch(), getData("Parameter option")))
        {
            error = "Failed to enter Monitoring on";
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Biodiversity_Monitoring.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.businessUnitOption1(getData("Parameter 1"))))
        {
            error = "Failed to wait for Emission source:" + getData("Parameter 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.businessUnitOption1(getData("Parameter 1"))))
        {
            error = "Failed to click Emission source Option drop down :" + getData("Emission source 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text2(getData("Parameter option"))))
        {
            error = "Failed to wait for Parameter option drop down option : " + getData("Parameter option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.Text2(getData("Parameter option"))))
        {
            error = "Failed to click Parameter option drop down option : " + getData("Parameter option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Parameter option :" + getData("Parameter option"));

        //Monitoring taken by
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.MonitoringTakenByDropDown()))
        {
            error = "Failed to wait for Monitoring taken by drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.MonitoringTakenByDropDown()))
        {
            error = "Failed to click Unit drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.TypeSearch()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.TypeSearch(), getData("Monitoring taken by")))

        {
            error = "Failed to enter  Monitoring taken by :" + getData("Monitoring taken by");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Biodiversity_Monitoring.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text2(getData("Monitoring taken by"))))
        {
            error = "Failed to wait for Monitoring taken by drop down option : " + getData("Monitoring taken by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.Text2(getData("Monitoring taken by"))))
        {
            error = "Failed to click Monitoring taken by drop down option : " + getData("Monitoring taken by");
            return false;
        }
        narrator.stepPassedWithScreenShot("Monitoring taken by :" + getData("Monitoring taken by"));

        //Comments
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Comments()))
        {
            error = "Failed to wait for comments text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.Comments(), getData("comments")))
        {
            error = "Failed to enter comments";
            return false;
        }

       

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.saveButton()))
        {
            error = "Failed to wait for Save and Continue.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.saveButton()))
        {
            error = "Failed to click Save and Continue.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.recordSaved_popup1()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.recordSaved_popup1());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.getActionRecord());
        String[] record = acionRecord.split(" ");
        Biodiversity_Monitoring.setRecord_Number(record[2]);
        String record_ = Biodiversity_Monitoring.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        return true;
    }

}

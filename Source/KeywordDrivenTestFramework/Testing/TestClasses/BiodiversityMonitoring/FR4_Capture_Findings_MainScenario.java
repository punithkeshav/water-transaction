/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.BiodiversityMonitoring;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Biodiversity_Monitoring.Biodiversity_Monitoring;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR4 Capture Findings Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_Findings_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_Capture_Findings_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!captureFindings())
        {
            return narrator.testFailed("Capture Findings Failed due - " + error);
        }

        return narrator.finalizeTest("Capture Findings");
    }

    public boolean captureFindings()
    {

        //Management Findings panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.MonitoringFindingsPanel()))
        {
            error = "Failed to locate Monitoring Findings panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.MonitoringFindingsPanel()))
        {
            error = "Failed to expand Monitoring Findings panel";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.findingsAddButton()))
        {
            error = "Failed to wait for Add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.findingsAddButton()))
        {
            error = "Failed to click on Add button.";
            return false;
        }

        //Process Flow
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.findingsProcessflow()))
        {
            error = "Failed to wait for Process Flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.findingsProcessflow()))
        {
            error = "Failed to click Process Flow button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Processflow in Add phase");

        //Findings Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Findings_desc()))
        {
            error = "Failed to wait for Description textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.Findings_desc(), getData("Description")))
        {
            error = "Failed to enter text into Description textarea.";
            return false;
        }

        //Findings Owner dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Findings_owner_dropdown()))
        {
            error = "Failed to wait for Findings Owner dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.Findings_owner_dropdown()))
        {
            error = "Failed to click Findings Owner dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.TypeSearch()))
        {
            error = "Failed to wait for Measurement taken by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.TypeSearch(), getData("Findings Owner")))
        {
            error = "Failed to enter  Findings Owner :" + getData("Findings Owner");
            return false;
        }
     if (!SeleniumDriverInstance.pressEnter_2(Biodiversity_Monitoring.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text2(getData("Findings Owner"))))
        {
            error = "Failed to wait for Findings Owner drop down option : " + getData("Findings Owner");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.Text2(getData("Findings Owner"))))
        {
            error = "Failed to click Findings Owner drop down option : " + getData("Findings Owner");
            return false;
        }

       

        //Findings Classification dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Findings_class_dropdown()))
        {
            error = "Failed to wait for Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.Findings_class_dropdown()))
        {
            error = "Failed to click Findings Classification dropdown.";
            return false;
        }
        //Findings Classification select
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text2(getData("Findings Classification"))))
        {
            error = "Failed to wait for '" + getData("Findings Classification") + "' in Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.Text2(getData("Findings Classification"))))
        {
            error = "Failed to click '" + getData("Findings Classification") + "' from Findings Classification dropdown.";
            return false;
        }
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.SaveButtonFindings()))
        {
            error = "Failed to locate Save button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.SaveButtonFindings()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
       
       //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.getActionRecord());
        String[] record = acionRecord.split(" ");
        Biodiversity_Monitoring.setRecord_Number(record[2]);
        String record_ = Biodiversity_Monitoring.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
  

        return true;
    }

}

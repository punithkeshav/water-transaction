/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.BiodiversityMonitoring;

import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Biodiversity_Monitoring.Biodiversity_Monitoring;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteHomePageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.WaterTransaction_PageObjects;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR5-Delete Biodiversity Monitoring",
        createNewBrowserInstance = false
)
public class FR5_DeleteBiodiversityMonitoring_MainScenario extends BaseClass
{

    String error = "";

    public FR5_DeleteBiodiversityMonitoring_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!DeleteRecord())
        {
            return narrator.testFailed("Failed to delete record :" + error);
        }

        return narrator.finalizeTest("Successfully Deleted Record");
    }

    public boolean DeleteRecord() throws InterruptedException
    {

        //Business Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.businessUnitDropdown()))
        {
            error = "Failed to locate Business Unit Dropdown";
            return false;
        }
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");

        //Search Option
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.SearchOption()))
        {
            error = "Failed to wait for search options";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.SearchOption()))
        {
            error = "Failed to click search options";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter contains to search";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.DeleteButton()))
        {
            error = "Failed click delete button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);

        return true;
    }

}

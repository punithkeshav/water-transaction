/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.BiodiversityMonitoring;

import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Biodiversity_Monitoring.Biodiversity_Monitoring;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteHomePageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.WaterTransaction_PageObjects;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR5-Edit Biodiversity Monitoring",
        createNewBrowserInstance = false
)
public class FR5_EditBiodiversityMonitoring_MainScenario extends BaseClass
{

    String error = "";

    public FR5_EditBiodiversityMonitoring_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!SearchRecord())
        {
            return narrator.testFailed("Failed to edit record  due to :" + error);
        }

        return narrator.finalizeTest("Successfully edited Water Monitoring record");
    }

    public boolean SearchRecord() throws InterruptedException
    {

        //Business Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.businessUnitDropdown()))
        {
            error = "Failed to wait for Business Unit Dropdown";
            return false;
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

   
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");

        //Search Option
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.SearchOption()))
        {
            error = "Failed to wait for search options";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.SearchOption()))
        {
            error = "Failed to click search options";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

        
        pause(4000);
        //Month/Year
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.MonthDropDown()))
        {
            error = "Failed to wait for Month month drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.MonthDropDown()))
        {
            error = "Failed to click Month month date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.TypeSearch2(), getData("Month")))

        {
            error = "Failed to enter Month month :" + getData("Month month");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text2(getData("Month"))))
        {
            error = "Failed to wait for Month month drop down option : " + getData("Month");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Biodiversity_Monitoring.Text2(getData("Month"))))
        {
            error = "Failed to click Month month drop down option : " + getData("Month");
            return false;
        }
        narrator.stepPassedWithScreenShot("Month");

        //year
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.YearDropDown()))
        {
            error = "Failed to wait for Year year drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.YearDropDown()))
        {
            error = "Failed to click year date drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Biodiversity_Monitoring.TypeSearch2(), getData("Year")))

        {
            error = "Failed to enter Year date year :" + getData("Year");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Biodiversity_Monitoring.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.Text2(getData("Year"))))
        {
            error = "Failed to wait for Month/Year year drop down option : " + getData("Year");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Biodiversity_Monitoring.Text2(getData("Year"))))
        {
            error = "Failed to click Month/Year year drop down option : " + getData("Year");
            return false;
        }
        narrator.stepPassedWithScreenShot("Year");

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

       
            //Save button
            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.saveButton()))
            {
                error = "Failed to locate Save button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Biodiversity_Monitoring.saveButton()))
            {
                error = "Failed to click Save button";
                return false;
            }
        

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Biodiversity_Monitoring.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Biodiversity_Monitoring.getActionRecord());
        String[] record = acionRecord.split(" ");
        Biodiversity_Monitoring.setRecord_Number(record[2]);
        String record_ = Biodiversity_Monitoring.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");
       

        return true;
    }

}

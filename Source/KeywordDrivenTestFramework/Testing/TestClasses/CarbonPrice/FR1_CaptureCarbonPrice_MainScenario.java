/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.CarbonPrice;

import KeywordDrivenTestFramework.Testing.TestClasses.EmissionLinking.*;
import KeywordDrivenTestFramework.Testing.TestClasses.BiodiversityMonitoring.*;
import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.CarbonPrice.CarbonPrice_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR1 - Capture Carbon Price Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_CaptureCarbonPrice_MainScenario extends BaseClass
{

    String error = "";

    public FR1_CaptureCarbonPrice_MainScenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!CaptureEmissioLinking())
        {
            return narrator.testFailed("Failed To Capture Emission Linking Due To : " + error);
        }

        return narrator.finalizeTest("Successfully Capture Emission Linking");
    }

    public boolean CaptureEmissioLinking()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.ECO2ManLabel()))
        {
            error = "Failed to wait for ECO2Man";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.ECO2ManLabel()))
        {
            error = "Failed to click on ECO2Man";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked ECO2ManLabel");

        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.MonitoringMaintence()))
        {
            error = "Failed to wait for Monitoring Maintenance";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.MonitoringMaintence()))
        {
            error = "Failed to click on Monitoring Maintenance";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Monitoring Maintenance");
        pause(3000);

        //Carbon Price
        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.CarbonPrice()))
        {
            error = "Failed to wait for Carbon Price";
            return false;
        }

        if (!SeleniumDriverInstance.doubleClickElementbyXpath(CarbonPrice_PageObjects.CarbonPrice()))
        {
            error = "Failed to click on Carbon Price";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Carbon Price");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.AddButton()))
        {
            error = "Failed to wait for the add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.AddButton()))
        {
            error = "Failed to click on add button";
            return false;
        }

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow in Add phase");

        //Business Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to locate Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for Business Unit text box.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(CarbonPrice_PageObjects.TypeSearch2(), getData("Business unit option")))
//        {
//            error = "Failed to wait for Business Unit :" + getData("Business unit option");
//            return false;
//        }
//        if (!SeleniumDriverInstance.pressEnter_2(CarbonPrice_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to press enter";
//            return false;
//        }

        pause(5000);
//        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Business unit"))))
//        {
//            error = "Failed to wait to expand Business Unit";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Business unit"))))
//        {
//            error = "Failed to expand Business Unit";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.CheckBox(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.CheckBox(getData("Business unit option"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }

        pause(3000);
        //Emission factor database
        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.EmissionSourceDropDown()))
        {
            error = "Failed to wait for Emission source drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.EmissionSourceDropDown()))
        {
            error = "Failed to click Emission sourcedrop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(CarbonPrice_PageObjects.TypeSearch2(), getData("Emission source option")))

        {
            error = "Failed to enter Emission source option :" + getData("Emission source option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(CarbonPrice_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(4000);

        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Emission source 1"))))
        {
            error = "Failed to wait for Emission source:" + getData("Emission source 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Emission source 1"))))
        {
            error = "Failed to click Emission source Option drop down :" + getData("Emission source 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Emission source 2"))))
        {
            error = "Failed to wait for Emission source Option drop down :" + getData("Emission source 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Emission source 2"))))
        {
            error = "Failed to click  Emission source Option drop down :" + getData("Emission source 2");
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Emission source 3"))))
//        {
//            error = "Failed to wait for Emission source Option drop down :" + getData("Emission source 3");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Emission source 3"))))
//        {
//            error = "Failed to click  Emission source Option drop down :" + getData("Emission source 3");
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.CheckBox(getData("Emission source option"))))
        {
            error = "Failed to wait for Emission source drop down option : " + getData("Emission source option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.CheckBox(getData("Emission source option"))))
        {
            error = "Failed to click Emission source drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Emission source option  :" + getData("Emission source option"));

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.EmissionSourceDropDown()))
        {
            error = "Failed to click Emission sourcedrop down";
            return false;
        }

        //Carbon price
        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.Carbonprice()))
        {
            error = "Failed to wait for Carbon price text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(CarbonPrice_PageObjects.Carbonprice(), "" + 26))

        {
            error = "Failed to enter Carbon price :" + getData("Emission factor database");
            return false;
        }

        //Start date
//        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.StartDate()))
//        {
//            error = "Failed to wait for Start date text box";
//            return false;
//        }
        
         if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.StartDate()))
        {
            error = "Failed to click Start date text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(CarbonPrice_PageObjects.StartDate(), startDate))

        {
            error = "Failed to enter Start date : " +startDate;
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.SaveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }
        //pause();

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(CarbonPrice_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(CarbonPrice_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(CarbonPrice_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        CarbonPrice_PageObjects.setRecord_Number(record[2]);
        String record_ = CarbonPrice_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;
    }

}

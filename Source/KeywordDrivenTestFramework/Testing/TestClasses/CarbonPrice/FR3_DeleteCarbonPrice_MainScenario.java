/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.CarbonPrice;

import KeywordDrivenTestFramework.Testing.TestClasses.EmissionLinking.*;
import KeywordDrivenTestFramework.Testing.TestClasses.BiodiversityMonitoring.*;
import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.CarbonPrice.CarbonPrice_PageObjects;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR3 - Delete Carbon Price",
        createNewBrowserInstance = false
)
public class FR3_DeleteCarbonPrice_MainScenario extends BaseClass
{

    String error = "";

    public FR3_DeleteCarbonPrice_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!DeleteRecord())
        {
            return narrator.testFailed("Failed to delete record :" + error);
        }

        return narrator.finalizeTest("Successfully Deleted Record");
    }

    public boolean DeleteRecord() throws InterruptedException
    {

      
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(CarbonPrice_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");



        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(CarbonPrice_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter contains to search";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);
        
         if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.DeleteButton()))
        {
            error = "Failed to wait for delete button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.DeleteButton()))
        {
            error = "Failed click delete button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);

        return true;
    }

}

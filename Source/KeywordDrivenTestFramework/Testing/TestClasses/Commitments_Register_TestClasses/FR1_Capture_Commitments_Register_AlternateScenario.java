/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Commitments_Register_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Commitments_Register_PageObjects.Commitments_Register_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Omphile.Mphahlele
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Commitments Register - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Commitments_Register_AlternateScenario extends BaseClass
{
     String error = "";
     SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Commitments_Register_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToCommitmentsRegister())
        {
            return narrator.testFailed("Navigate To Commitments Register Failed due :" + error);
        }
        if (!UploadDocument())
        {
            return narrator.testFailed("Capture Commitments Register Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Commitments Register");
    }
    
     public boolean NavigateToCommitmentsRegister()
    {
        //Navigate to Commitments Register
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitments_registerTab()))
        {
            error = "Failed to wait for Commitments Register tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitments_registerTab()))
        {
            error = "Failed to click on Commitments Register tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Commitments Register tab.");
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitmentsRegister_searchBtn()))
        {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitmentsRegister_searchBtn()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        pause(2000);
        //Last Result Page
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.lastResults_pageBtn()))
        {
            error = "Failed to wait for 'Last Page' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.lastResults_pageBtn()))
        {
            error = "Failed to click on 'Last Page' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Last Page' button.");
        
        pause(2000);
        //Record Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitmentsRegister_recordSelection()))
        {
            error = "Failed to wait for 'Record Selection'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitmentsRegister_recordSelection()))
        {
            error = "Failed to click on 'Record Selection'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Record.");

        return true;
    }
     
    public boolean UploadDocument()
    {
        pause(4000);
        //Supporting Documents
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to wait for 'Supporting Documents' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to click on 'Supporting Documents' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clciked the Supporting Documents tab.");
        
        //Link A Document
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.linkToADocument()))
        {
            error = "Failed to wait for 'Link A Document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.linkToADocument()))
        {
            error = "Failed to click on 'Link A Document' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clciked the 'Link A Document' button.");
        
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }
        
        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.UrlInput_TextArea()))
        {
            error = "Failed to wait for 'URL Input' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Commitments_Register_PageObjects.UrlInput_TextArea(), getData("Document url")))
        {
            error = "Failed to click on 'URL Input' field";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.tile_TextArea()))
        {
            error = "Failed to wait for 'Title Input' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Commitments_Register_PageObjects.tile_TextArea(), getData("Title")))
        {
            error = "Failed to click on 'Title Input' field.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.linkADoc_Add_button()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.linkADoc_Add_button()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Commitments_Register_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

       // SeleniumDriverInstance.pause(4000);

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Commitments_Register_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Commitments_Register_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Commitments_Register_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Commitments_Register_PageObjects.setRecord_Number(record[2]);
        String record_ = Commitments_Register_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        
        return true; 
    } 
}

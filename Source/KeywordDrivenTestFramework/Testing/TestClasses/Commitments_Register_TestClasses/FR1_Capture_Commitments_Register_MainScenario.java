/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Commitments_Register_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Commitments_Register_PageObjects.Commitments_Register_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author OMphahlele
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Commitments Register - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Commitments_Register_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Commitments_Register_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToCommitmentsRegister())
        {
            return narrator.testFailed("Navigate To Commitments Register Failed due :" + error);
        }
        if (!CaptureCommitmentsRegister())
        {
            return narrator.testFailed("Capture Commitments Register Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Commitments Register");
    }

    public boolean NavigateToCommitmentsRegister()
    {
        //Navigate to Commitments Register
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitments_registerTab()))
        {
            error = "Failed to wait for Commitments Register tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitments_registerTab()))
        {
            error = "Failed to click on Commitments Register tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Commitments Register tab.");
        
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitmentsRegister_addBtn()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitmentsRegister_addBtn()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean CaptureCommitmentsRegister()
    {
        pause(2000);
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitmentsRegister_pFlowBtn()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitmentsRegister_pFlowBtn()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.businessUnit_dd()))
        {
            error = "Failed to wait for 'Business Unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.businessUnit_dd()))
        {
            error = "Failed to click 'Business Unit' dropdown.";
            return false;
        }
        //Business Unit ACP Search
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.businessUnit_ddSearch()))
        {
            error = "Failed to wait for 'Business Unit' Search field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Commitments_Register_PageObjects.businessUnit_ddSearch(), getData("Business Unit Search")))
        {
            error = "Failed to click 'Business Unit' Search field.";
            return false;
        }
        pause(1000);
        
        //Business Unit Expand 1
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.businessUnit_ddTree1()))
        {
            error = "Failed to wait for 'Business Unit' expand1.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.businessUnit_ddTree1()))
        {
            error = "Failed to click 'Business Unit' expand1.";
            return false;
        }
        //Business Unit Expand 2
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.businessUnit_ddTree2()))
        {
            error = "Failed to wait for 'Business Unit' expand2.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.businessUnit_ddTree2()))
        {
            error = "Failed to click 'Business Unit' expand2.";
            return false;
        }
        //Business Unit Expand 3
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.businessUnit_ddTree3()))
        {
            error = "Failed to wait for 'Business Unit' expand3.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.businessUnit_ddTree3()))
        {
            error = "Failed to click 'Business Unit' expand3.";
            return false;
        }
        //Business Unit Expand 4
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.businessUnit_ddTree4()))
        {
            error = "Failed to wait for 'Business Unit' expand4.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.businessUnit_ddTree4()))
        {
            error = "Failed to click 'Business Unit' expand4.";
            return false;
        }
        //Business Unit Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.businessUnit_selection()))
        {
            error = "Failed to wait for 'Business Unit' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.businessUnit_selection()))
        {
            error = "Failed to click 'Business Unit' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Business Unit.");
        
        //Functional Location
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.funcLocation_dd()))
        {
            error = "Failed to wait for 'Functional Location' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.funcLocation_dd()))
        {
            error = "Failed to click 'Functional Location' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.funcLocation_selection()))
        {
            error = "Failed to wait for 'Parameter Components' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.funcLocation_selection()))
        {
            error = "Failed to click 'Parameter Components' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Parameter Component.");
        
        //Commitment Register Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitRegisterTitle_input()))
        {
            error = "Failed to wait for 'Commitment Register Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Commitments_Register_PageObjects.commitRegisterTitle_input(), getData("Commitment Register Title")))
        {
            error = "Failed to click 'Commitment Register Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered a 'Commitment Register Title'.");
        
        //Commitment Register Owner dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitRegisterOwner_dd()))
        {
            error = "Failed to wait for 'Commitment Register Owner' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitRegisterOwner_dd()))
        {
            error = "Failed to click 'Commitment Register Owner' dropdown.";
            return false;
        }
        //Commitment Register Owner dropdown search
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitRegisterOwner_ddSearch()))
        {
            error = "Failed to wait for 'Commitment Register Owner' search.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Commitments_Register_PageObjects.commitRegisterOwner_ddSearch(), getData("Commitment Register Owner Search")))
        {
            error = "Failed to click 'Commitment Register Owner' search.";
            return false;
        }
        //Press Enter
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitRegisterOwner_ddSearch()))
        {
            error = "Failed to wait for 'Commitment Register Owner' search.";
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Commitments_Register_PageObjects.commitRegisterOwner_ddSearch()))
        {
            error = "Failed to press enter 'Commitment Register Owner' search.";
            return false;
        }
        //Select Commitment Register Owner
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitRegisterOwner_selection()))
        {
            error = "Failed to wait for 'Commitment Register Owner' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitRegisterOwner_selection()))
        {
            error = "Failed to click 'Commitment Register Owner' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Commitment Register Owner.");
        
        //Related Permits
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.relatedPermits_dd()))
        {
            error = "Failed to wait for 'Functional Location' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.relatedPermits_dd()))
        {
            error = "Failed to click 'Functional Location' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.relatedPermits_selection()))
        {
            error = "Failed to wait for 'Functional Location' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.relatedPermits_selection()))
        {
            error = "Failed to click 'Functional Location' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Functional Location.");
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Commitments_Register_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Commitments_Register_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Commitments_Register_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Commitments_Register_PageObjects.setRecord_Number(record[2]);
        String record_ = Commitments_Register_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}

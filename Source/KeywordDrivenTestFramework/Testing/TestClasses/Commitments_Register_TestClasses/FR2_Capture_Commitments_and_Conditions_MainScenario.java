/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Commitments_Register_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Commitments_Register_PageObjects.Commitments_Register_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Omphile.Mphahlele
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Commitments and Conditions - Main Scenario",
        createNewBrowserInstance = false
)
public class FR2_Capture_Commitments_and_Conditions_MainScenario extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_Commitments_and_Conditions_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToCommitmentsRegister())
        {
            return narrator.testFailed("Navigate To Commitments Register Failed due :" + error);
        }
        if (!CaptureCommitmentsAndConditions())
        {
            return narrator.testFailed("Capture Commitments Register Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Commitments Register");
    }
    
     public boolean NavigateToCommitmentsRegister()
    {
        //Navigate to Commitments Register
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitments_registerTab()))
        {
            error = "Failed to wait for Commitments Register tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitments_registerTab()))
        {
            error = "Failed to click on Commitments Register tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Commitments Register tab.");
        
        pause(2000);
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitmentsRegister_searchBtn()))
        {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitmentsRegister_searchBtn()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        pause(6000);
        //Last Result Page
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.lastResults_pageBtn()))
        {
            error = "Failed to wait for 'Last Page' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.lastResults_pageBtn()))
        {
            error = "Failed to click on 'Last Page' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Last Page' button.");
        
        pause(2000);
        //Record Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitmentsRegister_recordSelection()))
        {
            error = "Failed to wait for 'Record Selection'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitmentsRegister_recordSelection()))
        {
            error = "Failed to click on 'Record Selection'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Record.");
        
        pause(4000);
        
        //Commitments Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitments_tab()))
        {
            error = "Failed to wait for 'Commitments Tab'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitments_tab()))
        {
            error = "Failed to click on 'Commitments Tab'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Commitments Tab.");
        
        //Commitments Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitment_addBtn()))
        {
            error = "Failed to wait for Commitments Add Button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitment_addBtn()))
        {
            error = "Failed to click on Commitments Add Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Commitments Add Button.");

        return true;
    } 
     
     public boolean CaptureCommitmentsAndConditions()
    {
        pause(4000);
        //Conditions And Commitments Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.CondiCommitment_pFlow()))
        {
            error = "Failed to wait for Conditions And Commitments Process Flow Button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.CondiCommitment_pFlow()))
        {
            error = "Failed to click on Conditions And Commitments Process Flow Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Conditions And Commitments Process Flow Button.");
        
        //Category Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.CondiCommitment_categoryDd()))
        {
            error = "Failed to wait for Category Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.CondiCommitment_categoryDd()))
        {
            error = "Failed to click on Category Dropdown.";
            return false;
        }
        //Category Dropdown Select
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.CondiCommitment_categoryDdSelection()))
        {
            error = "Failed to wait for Category Dropdown option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.CondiCommitment_categoryDdSelection()))
        {
            error = "Failed to click on Category Dropdown option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully on the Category Dropdown option.");
        
        //Title Input Field
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.CondiCommitment_titleInput()))
        {
            error = "Failed to wait for Title Input Field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Commitments_Register_PageObjects.CondiCommitment_titleInput(), getData("Title Input Field")))
        {
            error = "Failed to click on Title Input Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered on the Title Input Field.");
        
        //Description Input Field
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.CondiCommitment_descriptionInput()))
        {
            error = "Failed to wait for Title Description Field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Commitments_Register_PageObjects.CondiCommitment_descriptionInput(), getData("Description Input Field")))
        {
            error = "Failed to click on Title Description Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered on the Description Input Field.");
        
        //Nature Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.CondiCommitment_natureDd()))
        {
            error = "Failed to wait for Nature Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.CondiCommitment_natureDd()))
        {
            error = "Failed to click on Nature Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.CondiCommitment_natureDdSelect()))
        {
            error = "Failed to wait for Nature Dropdown option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.CondiCommitment_natureDdSelect()))
        {
            error = "Failed to click on Nature Dropdown option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Nature Dropdown option.");
        
        //Date Commitment/ Condition Was Set
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.CondiCommitment_dateSetInput()))
        {
            error = "Failed to wait for 'Date Commitment/ Condition Was Set' input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Commitments_Register_PageObjects.CondiCommitment_dateSetInput(), getData("Date Commitment/ Condition Was Set")))
        {
            error = "Failed to enter on 'Date Commitment/ Condition Was Set' input field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered on the 'Date Commitment/ Condition Was Set' input field.");
        
        //Once-Off or Permanent Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.onceOffOrPerm_dd()))
        {
            error = "Failed to wait for Once-Off or Permanent Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.onceOffOrPerm_dd()))
        {
            error = "Failed to click on Once-Off or Permanent Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.onceOffOrPerm_ddSelection()))
        {
            error = "Failed to wait for Once-Off or Permanent Dropdown option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.onceOffOrPerm_ddSelection()))
        {
            error = "Failed to click on Once-Off or Permanent Dropdown option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Once-Off or Permanent Dropdown option.");
        
        //Original target completion date
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.originalCompDate_input()))
        {
            error = "Failed to wait for 'Original target completion date' input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Commitments_Register_PageObjects.originalCompDate_input(), getData("Original target completion date")))
        {
            error = "Failed to enter on 'Original target completion date' input field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered on the 'Original target completion date' input field.");
        
        //Current Expected Completion Date
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.expectedCompDate_input()))
        {
            error = "Failed to wait for 'Current Expected Completion Date' input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Commitments_Register_PageObjects.expectedCompDate_input(), getData("Current Expected Completion Date")))
        {
            error = "Failed to enter on 'Current Expected Completion Date' input field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered on the 'Current Expected Completion Date' input field.");
        
        //Days Since Commitment/ Condition Set Date
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.daysSinceCommit_input()))
        {
            error = "Failed to wait for 'Days Since Commitment/ Condition Set Date' input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Commitments_Register_PageObjects.daysSinceCommit_input(), getData("Days Since Commitment/ Condition Set Date")))
        {
            error = "Failed to enter on 'Days Since Commitment/ Condition Set Date' input field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered on the 'Days Since Commitment/ Condition Set Date' input field.");
        pause(1000);
        //Conditions & Commitments Owner dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.condiCommit_OwnerDd()))
        {
            error = "Failed to wait for 'Conditions & Commitments Owner' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.condiCommit_OwnerDd()))
        {
            error = "Failed to click 'Conditions & Commitments Owner' dropdown.";
            return false;
        }
        //Conditions & Commitments Owner dropdown search
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.condiCommit_OwnerDdSearch()))
        {
            error = "Failed to wait for 'Conditions & Commitments Owner' dropdown search.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Commitments_Register_PageObjects.condiCommit_OwnerDdSearch(), getData("Conditions & Commitments Owner dropdown search")))
        {
            error = "Failed to enter 'Conditions & Commitments Owner' dropdown search.";
            return false;
        }
        //Press Enter
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.condiCommit_OwnerDdSearch()))
        {
            error = "Failed to wait for 'Conditions & Commitments Owner' search.";
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Commitments_Register_PageObjects.condiCommit_OwnerDdSearch()))
        {
            error = "Failed to press enter 'Conditions & Commitments Owner' search.";
            return false;
        }
        pause(6000);
        //Select Conditions & Commitments Owner
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.condiCommit_OwnerDdSelection()))
        {
            error = "Failed to wait for 'Conditions & Commitments Owner' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.condiCommit_OwnerDdSelection()))
        {
            error = "Failed to click 'Conditions & Commitments Owner' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Conditions & Commitments Owner.");
        
        //Conditions & Commitments Approver dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.condiCommit_approverDd()))
        {
            error = "Failed to wait for 'Conditions & Commitments Approver' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.condiCommit_approverDd()))
        {
            error = "Failed to click 'Conditions & Commitments Approver' dropdown.";
            return false;
        }
        //Conditions & Commitments Approver dropdown search
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.condiCommit_approverDdSearch()))
        {
            error = "Failed to wait for 'Conditions & Commitments Approver' dropdown search.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Commitments_Register_PageObjects.condiCommit_approverDdSearch(), getData("Conditions & Commitments Approver dropdown search")))
        {
            error = "Failed to click 'Conditions & Commitments Approver' dropdown search.";
            return false;
        }
        //Press Enter
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.condiCommit_approverDdSearch()))
        {
            error = "Failed to wait for 'Conditions & Commitments Approver' search.";
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Commitments_Register_PageObjects.condiCommit_approverDdSearch()))
        {
            error = "Failed to press enter 'Conditions & Commitments Approver' search.";
            return false;
        }
        pause(6000);
        //Select Conditions & Commitments Approver
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.condiCommit_approverDdSelection()))
        {
            error = "Failed to wait for 'Conditions & Commitments Approver' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.condiCommit_approverDdSelection()))
        {
            error = "Failed to click 'Conditions & Commitments Approver' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Conditions & Commitments Approver.");
        
        //Progress Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.progress_dd()))
        {
            error = "Failed to wait for Progress Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Commitments_Register_PageObjects.progress_dd()))
        {
            error = "Failed to press enter Progress Dropdown.";
            return false;
        }
        //Progress Dropdown Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.progress_ddSelection()))
        {
            error = "Failed to wait for Progress Dropdown Selection.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.progress_ddSelection()))
        {
            error = "Failed to click Progress Dropdown Selection.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Progress.");
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.condiCommit_saveBtn()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.condiCommit_saveBtn()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Commitments_Register_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Commitments_Register_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Commitments_Register_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Commitments_Register_PageObjects.setRecord_Number(record[2]);
        String record_ = Commitments_Register_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        
        return true;
    }
}

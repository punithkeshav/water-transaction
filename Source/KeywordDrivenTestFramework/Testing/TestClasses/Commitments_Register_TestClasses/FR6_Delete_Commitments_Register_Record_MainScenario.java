/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Commitments_Register_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Commitments_Register_PageObjects.Commitments_Register_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Omphile.Mphahlele
 */
@KeywordAnnotation(
        Keyword = "FR6-Delete Commitments Register Record - Main Scenario",
        createNewBrowserInstance = false
)

public class FR6_Delete_Commitments_Register_Record_MainScenario extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR6_Delete_Commitments_Register_Record_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToCommitmentsRegister())
        {
            return narrator.testFailed("Navigate To Commitments Register Failed due :" + error);
        }
        if (!DeleteCommitmentsRegister())
        {
            return narrator.testFailed("Capture Commitments Register Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Commitments Register");
    }

   public boolean NavigateToCommitmentsRegister()
    {
        //Navigate to Commitments Register
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitments_registerTab()))
        {
            error = "Failed to wait for Commitments Register tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitments_registerTab()))
        {
            error = "Failed to click on Commitments Register tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Commitments Register tab.");
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitmentsRegister_searchBtn()))
        {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitmentsRegister_searchBtn()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        pause(6000);
        //Last Result Page
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.lastResults_pageBtn()))
        {
            error = "Failed to wait for 'Last Page' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.lastResults_pageBtn()))
        {
            error = "Failed to click on 'Last Page' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Last Page' button.");
        
        pause(2000);
        //Record Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitmentsRegister_recordSelection()))
        {
            error = "Failed to wait for 'Record Selection'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitmentsRegister_recordSelection()))
        {
            error = "Failed to click on 'Record Selection'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Record.");

        return true;
    }

    public boolean DeleteCommitmentsRegister()
    {
        pause(2000);
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitmentsRegister_pFlowBtn()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitmentsRegister_pFlowBtn()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");
        
        //Delete Button
         if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.commitments_deleteBtn()))
        {
            error = "Failed to wait for delete button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.commitments_deleteBtn()))
        {
            error = "Failed click delete button";
            return false;
        }


        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }
        narrator.stepPassedWithScreenShot("");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Commitments_Register_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Commitments_Register_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);
        
        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ComplianceAssessment_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.ComplianceAssessment_PageObjects.ComplianceAssessment_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR1 Capture Compliance Assessment record",
        createNewBrowserInstance = false
)

public class FR1_Capture_ComplianceAssessmentRecord_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;

    public FR1_Capture_ComplianceAssessmentRecord_MainScenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!CaptureComplianceAssessment())
        {
            return narrator.testFailed("Failed To Capture Compliance Assessment Due To : " + error);
        }

        if (getData("Upload Document").equalsIgnoreCase("True"))
        {

            if (!UploadDocument())
            {
                return narrator.testFailed("Failed to upload document: " + error);
            }
        }
        return narrator.finalizeTest("Successfully Captured Compliance Assessment record");
    }

    public boolean CaptureComplianceAssessment()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.ComplianceAssessmentLabel()))
        {
            error = "Failed to wait for Compliance Assessment";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.ComplianceAssessmentLabel()))
        {
            error = "Failed to click on Compliance Assessment";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Compliance Assessment");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.AddButton()))
        {
            error = "Failed to locate add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.AddButton()))
        {
            error = "Failed to click on add button";
            return false;
        }

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow in Add phase");

        //Business Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to locate Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ComplianceAssessment_PageObjects.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to wait for Business Unit :" + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(ComplianceAssessment_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(4000);
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait to expand Business Unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to expand Business Unit";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down:" + getData("Business unit 5");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));
        
        
        //Assessment date
         if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.AssessmentDate()))
        {
            error = "Failed to wait for Assessment date text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ComplianceAssessment_PageObjects.AssessmentDate(), startDate))
        {
            error = "Failed to enter Assessment date:" + startDate;
            return false;
        }
        
        

        //Assessment conducted by Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.AssessmentConductedByDropdown()))
        {
            error = "Failed to wait for Assessment conducted by Dropdown";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.AssessmentConductedByDropdown()))
        {
            error = "Failed to click on Assessment conducted by Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Assessment conducted by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ComplianceAssessment_PageObjects.TypeSearch(), getData("Assessment conducted by")))
        {
            error = "Failed to enter SAssessment conducted by:" + getData("Assessment conducted by");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(ComplianceAssessment_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.Text(getData("Assessment conducted by"))))
        {
            error = "Failed to wait for Assessment conducted by drop down option : " + getData("Assessment conducted by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.Text(getData("Assessment conducted by"))))
        {
            error = "Failed to click Assessment conducted by drop down option : " + getData("Assessment conducted by");
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.SaveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ComplianceAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = ComplianceAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;
    }

    public boolean UploadDocument() throws InterruptedException
    {

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.linkADoc_button()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(ComplianceAssessment_PageObjects.linkADoc_button()))
        {
            error = "Failed to scroll to 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.linkADoc_button()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.urlInput_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(ComplianceAssessment_PageObjects.urlInput_TextAreaxpath(), getData("Document Link")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.tile_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(ComplianceAssessment_PageObjects.tile_TextAreaxpath(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(ComplianceAssessment_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.SaveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ComplianceAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = ComplianceAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;

    }

}

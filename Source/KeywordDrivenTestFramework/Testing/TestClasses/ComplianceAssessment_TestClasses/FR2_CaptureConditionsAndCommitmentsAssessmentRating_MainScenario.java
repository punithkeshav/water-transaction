/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ComplianceAssessment_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.ComplianceAssessment_PageObjects.ComplianceAssessment_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Conditions and Commitments Assessment Rating",
        createNewBrowserInstance = false
)

public class FR2_CaptureConditionsAndCommitmentsAssessmentRating_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;

    public FR2_CaptureConditionsAndCommitmentsAssessmentRating_MainScenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!CaptureConditionsAndCommitmentsAssessmentRating())
        {
            return narrator.testFailed("Failed To Capture Conditions and Commitments Assessment Rating Due To : " + error);
        }

        if (getData("Upload Document").equalsIgnoreCase("True"))
        {

            if (!UploadDocument())
            {
                return narrator.testFailed("Failed to upload document: " + error);
            }
        }
        return narrator.finalizeTest("Successfully Captured Conditions and Commitments Assessment Rating");
    }

    public boolean CaptureConditionsAndCommitmentsAssessmentRating()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.ApplicableConditionsAndCommitmentsTab()))
        {
            error = "Failed to wait for Applicable Conditions and Commitments tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.ApplicableConditionsAndCommitmentsTab()))
        {
            error = "Failed to click on Applicable Conditions and Commitments tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Applicable Conditions and Commitments tab");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.ConditionsAndCommitmentsAddButton()))
        {
            error = "Failed to locate add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.ConditionsAndCommitmentsAddButton()))
        {
            error = "Failed to click on add button";
            return false;
        }

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.ConditionsAndCommitmentsProcessFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.ConditionsAndCommitmentsProcessFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow in Add phase");

        //Title Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.TitleDropdown()))
        {
            error = "Failed to locate Title Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.TitleDropdown()))
        {
            error = "Failed to click on Title Dropdown";
            return false;
        }


        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.Text6(getData("Title option"))))
        {
            error = "Failed to wait for Title down option : " + getData("Title option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.Text6(getData("Title option"))))
        {
            error = "Failed to click Title drop down option : " + getData("Title option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Title option  :" + getData("Title option"));
        
       

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.SaveButton2()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.SaveButton2()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ComplianceAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = ComplianceAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");
        
      
        

        return true;
    }

    public boolean UploadDocument() throws InterruptedException
    {
        
        //
        
        
         if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to wait for Supporting Documents button.";
            return false;
        }
         
           if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to click on Supporting Documents button.";
            return false;
        }
         
         
         

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.linkADoc_button()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(ComplianceAssessment_PageObjects.linkADoc_button()))
        {
            error = "Failed to scroll to 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.linkADoc_button()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.urlInput_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(ComplianceAssessment_PageObjects.urlInput_TextAreaxpath(), getData("Document Link")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.tile_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(ComplianceAssessment_PageObjects.tile_TextAreaxpath(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(ComplianceAssessment_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.SaveButton2()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.SaveButton2()))
        {
            error = "Failed to click Save button";
            return false;
        }

        

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ComplianceAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = ComplianceAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;

    }

}

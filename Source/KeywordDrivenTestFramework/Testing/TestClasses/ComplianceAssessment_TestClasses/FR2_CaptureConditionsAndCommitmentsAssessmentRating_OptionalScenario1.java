/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ComplianceAssessment_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.ComplianceAssessment_PageObjects.ComplianceAssessment_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Conditions and Commitments Assessment Rating Optional Scenario 1",
        createNewBrowserInstance = false
)

public class FR2_CaptureConditionsAndCommitmentsAssessmentRating_OptionalScenario1 extends BaseClass
{

    String error = "";
    private String textbox;

    public FR2_CaptureConditionsAndCommitmentsAssessmentRating_OptionalScenario1()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!CaptureConditionsAndCommitmentsAssessmentRating())
        {
            return narrator.testFailed("Failed To Capture Conditions and Commitments Assessment Rating Due To : " + error);
        }

        return narrator.finalizeTest("Successfully Captured Conditions and Commitments Assessment Rating");
    }

    public boolean CaptureConditionsAndCommitmentsAssessmentRating()
    {

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.findingsAddButton()))
        {
            error = "Failed to wait for Add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.findingsAddButton()))
        {
            error = "Failed to click on Add button.";
            return false;
        }

        //Process Flow
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.findingsProcessflow()))
        {
            error = "Failed to wait for Process Flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.findingsProcessflow()))
        {
            error = "Failed to click Process Flow button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Processflow in Add phase");

        //Findings Description
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.Findings_desc()))
        {
            error = "Failed to wait for Description textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(ComplianceAssessment_PageObjects.Findings_desc(), testData.getData("Description")))
        {
            error = "Failed to enter text into Description textarea.";
            return false;
        }

        //Findings Owner dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.Findings_owner_dropdown()))
        {
            error = "Failed to wait for Findings Owner dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.Findings_owner_dropdown()))
        {
            error = "Failed to click Findings Owner dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Measurement taken by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ComplianceAssessment_PageObjects.TypeSearch(), getData("Findings Owner")))
        {
            error = "Failed to enter  Findings Owner :" + getData("Findings Owner");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(ComplianceAssessment_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        pause(10000);
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.Text2(getData("Findings Owner"))))
        {
            error = "Failed to wait for Findings Owner drop down option  : " + getData("Findings Owner") + " " + "for more than 10 seconds";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.Text2(getData("Findings Owner"))))
        {
            error = "Failed to click Findings Owner drop down option : " + getData("Findings Owner");
            return false;
        }

        //Findings Classification dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.Findings_class_dropdown()))
        {
            error = "Failed to wait for Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.Findings_class_dropdown()))
        {
            error = "Failed to click Findings Classification dropdown.";
            return false;
        }
        //Findings Classification select
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.Text2(getData("Findings Classification"))))
        {
            error = "Failed to wait for '" + testData.getData("Findings Classification") + "' in Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.Text2(testData.getData("Findings Classification"))))
        {
            error = "Failed to click '" + testData.getData("Findings Classification") + "' from Findings Classification dropdown.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.SaveButtonFindings()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ComplianceAssessment_PageObjects.SaveButtonFindings()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(ComplianceAssessment_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(ComplianceAssessment_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        ComplianceAssessment_PageObjects.setRecord_Number(record[2]);
        String record_ = ComplianceAssessment_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        return true;
    }

}

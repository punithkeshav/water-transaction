/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EmissionLinking;

import KeywordDrivenTestFramework.Testing.TestClasses.BiodiversityMonitoring.*;
import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.EmissionLinking.EmissionLinking_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.IsometricsPOCPageObjects;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR1 - Capture Emission Linking",
        createNewBrowserInstance = false
)

public class FR1_CaptureEmissionLinking_MainScenario extends BaseClass
{

    String error = "";

    public FR1_CaptureEmissionLinking_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!CaptureEmissioLinking())
        {
            return narrator.testFailed("Failed To Capture Emission Linking Due To : " + error);
        }
  
        return narrator.finalizeTest("Successfully Capture Emission Linking");
    }

    public boolean CaptureEmissioLinking()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.ECO2ManLabel()))
        {
            error = "Failed to wait for ECO2Man";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.ECO2ManLabel()))
        {
            error = "Failed to click on ECO2Man";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked ECO2ManLabel");
        
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.MonitoringMaintence()))
        {
            error = "Failed to wait for Monitoring Maintenance";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.MonitoringMaintence()))
        {
            error = "Failed to click on Monitoring Maintenance";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Monitoring Maintenance");
        
        pause(3000);
        //Emission Linking
          if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.EmissionLinking()))
        {
            error = "Failed to wait for Emission Linking";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.EmissionLinking()))
        {
            error = "Failed to click on Emission Linking";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Emission Linking");
        
        
        
        

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.AddButton()))
        {
            error = "Failed to locate add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.AddButton()))
        {
            error = "Failed to click on add button";
            return false;
        }

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow in Add phase");

        //Business Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to locate Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for Business Unit text box.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(EmissionLinking_PageObjects.TypeSearch2(), getData("Business unit option")))
//        {
//            error = "Failed to wait for Business Unit :" + getData("Business unit option");
//            return false;
//        }
//        if (!SeleniumDriverInstance.pressEnter_2(EmissionLinking_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to press enter";
//            return false;
//        }

        pause(3000);
//        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Business unit"))))
//        {
//            error = "Failed to wait to expand Business Unit";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Business unit"))))
//        {
//            error = "Failed to expand Business Unit";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }
        
          if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down:" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Emission factor database
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.EmissionFactorDatabaseDropDown()))
        {
            error = "Failed to wait for Emission factor database drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.EmissionFactorDatabaseDropDown()))
        {
            error = "Failed to click Emission factor database drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EmissionLinking_PageObjects.TypeSearch2(), getData("Emission factor database")))

        {
            error = "Failed to enter Emission factor database :" + getData("Emission factor database");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(EmissionLinking_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.Text2(getData("Emission factor database"))))
        {
            error = "Failed to wait for Emission factor database drop down option : " + getData("Emission factor database");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(EmissionLinking_PageObjects.Text2(getData("Emission factor database"))))
        {
            error = "Failed to click Emission factor database drop down option : " + getData("Emission factor database");
            return false;
        }
        narrator.stepPassedWithScreenShot("Emission factor database :"+getData("Emission factor database"));

        

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.SaveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }
        //pause();

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(EmissionLinking_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EmissionLinking_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EmissionLinking_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EmissionLinking_PageObjects.setRecord_Number(record[2]);
        String record_ = EmissionLinking_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;
    }

   

}

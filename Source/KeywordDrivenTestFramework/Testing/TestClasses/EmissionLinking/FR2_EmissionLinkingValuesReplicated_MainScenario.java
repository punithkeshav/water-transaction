/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EmissionLinking;

import KeywordDrivenTestFramework.Testing.TestClasses.BiodiversityMonitoring.*;
import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Biodiversity_Monitoring.Biodiversity_Monitoring;
import KeywordDrivenTestFramework.Testing.PageObjects.EmissionLinking.EmissionLinking_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.IsometricsPOCPageObjects;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR2- Emission Linking Values Replicated",
        createNewBrowserInstance = false
)

public class FR2_EmissionLinkingValuesReplicated_MainScenario extends BaseClass
{

    String error = "";

    public FR2_EmissionLinkingValuesReplicated_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (getData("Execute").equalsIgnoreCase("True"))
        {
            if (!EmissionLinkingValuesReplicated())
            {
                return narrator.testFailed("Emission Linking Values Replicated Failed Due To : " + error);
            }
        }

        if (getData("Execute Alternate").equalsIgnoreCase("True"))
        {

            if (!EmissionLinkingValuesReplicated_Alternate())
            {
                return narrator.testFailed("Emission Linking Values Replicated Failed Due To : " + error);
            }
        }
        return narrator.finalizeTest("Successfully Emission Linking Values Replicated");
    }

    public boolean EmissionLinkingValuesReplicated()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.TableRecord()))
        {
            error = "Failed to wait for Table Record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.TableRecord()))
        {
            error = "Failed to click on Table Record";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Table Record");

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.RecordprocessFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.RecordprocessFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow");

        //Year of compliance
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.YearOfComplianceDropdown()))
        {
            error = "Failed to wait for Year of compliance drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.YearOfComplianceDropdown()))
        {
            error = "Failed to click on Year of compliance drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Year of compliance text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EmissionLinking_PageObjects.TypeSearch2(), getData("Year of compliance")))
        {
            error = "Failed to enter  Year of compliance:" + getData("Year of compliance");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(EmissionLinking_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.Text2(getData("Year of compliance"))))
        {
            error = "Failed to wait for Year of compliance";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.Text2(getData("Year of compliance"))))
        {
            error = "Failed to click Year of compliance";
            return false;
        }

        narrator.stepPassedWithScreenShot("Year of compliance  :" + getData("Year of compliance"));

        //CO2e factor unit
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.CO2eFactorUnitDropDown()))
        {
            error = "Failed to wait for CO2e factor unit drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.CO2eFactorUnitDropDown()))
        {
            error = "Failed to click CO2e factor unit drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EmissionLinking_PageObjects.TypeSearch2(), getData("CO2e factor unit")))

        {
            error = "Failed to enter CO2e factor unit :" + getData("CO2e factor unit");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(EmissionLinking_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.Text2(getData("CO2e factor unit"))))
        {
            error = "Failed to wait for CO2e factor unit drop down option : " + getData("CO2e factor unit");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(EmissionLinking_PageObjects.Text2(getData("CO2e factor unit"))))
        {
            error = "Failed to click CO2e factor unit drop down option : " + getData("CO2e factor unit");
            return false;
        }
        narrator.stepPassedWithScreenShot("CO2e factor unit :" + getData("CO2e factor unit"));

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.SaveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(EmissionLinking_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EmissionLinking_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EmissionLinking_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Biodiversity_Monitoring.setRecord_Number(record[2]);
        String record_ = EmissionLinking_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;
    }

    public boolean EmissionLinkingValuesReplicated_Alternate()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.EmissionLinkingValuesAddButton()))
        {
            error = "Failed to wait for the add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.EmissionLinkingValuesAddButton()))
        {
            error = "Failed to click on the add button";
            return false;
        }

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.RecordprocessFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.RecordprocessFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow");

        //Emission source
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.EmissionSourceDropdown()))
        {
            error = "Failed to locate Emission source Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.EmissionSourceDropdown()))
        {
            error = "Failed to click on Emission source Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Emission source text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EmissionLinking_PageObjects.TypeSearch2(), getData("Emission source option")))
        {
            error = "Failed to wait for Emission source : " + getData("Emission source option");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(EmissionLinking_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        pause(3000);
//        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Emission source"))))
//        {
//            error = "Failed to wait to expand Emission source";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Emission source"))))
//        {
//            error = "Failed to expand Emission source";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Emission source 1"))))
        {
            error = "Failed to wait for Emission source:" + getData("Emission source 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Emission source 1"))))
        {
            error = "Failed to click Emission source Option drop down :" + getData("Emission source 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Emission source 2"))))
        {
            error = "Failed to wait for Emission source Option drop down :" + getData("Emission source 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Emission source 2"))))
        {
            error = "Failed to click  Emission source Option drop down :" + getData("Emission source 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Emission source 3"))))
        {
            error = "Failed to wait for Emission source Option drop down :" + getData("Emission source 3");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Emission source 3"))))
        {
            error = "Failed to click  Emission source Option drop down :" + getData("Emission source 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.Text3(getData("Emission source option"))))
        {
            error = "Failed to wait for Emission source drop down option : " + getData("Emission source option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.Text3(getData("Emission source option"))))
        {
            error = "Failed to click Emission source drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Emission source option  :" + getData("Emission source option"));

        //Year of compliance
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.YearOfComplianceDropdown()))
        {
            error = "Failed to wait for Year of compliance drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.YearOfComplianceDropdown()))
        {
            error = "Failed to click on Year of compliance drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EmissionLinking_PageObjects.TypeSearch2(), getData("Year of compliance")))
        {
            error = "Failed to enter  Year of compliance:" + getData("Year of compliance");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(EmissionLinking_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.Text2(getData("Year of compliance"))))
        {
            error = "Failed to wait for Year of compliance";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.Text2(getData("Year of compliance"))))
        {
            error = "Failed to click Year of compliance";
            return false;
        }

        narrator.stepPassedWithScreenShot("Year of compliance  :" + getData("Year of compliance"));

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.SaveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(EmissionLinking_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EmissionLinking_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EmissionLinking_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EmissionLinking_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Biodiversity_Monitoring.setRecord_Number(record[2]);
        String record_ = EmissionLinking_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Emission_Factors_Maintenance_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Emission_Factors_Maintenance_PageObjects.Emission_Factors_Maintenance_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author OMphahlele
 */
@KeywordAnnotation(
        Keyword = "FR2- Emission Factor Maintenance Values - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Emission_Factor_Maintenance_Values_MainScenario extends BaseClass
{
    
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR2_Emission_Factor_Maintenance_Values_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }
    
     public TestResult executeTest()
    {
        if (!NavigateToEmissionFactorsMaintenance())
        {
            return narrator.testFailed("Navigate To Emission Factors Maintenance Failed due :" + error);
        }
        if (!CaptureEmissionFactorsMaintenanceValues())
        {
            return narrator.testFailed("Capture Emission Factors Maintenance Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Emission Factors Maintenance");
    }
     
     public boolean NavigateToEmissionFactorsMaintenance()
    {
        //Navigate to ECO2Man tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.Eco2ManTab()))
        {
            error = "Failed to wait for ECO2Man tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.Eco2ManTab()))
        {
            error = "Failed to click on ECO2Man tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to ECO2Man tab.");
        
        //Maintenance tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.MonitoringMaintenanceTab()))
        {
            error = "Failed to wait for Monitoring Maintenance tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.MonitoringMaintenanceTab()))
        {
            error = "Failed to click on Monitoring Maintenance tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Monitoring Maintenance tab.");
        
        //Emission Factors tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.EmissionFactorsTab()))
        {
            error = "Failed to wait for Emission Factors tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.EmissionFactorsTab()))
        {
            error = "Failed to click on Emission Factors tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Emission Factors tab.");
        pause(2000);
        
        //Search
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.searchBtn()))
        {
            error = "Failed to wait for Search button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.searchBtn()))
        {
            error = "Failed to click on Search button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Search button.");
        pause(2000);
        
        //Emission Factor Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.emissionFactor_Record()))
        {
            error = "Failed to wait for Emission Factor Record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.emissionFactor_Record()))
        {
            error = "Failed to click on Emission Factor Record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected Emission Factor record.");
        pause(2000);
        
        return true;
    }
     
     public boolean CaptureEmissionFactorsMaintenanceValues()
    {
        //Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.value_addBtn()))
        {
            error = "Failed to wait for Value add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.value_addBtn()))
        {
            error = "Failed to click on Value add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Value add button.");
        
        pause(2000);
        
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.value_pFlowBtn()))
        {
            error = "Failed to wait for Process Flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.value_pFlowBtn()))
        {
            error = "Failed to click on Process Flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Process Flow button.");
        
        //Emission Source Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.emissionSource_Dd()))
        {
            error = "Failed to wait for Emission Source Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.emissionSource_Dd()))
        {
            error = "Failed to click on Emission Source Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.emissionSource_DdTree()))
        {
            error = "Failed to wait for Emission Source Dropdown tree.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.emissionSource_DdTree()))
        {
            error = "Failed to click on Emission Source Dropdown tree.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.emissionSource_DdTree1()))
        {
            error = "Failed to wait for Emission Source Dropdown tree 1.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.emissionSource_DdTree1()))
        {
            error = "Failed to click on Emission Source Dropdown tree 1.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.Text(getData("Emission Source option"))))
        {
            error = "Failed to wait for Emission Factor Record number : " + getData("Emission Source option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.Text(getData("Emission Source option"))))
        {
            error = "Failed to click on Emission Factor Record number : " + getData("Emission Source option");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Emission Source option.");
        
        //Year Of Compliance
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.yearOf_compliance()))
        {
            error = "Failed to wait for Emission Source Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.yearOf_compliance()))
        {
            error = "Failed to click on Emission Source Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.Text(getData("Year Of Compliance"))))
        {
            error = "Failed to wait for Year Of Compliance options : " + getData("Year Of Compliance");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.Text(getData("Year Of Compliance"))))
        {
            error = "Failed to click on Year Of Compliance options : " + getData("Year Of Compliance");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Year Of Compliance options.");
        
        //CO2e
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.CO2e_input()))
        {
            error = "Failed to wait for CO2e input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Emission_Factors_Maintenance_PageObjects.CO2e_input(), getData("CO2e Input")))
        {
            error = "Failed to enter CO2e input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered a CO2e.");
        
//        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.CO2e_Dd()))
//        {
//            error = "Failed to wait for CO2e Dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.CO2e_Dd()))
//        {
//            error = "Failed to click on CO2e Dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.CO2e_option()))
//        {
//            error = "Failed to wait for CO2e Dropdown option.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.CO2e_option()))
//        {
//            error = "Failed to click on CO2e Dropdown option.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully selected a CO2e option.");
        
        //CO2
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.CO2_input()))
        {
            error = "Failed to wait for CO2 input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Emission_Factors_Maintenance_PageObjects.CO2_input(), getData("CO2 Input")))
        {
            error = "Failed to enter CO2 input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered a CO2.");
        
//        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.CO2_Dd()))
//        {
//            error = "Failed to wait for CO2 Dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.CO2_Dd()))
//        {
//            error = "Failed to click on CO2 Dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.CO2_option()))
//        {
//            error = "Failed to wait for CO2 Dropdown option.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.CO2_option()))
//        {
//            error = "Failed to click on CO2 Dropdown option.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully selected a CO2 option option.");
        
        //CH4
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.CH4_input()))
        {
            error = "Failed to wait for CH4 input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Emission_Factors_Maintenance_PageObjects.CH4_input(), getData("CH4 Input")))
        {
            error = "Failed to enter CH4 input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered a CH4.");
        
//        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.CH4_Dd()))
//        {
//            error = "Failed to wait for CH4 Dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.CH4_Dd()))
//        {
//            error = "Failed to click on CH4 Dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.CH4_option()))
//        {
//            error = "Failed to wait for CH4 Dropdown option.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.CH4_option()))
//        {
//            error = "Failed to click on CH4 Dropdown option.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully selected a CH4 option option.");
        
        //N2O
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.N2O_input()))
        {
            error = "Failed to wait for N2O input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Emission_Factors_Maintenance_PageObjects.N2O_input(), getData("N2O Input")))
        {
            error = "Failed to enter N2O input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered a N2O.");
        
//        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.N2O_Dd()))
//        {
//            error = "Failed to wait for N2O Dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.N2O_Dd()))
//        {
//            error = "Failed to click on N2O Dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.N2O_option()))
//        {
//            error = "Failed to wait for N2O Dropdown option.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.N2O_option()))
//        {
//            error = "Failed to click on N2O Dropdown option.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully selected a N2O option option.");
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.Button_Save1()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.Button_Save1()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Emission_Factors_Maintenance_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Emission_Factors_Maintenance_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Emission_Factors_Maintenance_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Emission_Factors_Maintenance_PageObjects.setRecord_Number(record[2]);
        String record_ = Emission_Factors_Maintenance_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Emission_Factors_Maintenance_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Emission_Factors_Maintenance_PageObjects.Emission_Factors_Maintenance_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Omphile.Mphahlele
 */
@KeywordAnnotation(
        Keyword = "FR3-Edit Emission Factor Maintenance - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Edit_Emission_Factor_Maintenance_MainScenario extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR3_Edit_Emission_Factor_Maintenance_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }
    
     public TestResult executeTest()
    {
        if (!NavigateToEmissionFactorsMaintenance())
        {
            return narrator.testFailed("Navigate To Emission Factors Maintenance Failed due :" + error);
        }
        if (!EditEmissionFactorsMaintenanceValues())
        {
            return narrator.testFailed("Capture Emission Factors Maintenance Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Emission Factors Maintenance");
    }
     
     public boolean NavigateToEmissionFactorsMaintenance()
    {
        //Navigate to ECO2Man tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.Eco2ManTab()))
        {
            error = "Failed to wait for ECO2Man tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.Eco2ManTab()))
        {
            error = "Failed to click on ECO2Man tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to ECO2Man tab.");
        
        //Maintenance tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.MonitoringMaintenanceTab()))
        {
            error = "Failed to wait for Monitoring Maintenance tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.MonitoringMaintenanceTab()))
        {
            error = "Failed to click on Monitoring Maintenance tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Monitoring Maintenance tab.");
        
        //Emission Factors tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.EmissionFactorsTab()))
        {
            error = "Failed to wait for Emission Factors tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.EmissionFactorsTab()))
        {
            error = "Failed to click on Emission Factors tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Emission Factors tab.");
        pause(2000);
        
        //Search
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.searchBtn()))
        {
            error = "Failed to wait for Search button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.searchBtn()))
        {
            error = "Failed to click on Search button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Search button.");
        pause(2000);
        
        //Emission Factor Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.emissionFactor_Record()))
        {
            error = "Failed to wait for Emission Factor Record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.emissionFactor_Record()))
        {
            error = "Failed to click on Emission Factor Record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected Emission Factor record.");
        pause(2000);
        
        return true;
    }
     
    public boolean EditEmissionFactorsMaintenanceValues()
    {
        pause(2000);
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.EmissionFactorsMaintenance_processflow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.EmissionFactorsMaintenance_processflow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //Emission Factor Database
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.emissionFactorDatabaseDropDown()))
        {
            error = "Failed to wait for 'Emission Factor Database' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.emissionFactorDatabaseDropDown()))
        {
            error = "Failed to click 'Emission Factor Database' dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.Text(getData("Emission Factor Database"))))
        {
            error = "Failed to wait for Emission Factor Database dropdown option : " + getData("Emission Factor Database");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.Text(getData("Emission Factor Database"))))
        {
            error = "Failed to click on Emission Factor Database dropdown option : " + getData("Emission Factor Database");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected Emission Factor Database.");

//        

        //iFrame
//        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.iframeXpath()))
//        {
//            error = "Failed to switch to frame.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.switchToFrameByXpath(Emission_Factors_Maintenance_PageObjects.iframeXpath()))
//        {
//            error = "Failed to switch to frame.";
//            return false;
//        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Emission_Factors_Maintenance_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Emission_Factors_Maintenance_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Emission_Factors_Maintenance_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Emission_Factors_Maintenance_PageObjects.setRecord_Number(record[2]);
        String record_ = Emission_Factors_Maintenance_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }
}

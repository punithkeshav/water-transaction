/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Emission_Factors_Maintenance_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Emission_Factors_Maintenance_PageObjects.Emission_Factors_Maintenance_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Omphile.Mphahlele
 */
@KeywordAnnotation(
        Keyword = "FR4-Delete Emission Factor Maintenance - Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_Delete_Emission_Factor_Maintenance_MainScenario extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR4_Delete_Emission_Factor_Maintenance_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }
    
    public TestResult executeTest()
    {
        if (!NavigateToEmissionFactorsMaintenance())
        {
            return narrator.testFailed("Navigate To Emission Factors Maintenance Failed due :" + error);
        }
        if (!DeleteEmissionFactorsMaintenanceValues())
        {
            return narrator.testFailed("Capture Emission Factors Maintenance Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Emission Factors Maintenance");
    }
    
    public boolean NavigateToEmissionFactorsMaintenance()
    {
        //Navigate to ECO2Man tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.Eco2ManTab()))
        {
            error = "Failed to wait for ECO2Man tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.Eco2ManTab()))
        {
            error = "Failed to click on ECO2Man tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to ECO2Man tab.");
        
        //Maintenance tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.MonitoringMaintenanceTab()))
        {
            error = "Failed to wait for Monitoring Maintenance tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.MonitoringMaintenanceTab()))
        {
            error = "Failed to click on Monitoring Maintenance tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Monitoring Maintenance tab.");
        
        //Emission Factors tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.EmissionFactorsTab()))
        {
            error = "Failed to wait for Emission Factors tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.EmissionFactorsTab()))
        {
            error = "Failed to click on Emission Factors tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Emission Factors tab.");
        pause(2000);
        
        //Search
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.searchBtn()))
        {
            error = "Failed to wait for Search button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.searchBtn()))
        {
            error = "Failed to click on Search button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Search button.");
        pause(2000);
        
        //Emission Factor Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.emissionFactor_Record()))
        {
            error = "Failed to wait for Emission Factor Record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.emissionFactor_Record()))
        {
            error = "Failed to click on Emission Factor Record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected Emission Factor record.");
        pause(2000);
        
        return true;
    }
    
    public boolean DeleteEmissionFactorsMaintenanceValues()
    {
        //Delete Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.delete_btn()))
        {
            error = "Failed to wait for Delete button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.delete_btn()))
        {
            error = "Failed to click on Delete button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the Delete button.");
        pause(2000);
        
       if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted Successfully");

//        if (!SeleniumDriverInstance.switchToTabOrWindow())
//        {
//            error = "Failed to switch tab.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Emission_Factors_Maintenance_PageObjects.ButtonOK()))
//        {
//            error = "Failed to wait for the ok button";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Emission_Factors_Maintenance_PageObjects.ButtonOK()))
//        {
//            error = "Failed to click the ok button";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);
        
        return true;
    }
}

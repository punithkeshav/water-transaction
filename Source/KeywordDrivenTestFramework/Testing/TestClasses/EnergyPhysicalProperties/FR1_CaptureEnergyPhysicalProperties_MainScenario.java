/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EnergyPhysicalProperties;

import KeywordDrivenTestFramework.Testing.TestClasses.CarbonPrice.*;
import KeywordDrivenTestFramework.Testing.TestClasses.EmissionLinking.*;
import KeywordDrivenTestFramework.Testing.TestClasses.BiodiversityMonitoring.*;
import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.EnergyPhysicalProperties.EnergyPhysicalProperties_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Energy Physical Properties Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_CaptureEnergyPhysicalProperties_MainScenario extends BaseClass
{

    String error = "";

    public FR1_CaptureEnergyPhysicalProperties_MainScenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!CaptureEnergyPhysicalProperties())
        {
            return narrator.testFailed("Failed To Capture Energy Physical Properties Due To : " + error);
        }

        return narrator.finalizeTest("Successfully Capture Capture Energy Physical Properties");
    }

    public boolean CaptureEnergyPhysicalProperties()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.ECO2ManLabel()))
        {
            error = "Failed to wait for ECO2Man";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.ECO2ManLabel()))
        {
            error = "Failed to click on ECO2Man";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked ECO2ManLabel");

        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.MonitoringMaintence()))
        {
            error = "Failed to wait for Monitoring Maintenance";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.MonitoringMaintence()))
        {
            error = "Failed to click on Monitoring Maintenance";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Monitoring Maintenance");

        //Carbon Price
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.EnergyPhysicalPropertiesLabel()))
        {
            error = "Failed to wait for Energy Physical Properties";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.EnergyPhysicalPropertiesLabel()))
        {
            error = "Failed to click on Energy Physical Properties";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Energy Physical Properties");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.AddButton()))
        {
            error = "Failed to locate add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.AddButton()))
        {
            error = "Failed to click on add button";
            return false;
        }

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow in Add phase");

        //Business Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to locate Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EnergyPhysicalProperties_PageObjects.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to wait for Business Unit :" + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(EnergyPhysicalProperties_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        pause(3000);
//        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.businessUnitOption1(getData("Business unit"))))
//        {
//            error = "Failed to wait to expand Business Unit";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(EmissionLinking_PageObjects.businessUnitOption1(getData("Business unit"))))
//        {
//            error = "Failed to expand Business Unit";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Emission factor database
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.EmissionSourceDropDown()))
        {
            error = "Failed to wait for Emission source drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.EmissionSourceDropDown()))
        {
            error = "Failed to click Emission sourcedrop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EnergyPhysicalProperties_PageObjects.TypeSearch2(), getData("Emission source option")))

        {
            error = "Failed to enter Emission source option :" + getData("Emission source option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(EnergyPhysicalProperties_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(4000);

        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.businessUnitOption1(getData("Emission source 1"))))
        {
            error = "Failed to wait for Emission source:" + getData("Emission source 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.businessUnitOption1(getData("Emission source 1"))))
        {
            error = "Failed to click Emission source Option drop down :" + getData("Emission source 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.businessUnitOption1(getData("Emission source 2"))))
        {
            error = "Failed to wait for Emission source Option drop down :" + getData("Emission source 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.businessUnitOption1(getData("Emission source 2"))))
        {
            error = "Failed to click  Emission source Option drop down :" + getData("Emission source 2");
            return false;
        }


        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.Text2(getData("Emission source option"))))
        {
            error = "Failed to wait for Emission source drop down option : " + getData("Emission source option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.Text2(getData("Emission source option"))))
        {
            error = "Failed to click Emission source drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Emission source option  :" + getData("Emission source option"));

      

        //Density
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.Density()))
        {
            error = "Failed to wait for Carbon price text box";
            return false;
        }
        
       

        if (!SeleniumDriverInstance.enterTextByXpath(EnergyPhysicalProperties_PageObjects.Density(), "" + 26))

        {
            error = "Failed to enter Density :" + 26;
            return false;
        }


        if (!SeleniumDriverInstance.enterTextByXpath(EnergyPhysicalProperties_PageObjects.DensitySource(), getData("Density source")))

        {
            error = "Failed to enter Start date : " +getData("Density source");
            return false;
        }
        
          if (!SeleniumDriverInstance.enterTextByXpath(EnergyPhysicalProperties_PageObjects.NCV(), "" + 26))

        {
            error = "Failed to enter NCV :" + 26;
            return false;
        }
          
          
        if (!SeleniumDriverInstance.enterTextByXpath(EnergyPhysicalProperties_PageObjects.NCVSource(), getData("NCV source")))

        {
            error = "Failed to enter NCV source : " +getData("NCV source");
            return false;
        }
        
        

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.SaveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }
        //pause();

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(EnergyPhysicalProperties_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EnergyPhysicalProperties_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EnergyPhysicalProperties_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EnergyPhysicalProperties_PageObjects.setRecord_Number(record[2]);
        String record_ = EnergyPhysicalProperties_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;
    }

}

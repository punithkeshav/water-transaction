/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.EnergyPhysicalProperties;

import KeywordDrivenTestFramework.Testing.TestClasses.CarbonPrice.*;
import KeywordDrivenTestFramework.Testing.TestClasses.EmissionLinking.*;
import KeywordDrivenTestFramework.Testing.TestClasses.BiodiversityMonitoring.*;
import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.EnergyPhysicalProperties.EnergyPhysicalProperties_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteHomePageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.WaterTransaction_PageObjects;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR2-Edit Energy Physical Properties",
        createNewBrowserInstance = false
)
public class FR2_EditEnergyPhysicalProperties_MainScenario extends BaseClass
{

    String error = "";

    public FR2_EditEnergyPhysicalProperties_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!EditEnergyPhysicalProperties())
        {
            return narrator.testFailed("Failed to Edit Energy Physical Properties  due to :" + error);
        }

        return narrator.finalizeTest("Successfully edited Edit Energy Physical Properties record");
    }

    public boolean EditEnergyPhysicalProperties() throws InterruptedException
    {

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EnergyPhysicalProperties_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

   
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");


        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(EnergyPhysicalProperties_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }

        pause(6000);
        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }
        
        
         //Emission factor database
         if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.EmissionSourceDropDown()))
        {
            error = "Failed to wait for Emission source drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.EmissionSourceDropDown()))
        {
            error = "Failed to click Emission sourcedrop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(EnergyPhysicalProperties_PageObjects.TypeSearch2(), getData("Emission source option")))

        {
            error = "Failed to enter Emission source option :" + getData("Emission source option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(EnergyPhysicalProperties_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(4000);

        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.businessUnitOption1(getData("Emission source 1"))))
        {
            error = "Failed to wait for Emission source:" + getData("Emission source 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.businessUnitOption1(getData("Emission source 1"))))
        {
            error = "Failed to click Emission source Option drop down :" + getData("Emission source 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.businessUnitOption1(getData("Emission source 2"))))
        {
            error = "Failed to wait for Emission source Option drop down :" + getData("Emission source 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.businessUnitOption1(getData("Emission source 2"))))
        {
            error = "Failed to click  Emission source Option drop down :" + getData("Emission source 2");
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Emission source 3"))))
//        {
//            error = "Failed to wait for Emission source Option drop down :" + getData("Emission source 3");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(CarbonPrice_PageObjects.businessUnitOption1(getData("Emission source 3"))))
//        {
//            error = "Failed to click  Emission source Option drop down :" + getData("Emission source 3");
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.Text2(getData("Emission source option"))))
        {
            error = "Failed to wait for Emission source drop down option : " + getData("Emission source option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.Text2(getData("Emission source option"))))
        {
            error = "Failed to click Emission source drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Emission source option  :" + getData("Emission source option"));

        

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.SaveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnergyPhysicalProperties_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(EnergyPhysicalProperties_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(EnergyPhysicalProperties_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(EnergyPhysicalProperties_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(EnergyPhysicalProperties_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        EnergyPhysicalProperties_PageObjects.setRecord_Number(record[2]);
        String record_ = EnergyPhysicalProperties_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

       

        return true;
    }

}

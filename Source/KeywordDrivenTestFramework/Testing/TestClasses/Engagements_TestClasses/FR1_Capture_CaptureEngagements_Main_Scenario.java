/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_PageObjects.Engagements_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR1 Capture Engagements Main Scenario",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class FR1_Capture_CaptureEngagements_Main_Scenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_CaptureEngagements_Main_Scenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_An_Event())
        {
            return narrator.testFailed("Failed To Capture Engagements Due To :" + error);
        }
        if (getData("Execute").equalsIgnoreCase("True"))
        {
            uploadSupportingDocuments();
        }

        return narrator.finalizeTest("Successfully Captured An Engagements");
    }

    public boolean Capture_An_Event()
    {
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.EventManagemantTab()))
        {
            error = "Failed to wait for Engagement Management Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.EventManagemantTab()))
        {
            error = "Failed to click Engagement Management Tab";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Event Managemant");

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.AddButton()))
        {
            error = "Failed to wait for the add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.AddButton()))
        {
            error = "Failed to click the add button";
            return false;
        }

        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ProcessFlowButton()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ProcessFlowButton()))
        {
            error = "Failed to click the process flow button";
            return false;
        }

        //Event title 
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.EngagementTitle()))
        {
            error = "Failed to wait for  Engagement title text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.EngagementTitle(), getData("Engagement title")))
        {
            error = "Failed to enter Engagement title :" + getData("Engagement title");
            return false;
        }

        narrator.stepPassed("Engagement title :" + getData("Engagement title"));

        //Engagement date
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.EngagementDate()))
        {
            error = "Failed to wait for Engagement date text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.EngagementDate(), startDate))
        {
            error = "Failed to enter Engagement date :" + startDate;
            return false;
        }

        narrator.stepPassed("Engagement date :" + getData("Engagement title"));

        //Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.BusinessUnitDropDown()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.BusinessUnitDropDown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to wait for Business Unit option :" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down:" + getData("Business unit 5");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        if (getData("Link to project").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.LinkToProjectCheckBox()))
            {
                error = "Failed to wait for Link to project";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.LinkToProjectCheckBox()))
            {
                error = "Failed to click Link to project";
                return false;
            }

            //Project
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ProjectDropDown()))
            {
                error = "Failed to wait for project drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ProjectDropDown()))
            {
                error = "Failed to click project drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Project text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch(), getData("Project")))
            {
                error = "Failed to enter Project option :" + getData("Project");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ProjectCheckBox(getData("Project"))))
            {
                error = "Failed to wait for project check box";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ProjectCheckBox(getData("Project"))))
            {
                error = "Failed to click project check box";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ProjectDropDown()))
            {
                error = "Failed to click project drop down";
                return false;
            }

        }

        //Function of engagement 
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.FunctionOfEngagementDropDown()))
        {
            error = "Failed to wait for Function of engagement drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.FunctionOfEngagementDropDown()))
        {
            error = "Failed to click Function of engagement drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Function of engagement")))
        {
            error = "Failed to enter Function of engagement :" + getData("Function of engagement");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Check_box(getData("Function of engagement"))))
        {
            error = "Failed to wait for Function of engagement :" + getData("Function of engagement");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Check_box(getData("Function of engagement"))))
        {
            error = "Failed to click Function of engagement :" + getData("Function of engagement");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.FunctionOfEngagementDropDown()))
        {
            error = "Failed to click Function of engagement drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Function of engagement : " + getData("Function of engagement"));

        //Responsible person
//        if (SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ResponsiblePersonDropDown()))
//        {
//            error = "Failed to wait for Responsible person drop down";
//            return false;
//        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ResponsiblePersonDropDown()))
        {
            error = "Failed to click on Responsible person drop down";

            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch(), getData("Responsible person")))
        {
            error = "Failed to enter Responsible person :" + getData("Responsible person");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to wait for Responsible person drop down option : " + getData("Responsible person");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Engagements_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to click Responsible person drop down option : " + getData("Responsible person");
            return false;
        }

        narrator.stepPassed("Responsible person:" + getData("Responsible person"));

        //Method of engagement
        pause(3000);
//        if (SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.MethodOfEngagementDropDown()))
//        {
//            error = "Failed to wait for Method of engagement drop down";
//            return false;
//        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.MethodOfEngagementDropDown()))
        {
            error = "Failed to click on Method of engagement drop down";

            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Method of engagement")))
        {
            error = "Failed to enter Method of engagement :" + getData("Method of engagement");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text2(getData("Method of engagement"))))
        {
            error = "Failed to wait for Method of engagement drop down option : " + getData("Method of engagement");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Engagements_PageObjects.Text2(getData("Method of engagement"))))
        {
            error = "Failed to click Method of engagement drop down option : " + getData("Method of engagement");
            return false;
        }

        narrator.stepPassed("Method of engagement :" + getData("Method of engagement"));

        //Contact inquiry / topic
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ContactInquiryTopicDropDown()))
        {
            error = "Failed to wait for Contact inquiry / topic drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ContactInquiryTopicDropDown()))
        {
            error = "Failed to click Contact inquiry / topic drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Contact inquiry / topic")))
        {
            error = "Failed to enter Contact inquiry / topic :" + getData("Contact inquiry / topic");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Check_box(getData("Contact inquiry / topic"))))
        {
            error = "Failed to wait for Contact inquiry / topic :" + getData("Contact inquiry / topic");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Check_box(getData("Contact inquiry / topic"))))
        {
            error = "Failed to click Contact inquiry / topic :" + getData("Contact inquiry / topic");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ContactInquiryTopicDropDown()))
        {
            error = "Failed to click Contact inquiry / topic drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Contact inquiry / topic : " + getData("Contact inquiry / topic"));

        //Confidential
        if (getData("Confidential").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ConfidentialCheckBox()))
            {
                error = "Failed to wait for Confidential check box";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ConfidentialCheckBox()))
            {
                error = "Failed to click Confidential check box";
                return false;
            }

        }

        //Event description
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.EngagementDescription()))
        {
            error = "Failed to wait for Event description text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.EngagementDescription(), getData("Engagement description")))
        {
            error = "Failed to enter Engagement description:" + getData("Engagement description");
            return false;
        }

        narrator.stepPassed("Engagement description :" + getData("Engagement description"));

        if (getData("Linked commitments execute").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.LinkedCommitmentsDropDown()))
            {
                error = "Failed to wait for Linked commitments drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.LinkedCommitmentsDropDown()))
            {
                error = "Failed to click Linked commitments drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Linked commitments")))
            {
                error = "Failed to enter Linked commitments :" + getData("Linked commitments");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Check_box(getData("Linked commitments"))))
            {
                error = "Failed to wait for Linked commitments :" + getData("Linked commitments");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Check_box(getData("Linked commitments"))))
            {
                error = "Failed to click Linked commitments :" + getData("Linked commitments");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.LinkedCommitmentsDropDown()))
            {
                error = "Failed to click Linked commitments drop down";
                return false;
            }
            narrator.stepPassedWithScreenShot("Linked commitments : " + getData("Linked commitments"));

        }

        if (getData("Related permits execute").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.RelatedPermitsDropDown()))
            {
                error = "Failed to wait for Related permits drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.RelatedPermitsDropDown()))
            {
                error = "Failed to click Related permits drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch(), getData("Related permits option")))
            {
                error = "Failed to enter Related permits :" + getData("Related permits option");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Related permits"))))
            {
                error = "Failed to wait for Related permits :" + getData("Related permits");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Related permits"))))
            {
                error = "Failed to click Related permits Option drop down :" + getData("Related permits");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Check_box(getData("Related permits option"))))
            {
                error = "Failed to wait for Related permits :" + getData("Related permits option");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Check_box(getData("Related permits option"))))
            {
                error = "Failed to click Related permits :" + getData("Related permits option");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.RelatedPermitsDropDown()))
            {
                error = "Failed to click Related permits drop down";
                return false;
            }
            narrator.stepPassedWithScreenShot("Related permits option : " + getData("Related permits option"));

        }

        if (getData("Linked grievances execute").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.LinkedGrievancesDropDown()))
            {
                error = "Failed to wait for Linked grievances drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.LinkedGrievancesDropDown()))
            {
                error = "Failed to click Linked grievances drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch(), getData("Linked grievances")))
            {
                error = "Failed to enter Linked grievances :" + getData("Linked grievances");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Check_box(getData("Linked grievances"))))
            {
                error = "Failed to wait for Contact inquiry / topic :" + getData("Linked grievances");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Check_box(getData("Linked grievances"))))
            {
                error = "Failed to click Linked grievances :" + getData("Linked grievances");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.LinkedGrievancesDropDown()))
            {
                error = "Failed to click Linked grievances drop down";
                return false;
            }
            narrator.stepPassedWithScreenShot("Linked grievances : " + getData("Linked grievances"));

        }

      

        //Geographic location
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.GeographicLocationDropDown()))
        {
            error = "Failed to wait for Geographic location drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.GeographicLocationDropDown()))
        {
            error = "Failed to click Geographic location drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Geographic location text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Geographic location")))
        {
            error = "Failed to enter Geographic location:" + getData("Geographic location");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text2(getData("Geographic location"))))
        {
            error = "Failed to wait for Geographic location";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text2(getData("Geographic location"))))
        {
            error = "Failed to click Geographic location";
            return false;
        }

        narrator.stepPassedWithScreenShot("Geographic location :" + getData("Geographic location"));

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Engagements_PageObjects.setRecord_Number(record[2]);
        String record_ = Engagements_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

    public boolean uploadSupportingDocuments()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to wait for Supporting Documents button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to click on Supporting Documents button.";
            return false;
        }

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.linkADoc_button()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(Engagements_PageObjects.linkADoc_button()))
        {
            error = "Failed to scroll to 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.linkADoc_button()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.urlInput_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.urlInput_TextAreaxpath(), getData("Document Link")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.tile_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.tile_TextAreaxpath(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagements_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Engagements_PageObjects.setRecord_Number(record[2]);
        String record_ = Engagements_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;

    }

}

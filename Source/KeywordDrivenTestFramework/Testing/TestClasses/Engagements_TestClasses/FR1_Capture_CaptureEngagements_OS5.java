/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_PageObjects.Engagements_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR1 Capture Engagements OS5",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class FR1_Capture_CaptureEngagements_OS5 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_CaptureEngagements_OS5()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_An_Event())
        {
            return narrator.testFailed("Failed To Capture Engagements Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured An Engagements");
    }

    public boolean Capture_An_Event()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.GrievanceAddButton()))
        {
            error = "Failed to wait for the grievance add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.GrievanceAddButton()))
        {
            error = "Failed to click the grievance add button";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to tab";
            return false;
        }

        pause(5000);

        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagements_PageObjects.iframe()))
        {
            error = "Failed to switch to tab";
            return false;
        }

        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.EventProcessFlowButton()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.EventProcessFlowButton()))
        {
            error = "Failed to click the process flow button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeofEventDropDown()))
        {
            error = "Failed to wait for Type of event down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.TypeofEventDropDown()))
        {
            error = "Failed to click Type of event drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Type of event search text box";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text(getData("Type of Event"))))
        {
            error = "Failed to click Type of event :" + getData("Type of Event");
            return false;
        }
        narrator.stepPassedWithScreenShot("Type of Event :" + getData("Type of Event"));

        //Event title 
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.EventTitle()))
        {
            error = "Failed to wait for  Event title text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.EventTitle(), getData("Event title")))
        {
            error = "Failed to enter Event title :" + getData("Event title");
            return false;
        }

        narrator.stepPassedWithScreenShot("Event title :" + getData("Event title"));

        //Event description
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.EventDescription()))
        {
            error = "Failed to wait for Event description text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.EventDescription(), getData("Event description")))
        {
            error = "Failed to enter Event description :" + getData("Event description");
            return false;
        }

        narrator.stepPassedWithScreenShot("Event description :" + getData("Event description"));

        //Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitDropDown()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitDropDown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to wait for Business Unit option :" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down:" + getData("Business unit 5");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Functional location
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.FunctionalLocationDropDown()))
        {
            error = "Failed to wait for Functional location drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.FunctionalLocationDropDown()))
        {
            error = "Failed to click Functional location drop down";
            return false;
        }


        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Functional location   text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Functional location  option")))
        {
            error = "Failed to enter Functional location option:" + getData("Functional location  option");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(7000);


        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 1"))))
        {
            error = "Failed to click Functional location Option drop down";
            return false;
        }
//
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 2"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 2"))))
        {
            error = "Failed to click  Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 3"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 3"))))
        {
            error = "Failed to click  Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 4"))))
        {
            error = "Failed to wait Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 4"))))
        {
            error = "Failed to click  Functional location Option drop down";
            return false;
        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 5"))))
//        {
//            error = "Failed to wait for Functional location Option drop down";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 5"))))
//        {
//            error = "Failed to wait for Functional location Option drop down";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text2(getData("Functional location  option"))))
        {
            error = "Failed to wait for Functional location drop down option : " + getData("Functional location  option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text2(getData("Functional location  option"))))
        {
            error = "Failed to click  Functional location drop down option : " + getData("Functional location  option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Functional location :" + getData("Functional location  option"));

        //Specific location
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Specific_location()))
        {
            error = "Failed to wait for Specific location text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.Specific_location(), getData("Specific location")))
        {
            error = "Failed to enter Specific location :" + getData("Specific location");
            return false;
        }

//        //Pin to map
        if (getData("Pin To Map").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.PinToMap()))
            {
                error = "Failed to wait for Pin to map";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.PinToMap()))
            {
                error = "Failed to click Pin to map";
                return false;
            }

        }

        narrator.stepPassedWithScreenShot("Pin To Map");
        //Link to projects
        if (getData("Link to projects").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.LinkToProjects()))
            {
                error = "Failed To Wait For Link To Projects";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.LinkToProjects()))
            {
                error = "Failed To Click On Link To Projects";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.LinkToProjectsDropDown()))
            {
                error = "Failed To Wait For Link To Projects Drop Down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.LinkToProjectsDropDown()))
            {
                error = "Failed To click Link To Projects Drop Down";
                return false;
            }

            pause(3000);
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Check_box(getData("Project"))))
            {
                error = "Failed to wait for Project :" + getData("Project");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Check_box(getData("Project"))))
            {
                error = "Failed to click Project :" + getData("Project");
                return false;
            }
            narrator.stepPassed("Project :" + getData("Project"));
        }
        narrator.stepPassedWithScreenShot("Link to projects");

        //Date of event
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.DateOfEvent()))
        {
            error = "Failed to wait for Date of event";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.DateOfEvent(), startDate))
        {
            error = "Failed to enter Date of event";
            return false;
        }
        narrator.stepPassedWithScreenShot("Date of event", startDate);

//        //Date reported
//        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.DateReported()))
//        {
//            error = "Failed to wait for Date reported";
//            return false;
//        }
//        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.DateReported(), startDate))
//        {
//            error = "Failed to enter Date reported";
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Date reported", startDate);

        //Time of event
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TimeOfEvent()))
        {
            error = "Failed to wait for Time of event";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TimeOfEvent(), startDate))
        {
            error = "Failed to enter Time of event";
            return false;
        }

//        //Time reported
//        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TimeReported()))
//        {
//            error = "Failed to wait for Time reported";
//            return false;
//        }
//        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TimeReported(), startDate))
//        {
//            error = "Failed to enter Time reported";
//            return false;
//        }
        //Immediate action taken
        if (SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ImmediateActionTaken()))
        {

            if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.ImmediateActionTaken(), getData("Immediate action taken")))
            {
                error = "Failed to enter Immediate action taken";
                return false;
            }
            narrator.stepPassed("Immediate action taken : " + getData("Immediate action taken"));

        }


        //Validator
        if (SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ValidatorDropDown()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ValidatorDropDown()))
            {
                error = "Failed to click Validator drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Validator  text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch(), getData("Validator")))
            {
                error = "Failed to enter Validator  option :" + getData("Validator");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text2(getData("Validator"))))
            {
                error = "Failed to wait for Validator  drop down option : " + getData("Validator");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text2(getData("Validator"))))
            {
                error = "Failed to click Validator  drop down option : " + getData("Validator");
                return false;
            }

            narrator.stepPassedWithScreenShot("Validator  :" + getData("Validator"));
        }

        //Responsible supervisor
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ResponsibleSupervisorDropDown()))
        {
            error = "Failed to wait for Responsible supervisor drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ResponsibleSupervisorDropDown()))
        {
            error = "Failed to click Responsible supervisor drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Responsible supervisor text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch(), getData("Responsible supervisor")))
        {
            error = "Failed to enter Responsible supervisor option :" + getData("Responsible supervisor");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(7000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text2(getData("Reported by"))))
        {
            error = "Failed to wait for Reported by drop down option : " + getData("Reported by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text2(getData("Reported by"))))
        {
            error = "Failed to click Responsible supervisor drop down option : " + getData("Reported by");
            return false;
        }

        narrator.stepPassed("Responsible supervisor :" + getData("Responsible supervisor"));

        //Reported by
        if (getData("Reported By Check").equalsIgnoreCase("True"))
        {
            if (SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ReportedByCheckBox()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ReportedByCheckBox()))
                {
                    error = "Failed to click Reported by check box";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ReportedByDropDown()))
                {
                    error = "Failed to wait for Reported bydrop down";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ReportedByDropDown()))
                {
                    error = "Failed to click Reported by drop down";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch()))
                {
                    error = "Failed to wait for Reported bytext box.";
                    return false;
                }

                if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch(), getData("Reported by")))
                {
                    error = "Failed to enter Reported by option :" + getData("Reported by");
                    return false;
                }
                if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch()))
                {
                    error = "Failed to press enter";
                    return false;
                }
                if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text(getData("Reported by"))))
                {
                    error = "Failed to wait for Reported by drop down option : " + getData("Reported by");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text(getData("Reported by"))))
                {
                    error = "Failed to click Reported by drop down option : " + getData("Reported by");
                    return false;
                }

                narrator.stepPassed("Reported by :" + getData("Reported by"));

            }

        }


        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Event_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Event_Save()))
        {
            error = "Failed to click button save";
            return false;
        }


        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Engagements_PageObjects.setRecord_Number(record[2]);
        String record_ = Engagements_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;

    }

}

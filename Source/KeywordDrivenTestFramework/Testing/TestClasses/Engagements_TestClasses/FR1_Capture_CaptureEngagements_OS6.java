/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_PageObjects.Engagements_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR1 Capture Engagements OS 6",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class FR1_Capture_CaptureEngagements_OS6 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_CaptureEngagements_OS6()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_An_Event())
        {
            return narrator.testFailed("Failed To Capture Engagements Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured An Engagements");
    }

    public boolean Capture_An_Event()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.CommitmentAddButton()))
        {
            error = "Failed to wait for the commitment add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.CommitmentAddButton()))
        {
            error = "Failed to click the commitment add button";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to tab";
            return false;
        }

        pause(5000);

        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagements_PageObjects.iframe()))
        {
            error = "Failed to switch to tab";
            return false;
        }

        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.CommitmentProcessFlowButton()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.CommitmentProcessFlowButton()))
        {
            error = "Failed to click the process flow button";
            return false;
        }

        //Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.CommitmentBusinessUnitDropDown()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.CommitmentBusinessUnitDropDown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to wait for Business Unit option :" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down:" + getData("Business unit 5");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Functional location
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.CommitmentFunctionalLocationDropDown()))
        {
            error = "Failed to wait for Functional location drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.CommitmentFunctionalLocationDropDown()))
        {
            error = "Failed to click Functional location drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Functional location   text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Functional location  option")))
        {
            error = "Failed to enter Functional location option:" + getData("Functional location  option");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(7000);

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 1"))))
        {
            error = "Failed to click Functional location Option drop down";
            return false;
        }
//
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 2"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 2"))))
        {
            error = "Failed to click  Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 3"))))
        {
            error = "Failed to wait for Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 3"))))
        {
            error = "Failed to click  Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 4"))))
        {
            error = "Failed to wait Functional location Option drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Functional location 4"))))
        {
            error = "Failed to click  Functional location Option drop down";
            return false;
        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 5"))))
//        {
//            error = "Failed to wait for Functional location Option drop down";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(EventManagemant_PageObjects.businessUnitOption1(getData("Functional location 5"))))
//        {
//            error = "Failed to wait for Functional location Option drop down";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text2(getData("Functional location  option"))))
        {
            error = "Failed to wait for Functional location drop down option : " + getData("Functional location  option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text2(getData("Functional location  option"))))
        {
            error = "Failed to click  Functional location drop down option : " + getData("Functional location  option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Functional location :" + getData("Functional location  option"));

        //Commitment register title
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.CommitmentRegisterTitle()))
        {
            error = "Failed to wait for Commitment register title text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.CommitmentRegisterTitle(), getData("Commitment register title")))
        {
            error = "Failed to enter Commitment register title:" + getData("Commitment register title");
            return false;
        }

        //Commitment register owner
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.CommitmentRegisterOwner()))
        {
            error = "Failed to wait for Commitment register owner drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.CommitmentRegisterOwner()))
        {
            error = "Failed to click  Commitment register owner drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Commitment register owner text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch(), getData("Commitment register owner")))
        {
            error = "Failed to enter Commitment register owner:" + getData("Commitment register owner");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text(getData("Commitment register owner"))))
        {
            error = "Failed to wait for Commitment register owner drop down option : " + getData("Commitment register owner");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text(getData("Commitment register owner"))))
        {
            error = "Failed to click Commitment register owner drop down option : " + getData("Commitment register owner");
            return false;
        }

        narrator.stepPassedWithScreenShot("Commitment register owner :" + getData("Commitment register owner"));
        
        
        

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Commitment_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Commitment_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Engagements_PageObjects.setRecord_Number(record[2]);
        String record_ = Engagements_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }
   
}

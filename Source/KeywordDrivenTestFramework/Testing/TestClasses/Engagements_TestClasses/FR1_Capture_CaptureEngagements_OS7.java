/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_PageObjects.Engagements_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR1 Capture Engagements OS 7",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class FR1_Capture_CaptureEngagements_OS7 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_CaptureEngagements_OS7()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_An_Event())
        {
            return narrator.testFailed("Failed To Capture Engagements Due To :" + error);
        }
    

        return narrator.finalizeTest("Successfully Captured An Engagements");
    }

    public boolean Capture_An_Event()
    {
      

         if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.PermitAddButton()))
        {
            error = "Failed to wait for the Permit add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.PermitAddButton()))
        {
            error = "Failed to click the Permit add button";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to tab";
            return false;
        }

        pause(5000);

        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagements_PageObjects.iframe()))
        {
            error = "Failed to switch to tab";
            return false;
        }

        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.PermitProcessFlowButton()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.PermitProcessFlowButton()))
        {
            error = "Failed to click the process flow button";
            return false;
        }

        //Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.PermitBusinessUnitDropDown()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.PermitBusinessUnitDropDown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to wait for Business Unit option :" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down:" + getData("Business unit 5");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));
        
 
        //Commodity

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.CommodityDropDown()))
        {
            error = "Failed to wait for Commodity drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.CommodityDropDown()))
        {
            error = "Failed to click  Commodity drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Commitment register owner text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Commodity")))
        {
            error = "Failed to enter Commitment register owner:" + getData("Commodity");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text(getData("Commodity"))))
        {
            error = "Failed to wait for Commodity drop down option : " + getData("Commodity");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text(getData("Commodity"))))
        {
            error = "Failed to click Commodity drop down option : " + getData("Commodity");
            return false;
        }

        narrator.stepPassedWithScreenShot("Commodity :" + getData("Commodity"));
        
        
        
        
 
        
        
        
        

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Permit_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Permit_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Engagements_PageObjects.setRecord_Number(record[2]);
        String record_ = Engagements_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

    

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_PageObjects.Engagements_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR2 Capture Team Attendees Main Scenario",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class FR2_CaptureTeamAttendees_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_CaptureTeamAttendees_MainScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_An_Event())
        {
            return narrator.testFailed("Failed To Capture Engagements Due To :" + error);
        }
     
        return narrator.finalizeTest("Successfully Captured An Engagements");
    }

    public boolean Capture_An_Event()
    {
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.AttendeesTab()))
        {
            error = "Failed to wait for Attendees Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.AttendeesTab()))
        {
            error = "Failed to click Attendees Tab";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Attendees");

        //Total non-listed attendees
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TotalnonListedAttendees()))
        {
            error = "Failed to wait for Total non-listed attendees text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TotalnonListedAttendees(), ""+12))
        {
            error = "Failed to enter Total non-listed attendees :" + startDate;
            return false;
        }

        narrator.stepPassed("Total non-listed attendees :" +12);

        //Team Attendees Add
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TeamAttendeesAdd()))
        {
            error = "Failed to wait for Team Attendees add";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.TeamAttendeesAdd()))
        {
            error = "Failed to click Team Attendees add";
            return false;
        }
       //// Team Attendees
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TeamAttendeesDropDown()))
        {
            error = "Failed to wait for Team Attendees Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.TeamAttendeesDropDown()))
        {
            error = "Failed to click Team Attendees Drop Down";
            return false;
        }
 
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Team Attendees text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch(), getData("Team Attendees")))
        {
            error = "Failed to wait for Team Attendees option :" + getData("Team Attendees");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
       
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text(getData("Team Attendees"))))
        {
            error = "Failed to wait for Team Attendees drop down option : " + getData("Team Attendees");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text(getData("Team Attendees"))))
        {
            error = "Failed to click Team Attendees drop down option : " + getData("Team Attendees");
            return false;
        }

        narrator.stepPassedWithScreenShot("Team Attendees option  :" + getData("Team Attendees"));
        
        
        


        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Engagements_PageObjects.setRecord_Number(record[2]);
        String record_ = Engagements_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

    

}

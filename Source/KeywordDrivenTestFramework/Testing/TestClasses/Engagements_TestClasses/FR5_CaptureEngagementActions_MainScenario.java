/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.TopEvent_TestClasses.*;
import KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_PageObjects.Engagements_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR5 - Capture Engagement Actions Main Scenario",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class FR5_CaptureEngagementActions_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR5_CaptureEngagementActions_MainScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!ViewBowtieControls())
        {
            return narrator.testFailed("Failed To View Bowtie Controls Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured View Bowtie Controls");
    }

    public boolean ViewBowtieControls()
    {
        // Actions
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Actions_Tab()))
        {
            error = "Failed to wait for Actions tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Actions_Tab()))
        {
            error = "Failed to click on Actions tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Actions tab.");
        
        
        
        pause(4000);
         if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ActionsAdd()))
        {
            error = "Failed to wait for Actions Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ActionsAdd()))
        {
            error = "Failed to click Actions Add";
            return false;
        }
           
        
       if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ActionsprocessFlow()))
        {
            error = "Failed to locate process flow";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ActionsprocessFlow()))
        {
            error = "Failed to click on process flow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow");
        
        //
    
         if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to click on Type of action drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Type of action")))
        {
            error = "Failed to enter Type of action :" + getData("Type of action");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to wait for Type of action drop down option : " + getData("Type of action");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Engagements_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to click v drop down option : " + getData("Issue category");
            return false;
        }
        narrator.stepPassed("Type of action :" + getData("Type of action"));

        //Action description
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ActionDescription()))
        {
            error = "Failed to wait for Action descriptiontext box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.ActionDescription(), getData("Action description")))
        {
            error = "Failed to enter Action description:" + getData("Action description");
            return false;
        }

        narrator.stepPassedWithScreenShot("Action description " + getData("Action description"));
        
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.EntityDropdown()))
        {
            error = "Failed to locate Entity Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.EntityDropdown()))
        {
            error = "Failed to click on Entity Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Entity text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to wait for Business Unit :" + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Entity:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  EntityOption drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Text2(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Entity option  :" + getData("Business unit option"));

        //Responsible person
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ActionsResponsiblePersonDropDown()))
        {
            error = "Failed to wait for Responsible person drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ActionsResponsiblePersonDropDown()))
        {
            error = "Failed to click on Responsible person drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.TypeSearch(), getData("Responsible person")))
        {
            error = "Failed to enter Responsible person :" + getData("Responsible person");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Engagements_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to wait for Responsible person drop down option : " + getData("Responsible person");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Engagements_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to click Responsible person drop down option : " + getData("Responsible person");
            return false;
        }
        narrator.stepPassed("Responsible person:" + getData("Responsible person"));

        pause(3000);

//            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ActionDueDate()))
//            {
//                error = "Failed to wait for Action due date text box";
//                return false;
//            }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObjects.ActionDueDate(), startDate))
        {
            error = "Failed to enter Action due date :" + startDate;
            return false;
        }

         
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.ActionButton_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.ActionButton_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Engagements_PageObjects.setRecord_Number(record[2]);
        String record_ = Engagements_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}

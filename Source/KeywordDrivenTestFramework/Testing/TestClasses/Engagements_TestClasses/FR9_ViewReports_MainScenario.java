/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_PageObjects.Engagements_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR9 - View Reports Main Scenario",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class FR9_ViewReports_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR9_ViewReports_MainScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_An_Event())
        {
            return narrator.testFailed("Failed To View Engagements Report Due To :" + error);
        }
      

        return narrator.finalizeTest("Successfully Viewed Engagements Report");
    }

    public boolean Capture_An_Event()
    {
        //Reports
        if(!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.Reports())){
            error = "Failed to wait for 'report' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.Reports())){
            error = "Failed to click on 'report' button.";
            return false;
        }
        //View Reports
        if(!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.EngagementReport())){
            error = "Failed to wait for 'Engagement Report";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.EngagementReport())){
            error = "Failed to click Engagement Report";
            return false;
        }
        //Wait View Reports
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to pop-up.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObjects.continueButton())){
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObjects.continueButton())){
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked 'Continue' button.");
        
        if(!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
        pause(30000);
 
        narrator.stepPassedWithScreenShot("Report opened" );

        return true;
    }

  
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Hazard_Categories_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Hazard_Categories_PageObjects.Hazard_Categories_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author OMphahlele
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Hazard Categories - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Hazard_Categories_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Hazard_Categories_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToHazardCategories())
        {
            return narrator.testFailed("Navigate To Hazard Categories Failed due :" + error);
        }
        if (!CaptureHazardCategories())
        {
            return narrator.testFailed("Capture Hazard Categories Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Hazard Categories Points");
    }

    public boolean NavigateToHazardCategories()
    {
        //Navigate to Environmental Sustainability
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.enviroSustainabilityTab()))
        {
            error = "Failed to wait for Environmental Sustainability tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.enviroSustainabilityTab()))
        {
            error = "Failed to click on Environmental Sustainability tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Environmental Sustainability tab.");
        
        //Navigate to Evaluate Performance Maintanance
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.evaluatePerformanceMaintenanceTab()))
        {
            error = "Failed to wait for Evaluate Performance Maintanance tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.evaluatePerformanceMaintenanceTab()))
        {
            error = "Failed to click on Evaluate Performance Maintanance tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Evaluate Performance Maintanance tab.");
        
        //Navigate to Hazard Categories
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.hazardCategoriesTab()))
        {
            error = "Failed to wait for Monitoring Points tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.hazardCategoriesTab()))
        {
            error = "Failed to click on Monitoring Points tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Monitoring Points tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.hazardCategories_AddBtn()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.hazardCategories_AddBtn()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean CaptureHazardCategories()
    {
        pause(2000);
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.hazardCategories_processFlow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.hazardCategories_processFlow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.businessUnit_dropDown()))
        {
            error = "Failed to wait for 'Business Unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.businessUnit_dropDown()))
        {
            error = "Failed to click 'Business Unit' dropdown.";
            return false;
        }
        //Business Unit Expand 1
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.businessUnit_expand1()))
        {
            error = "Failed to wait for 'Business Unit' expand1.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.businessUnit_expand1()))
        {
            error = "Failed to click 'Business Unit' expand1.";
            return false;
        }
        //Business Unit Expand 2
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.businessUnit_expand2()))
        {
            error = "Failed to wait for 'Business Unit' expand2.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.businessUnit_expand2()))
        {
            error = "Failed to click 'Business Unit' expand2.";
            return false;
        }
        //Business Unit Expand 3
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.businessUnit_expand3()))
        {
            error = "Failed to wait for 'Business Unit' expand3.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.businessUnit_expand3()))
        {
            error = "Failed to click 'Business Unit' expand3.";
            return false;
        }
        //Business Unit Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.businessUnit_selection()))
        {
            error = "Failed to wait for 'Business Unit' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.businessUnit_selection()))
        {
            error = "Failed to click 'Business Unit' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Business Unit.");
        //Close Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.closeBusinessUnit_dropDown()))
        {
            error = "Failed to wait for 'Close Business Unit' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.closeBusinessUnit_dropDown()))
        {
            error = "Failed to click 'Close Business Unit' button.";
            return false;
        }
        
        //Type
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.type_inputField()))
        {
            error = "Failed to wait for 'Type' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Hazard_Categories_PageObjects.type_inputField(), getData("Type")))
        {
            error = "Failed to click 'Type' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered a 'Type'.");
        
        //Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.description_inputField()))
        {
            error = "Failed to wait for 'Description' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Hazard_Categories_PageObjects.description_inputField(), getData("Description")))
        {
            error = "Failed to click 'Description' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered a 'Description'.");
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Hazard_Categories_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Hazard_Categories_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Hazard_Categories_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Hazard_Categories_PageObjects.setRecord_Number(record[2]);
        String record_ = Hazard_Categories_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}

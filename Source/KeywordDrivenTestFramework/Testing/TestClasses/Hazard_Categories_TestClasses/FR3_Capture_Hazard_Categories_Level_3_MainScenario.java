/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Hazard_Categories_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Hazard_Categories_PageObjects.Hazard_Categories_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Omphile.Mphahlele
 */

@KeywordAnnotation(
        Keyword = "FR3-Capture Hazard Categories Level 3 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR3_Capture_Hazard_Categories_Level_3_MainScenario extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Capture_Hazard_Categories_Level_3_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToHazardCategoriesLevel3())
        {
            return narrator.testFailed("Navigate To Hazard Categories Level 3 Failed due :" + error);
        }
        if (!CaptureHazardCategoriesLevel3())
        {
            return narrator.testFailed("Capture Hazard Categories Level 3 Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Hazard Categories Level 3");
    }

    public boolean NavigateToHazardCategoriesLevel3()
    {
        //Navigate to Environmental Sustainability
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.enviroSustainabilityTab()))
        {
            error = "Failed to wait for Environmental Sustainability tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.enviroSustainabilityTab()))
        {
            error = "Failed to click on Environmental Sustainability tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Environmental Sustainability tab.");
        
        //Navigate to Evaluate Performance Maintanance
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.evaluatePerformanceMaintenanceTab()))
        {
            error = "Failed to wait for Evaluate Performance Maintanance tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.evaluatePerformanceMaintenanceTab()))
        {
            error = "Failed to click on Evaluate Performance Maintanance tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Evaluate Performance Maintanance tab.");
        
        //Navigate to Hazard Categories
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.hazardCategoriesTab()))
        {
            error = "Failed to wait for Monitoring Points tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.hazardCategoriesTab()))
        {
            error = "Failed to click on Monitoring Points tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Monitoring Points tab.");
        
         pause(1000);
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.search_Btn()))
        {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.search_Btn()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        pause(50000);      
        //2nd Page
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.nextResult_page()))
        {
            error = "Failed to wait for '2nd Page' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.nextResult_page()))
        {
            error = "Failed to click on '2nd Page' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on '2nd Page' button.");
        
        pause(2000);       
        //Record Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.record_Selection()))
        {
            error = "Failed to wait for 'Record'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.record_Selection()))
        {
            error = "Failed to click on 'Record'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Record'.");
        
        pause(2000);
        //Level 2 Select Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level2_recSelection()))
        {
            error = "Failed to wait for Level 2 Record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level2_recSelection()))
        {
            error = "Failed to click Level 2 Record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Level 2 Record.");
        
        pause(2000);
        //Level 3 Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3_addBtn()))
        {
            error = "Failed to wait for Level 3 Add Button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level3_addBtn()))
        {
            error = "Failed to click Level 3 Add Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Level 3 Add Button.");

        return true;
    }
    
    public boolean CaptureHazardCategoriesLevel3()
    {
        pause(2000);
        
        //Level 3 Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3_processFlow()))
        {
            error = "Failed to wait for Level 3 Process Flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level3_processFlow()))
        {
            error = "Failed to click Level 3 Process Flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Level 2 Process Flow.");
        
        //Level 3 Input Field
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3_inputField()))
        {
            error = "Failed to wait for Level 3 Input Field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Hazard_Categories_PageObjects.level3_inputField(), getData("Level 3 Input Field")))
        {
            error = "Failed to entered Level 3 Input Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Level 3 Input Field.");
        
        //Level 3 Description Field
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3_descriptionField()))
        {
            error = "Failed to wait for Level 3 Description Field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Hazard_Categories_PageObjects.level3_descriptionField(), getData("Level 3 Description Field")))
        {
            error = "Failed to entered Level 3 Description Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Level 3 Description Field.");
        
        //Level 3 Consequence Field
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3_consequenceField()))
        {
            error = "Failed to wait for Level 3 Consequence Field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Hazard_Categories_PageObjects.level3_consequenceField(), getData("Level 3 Consequence Field")))
        {
            error = "Failed to entered Level 3 Consequence Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Level 3 Consequence Field.");
        
        //Level 3 Link Field
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3_linkField()))
        {
            error = "Failed to wait for Level 3 Link Field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Hazard_Categories_PageObjects.level3_linkField(), getData("Level 3 Link Field")))
        {
            error = "Failed to entered Level 3 Link Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Level 3 Link Field.");
        
        //Level 3 Link To Business Unit & Type?
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3_linkToBusiness()))
        {
            error = "Failed to wait for 'Level 3 Link To Business Unit & Type?' checkbox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level3_linkToBusiness()))
        {
            error = "Failed to click 'Level 3 Link To Business Unit & Type?' checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Level 3 Link To Business Unit & Type?' checkbox.");
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level3Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }
        
        pause(4000);
        
        //Level 3 Link To Business Unit & Impact Type
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3_linkToBusinessTab()))
        {
            error = "Failed to wait for 'Level 3 Link To Business Unit & Impact Type' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level3_linkToBusinessTab()))
        {
            error = "Failed to click 'Level 3 Link To Business Unit & Impact Type' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Level 3 Link To Business Unit & Impact Type' tab.");
        
        //Level 3 Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3BusinessUnit_dd()))
        {
            error = "Failed to wait for 'Level 3 Business Unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level3BusinessUnit_dd()))
        {
            error = "Failed to click 'Level 3 Business Unit' dropdown.";
            return false;
        }
        //Leve 3 Business Unit Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3BusinessUnit_ddSelection()))
        {
            error = "Failed to wait for 'Level 3 Business Unit' selection.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level3BusinessUnit_ddSelection()))
        {
            error = "Failed to click 'Level 3 Business Unit' selection.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected 'Level 3 Business Unit' selection.");
        
        //Level 3 Impact Type
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.levl3ImpactType_dd()))
        {
            error = "Failed to wait for 'Level 3 Impact Type' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.levl3ImpactType_dd()))
        {
            error = "Failed to click 'Level 3 Impact Type' dropdown.";
            return false;
        }
        //Level 3 Impact Type Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3ImpactType_ddSelection()))
        {
            error = "Failed to wait for 'Level 3 Impact Type' selection.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level3ImpactType_ddSelection()))
        {
            error = "Failed to click 'Level 3 Impact Type' selection.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected 'Level 3 Impact Type' selection.");
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level3Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Hazard_Categories_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Hazard_Categories_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Hazard_Categories_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Hazard_Categories_PageObjects.setRecord_Number(record[2]);
        String record_ = Hazard_Categories_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        
        return true;
    }
}

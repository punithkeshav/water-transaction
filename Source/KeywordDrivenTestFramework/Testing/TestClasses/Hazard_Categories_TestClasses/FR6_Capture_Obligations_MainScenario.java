/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Hazard_Categories_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Hazard_Categories_PageObjects.Hazard_Categories_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Omphile.Mphahlele
 */

@KeywordAnnotation(
        Keyword = "FR6-Capture Obligations - Main Scenario",
        createNewBrowserInstance = false
)
public class FR6_Capture_Obligations_MainScenario extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR6_Capture_Obligations_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToObligations())
        {
            return narrator.testFailed("Navigate To Obligations Failed due :" + error);
        }
        if (!CaptureObligations())
        {
            return narrator.testFailed("Capture Obligations Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Obligations");
    }

    public boolean NavigateToObligations()
    {
        //Navigate to Environmental Sustainability
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.enviroSustainabilityTab()))
        {
            error = "Failed to wait for Environmental Sustainability tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.enviroSustainabilityTab()))
        {
            error = "Failed to click on Environmental Sustainability tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Environmental Sustainability tab.");
        
        //Navigate to Evaluate Performance Maintanance
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.evaluatePerformanceMaintenanceTab()))
        {
            error = "Failed to wait for Evaluate Performance Maintanance tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.evaluatePerformanceMaintenanceTab()))
        {
            error = "Failed to click on Evaluate Performance Maintanance tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Evaluate Performance Maintanance tab.");
        
        //Navigate to Hazard Categories
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.hazardCategoriesTab()))
        {
            error = "Failed to wait for Monitoring Points tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.hazardCategoriesTab()))
        {
            error = "Failed to click on Monitoring Points tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Monitoring Points tab.");
        
        pause(1000);
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.search_Btn()))
        {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.search_Btn()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        pause(70000);      
        //2nd Page
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.nextResult_page()))
        {
            error = "Failed to wait for '2nd Page' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.nextResult_page()))
        {
            error = "Failed to click on '2nd Page' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on '2nd Page' button.");
        
        pause(2000);       
        //Record Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.record_Selection()))
        {
            error = "Failed to wait for 'Record'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.record_Selection()))
        {
            error = "Failed to click on 'Record'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Record'.");
        
        pause(2000);
        //Level 2 Select Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level2_recSelection()))
        {
            error = "Failed to wait for Level 2 Record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level2_recSelection()))
        {
            error = "Failed to click Level 2 Record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Level 2 Record.");
        
        pause(2000);
        //Level 3 Select Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level3_recSelection()))
        {
            error = "Failed to wait for Level 3 Record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level3_recSelection()))
        {
            error = "Failed to click Level 3 Record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Level 3 Record.");
        
        pause(2000);
        //Level 4 Select Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level4_recSelection()))
        {
            error = "Failed to wait for Level 4 Record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level4_recSelection()))
        {
            error = "Failed to click Level 4 Record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Level 4 Record.");
        
        pause(2000);
        //Level 6 Select Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.level5_recSelection()))
        {
            error = "Failed to wait for Level 5 Record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.level5_recSelection()))
        {
            error = "Failed to click Level 5 Record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Level 5 Record.");
        
        pause(2000);
        //Togle In-line Edit
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.inLine_Btn()))
        {
            error = "Failed to wait for In-line Edit Button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.inLine_Btn()))
        {
            error = "Failed to click In-line Edit Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked In-line Edit Button.");
        
        //Obligations Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.obligations_addBtn()))
        {
            error = "Failed to wait for Obligations Add Button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.obligations_addBtn()))
        {
            error = "Failed to click Obligations Add Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Obligations Add Button.");

        return true;
    }
    
    public boolean CaptureObligations()
    {
        pause(2000);
        
        //Obligations Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.obligations_processFlow()))
        {
            error = "Failed to wait for Obligations Process Flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.obligations_processFlow()))
        {
            error = "Failed to click Obligations Process Flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Obligations Process Flow.");
        
        //Obligations Input Field
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.obligations_inputField()))
        {
            error = "Failed to wait for Obligations Input Field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Hazard_Categories_PageObjects.obligations_inputField(), getData("Obligations Input Field")))
        {
            error = "Failed to entered Obligations Input Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Obligations Input Field.");
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.obligationsBtn_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Hazard_Categories_PageObjects.obligationsBtn_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Hazard_Categories_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Hazard_Categories_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Hazard_Categories_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Hazard_Categories_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Hazard_Categories_PageObjects.setRecord_Number(record[2]);
        String record_ = Hazard_Categories_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        
        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Monitoring_Points_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Monitoring_Points_PageObjects.Monitoring_Points_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Omphile.Mphahlele
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Points - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Points_MainScenario extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_Points_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToMonitoringPoints())
        {
            return narrator.testFailed("Navigate To Process Mapping Failed due :" + error);
        }
        if (!CapturePoints())
        {
            return narrator.testFailed("Capture Process Mapping Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Process Mapping");
    }
    
    public boolean NavigateToMonitoringPoints()
    {
       //Navigate to Environmental Sustainability
        if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.enviroSustainabilityTab()))
        {
            error = "Failed to wait for Environmental Sustainability tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Monitoring_Points_PageObjects.enviroSustainabilityTab()))
        {
            error = "Failed to click on Environmental Sustainability tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Environmental Sustainability tab.");
        
        //Navigate to Evaluate Performance Maintanance
        if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.evaluatePerformanceMaintenanceTab()))
        {
            error = "Failed to wait for Evaluate Performance Maintanance tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Monitoring_Points_PageObjects.evaluatePerformanceMaintenanceTab()))
        {
            error = "Failed to click on Evaluate Performance Maintanance tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Evaluate Performance Maintanance tab.");
        
        //Navigate to Monitoring Points
        if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.monitoringPointsTab()))
        {
            error = "Failed to wait for Monitoring Points tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Monitoring_Points_PageObjects.monitoringPointsTab()))
        {
            error = "Failed to click on Monitoring Points tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Monitoring Points tab.");

        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.search_Btn()))
        {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Monitoring_Points_PageObjects.search_Btn()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        pause(8000);
        
        //Record Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.record_Selection()))
        {
            error = "Failed to wait for 'Record'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Monitoring_Points_PageObjects.record_Selection()))
        {
            error = "Failed to click on 'Record'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Record'.");

        return true;
    }
    
    public boolean CapturePoints()
    {
        pause(2000);
        //Add Points
        if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.addPoints_Btn()))
        {
            error = "Failed to wait for Add Points button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Monitoring_Points_PageObjects.addPoints_Btn()))
        {
            error = "Failed to click Add Points button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Add Points button.");
        
        pause(2000);
        //Points Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.points_processFlow()))
        {
            error = "Failed to wait for Points Process Flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Monitoring_Points_PageObjects.points_processFlow()))
        {
            error = "Failed to click Points Process Flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Points Process Flow button.");
        
        //Point Reference
        if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.points_refInput()))
        {
            error = "Failed to wait for Points Point Reference field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Monitoring_Points_PageObjects.points_refInput(), getData("Point Reference")))
        {
            error = "Failed to click Points Point Reference field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Point Reference field.");
        
        //Environmental Classification
        if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.envClassification_dd()))
        {
            error = "Failed to wait for Environmental Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Monitoring_Points_PageObjects.envClassification_dd()))
        {
            error = "Failed to click Environmental Classification dropdown.";
            return false;
        }
        //Environmental Classification Expand
        if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.envClassification_ddExpand()))
        {
            error = "Failed to wait for Environmental Classification dropdown expand.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Monitoring_Points_PageObjects.envClassification_ddExpand()))
        {
            error = "Failed to click Environmental Classification dropdown expand.";
            return false;
        }
        //Environmental Classification Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.envClassification_ddSelection()))
        {
            error = "Failed to wait for Environmental Classification Selection.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Monitoring_Points_PageObjects.envClassification_ddSelection()))
        {
            error = "Failed to click Environmental Classification Selection.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Environmental Classification Selection.");
        
        //Point Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.pointDescription_input()))
        {
            error = "Failed to wait for Point Description field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Monitoring_Points_PageObjects.pointDescription_input(), getData("Point Description")))
        {
            error = "Failed to click Point Description field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Point Description field.");
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.pointsButton_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Monitoring_Points_PageObjects.pointsButton_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Monitoring_Points_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Monitoring_Points_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Monitoring_Points_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Monitoring_Points_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Monitoring_Points_PageObjects.setRecord_Number(record[2]);
        String record_ = Monitoring_Points_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
              
        return true;
    }
}

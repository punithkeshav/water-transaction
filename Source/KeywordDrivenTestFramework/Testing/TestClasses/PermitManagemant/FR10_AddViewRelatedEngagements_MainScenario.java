/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PermitManagemant;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PermitManagement_PageObjects.PermitManagement_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR10- Add/View Related Engagements Main Scenario",
        createNewBrowserInstance = false
)
public class FR10_AddViewRelatedEngagements_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR10_AddViewRelatedEngagements_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Engagements())
        {
            return narrator.testFailed("Add/View Related EngagementsFailed due To :" + error);
        }
        if (getData("Execute Optional").equalsIgnoreCase("True"))
        {
            if (!uploadSupportingDocuments())
            {
                return narrator.testFailed("Failed to upload Supporting Documents: " + error);
            }
        }

        return narrator.finalizeTest("Successfully Add/View Related Engagements");
    }

    public boolean Engagements()
    {
        for (int i = 0; i < 6; i++)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.NextButton()))
            {
                error = "Failed to wait for next button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.NextButton()))
            {
                error = "Failed to click on next button";
                return false;
            }

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Engagements_Tab()))
        {
            error = "Failed to wait for Engagements tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.Engagements_Tab()))
        {
            error = "Failed to click on Engagements tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Engagements tab.");

        if (getData("Execute").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.CreateANewEngagement()))
            {
                error = "Failed to wait for Create a new engagement  Button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.CreateANewEngagement()))
            {
                error = "Failed to click Issues Create a new engagement Button";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully Click Issues Create a new engagement Button");

            //switch to new window
            if (!SeleniumDriverInstance.switchToTabOrWindow())
            {
                error = "Failed to switch to new window or tab.";
                return false;
            }

            if (!SeleniumDriverInstance.switchToFrameByXpath(PermitManagement_PageObjects.iframe()))
            {
                error = "Failed to switch to frame.";
                return false;
            }
            //processflow
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.EngagementsprocessFlow()))
            {
                error = "Failed to locate process flow";
                return false;
            }

            pause(3000);
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.EngagementsprocessFlow()))
            {
                error = "Failed to click on process flow";
                return false;
            }

            narrator.stepPassedWithScreenShot("Process flow");

            //Engagement title
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.EngagementTitle()))
            {
                error = "Failed to wait for Expiration date:";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.EngagementTitle(), getData("Engagement title")))
            {
                error = "Failed to enter Expiration date:" + getData("Engagement title");
                return false;
            }
            narrator.stepPassed("Engagement title :" + getData("Engagement title"));
            //Engagement date
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.EngagementDate()))
            {
                error = "Failed to wait for Engagement date:";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.EngagementDate(), startDate))
            {
                error = "Failed to enter Engagement date:" + startDate;
                return false;
            }
            narrator.stepPassed("Engagement date :" + startDate);

            //Business unit
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.BusinessUnitDropdown()))
            {
                error = "Failed to locate Business unit Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.BusinessUnitDropdown()))
            {
                error = "Failed to click on Business unit Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Business unit text box.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Business unit"))))
            {
                error = "Failed to wait for Business unit drop down option : " + getData("Business unit");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitexpandButton()))
            {
                error = "Failed to wait to expand Business unit";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitexpandButton()))
            {
                error = "Failed to expand Business unit";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 1"))))
            {
                error = "Failed to wait for Business unit:" + getData("Business unit 1");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 1"))))
            {
                error = "Failed to click Business unit Option drop down :" + getData("Business unit 1");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 2"))))
            {
                error = "Failed to wait for Business unit Option drop down :" + getData("Business unit 2");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 2"))))
            {
                error = "Failed to click  Business unit Option drop down :" + getData("Business unit 2");
                return false;
            }
            
//             if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 3"))))
//            {
//                error = "Failed to wait for Business unit Option drop down :" + getData("Business unit 3");
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 3"))))
//            {
//                error = "Failed to click  Business unit Option drop down :" + getData("Business unit 3");
//                return false;
//            }
//            
//            
//            
//             if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 4"))))
//            {
//                error = "Failed to wait for Business unit Option drop down :" + getData("Business unit 4");
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 4"))))
//            {
//                error = "Failed to click  Business unit Option drop down :" + getData("Business unit 4");
//                return false;
//            }
//            
//            
//             if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 5"))))
//            {
//                error = "Failed to wait for Business unit Option drop down :" + getData("Business unit 5");
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 5"))))
//            {
//                error = "Failed to click  Business unit Option drop down :" + getData("Business unit 5");
//                return false;
//            }
            
            
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Business unit option"))))
            {
                error = "Failed to wait for Business unit option option :" + getData("Business unit option");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Business unit option"))))
            {
                error = "Failed to select Business unit option option :" + getData("Business unit option");
                return false;
            }

            narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));


            //Engagement description
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.EngagementDescription()))
            {
                error = "Failed to wait for Issue title text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.EngagementDescription(), getData("Engagement description")))
            {
                error = "Failed to enter Engagement description :" + getData("Engagement description");
                return false;
            }

            narrator.stepPassedWithScreenShot("Engagement description :" + getData("Engagement description"));

            //Contact inquiry / topic
//             if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ContactInquiryTopicDropDown()))
//            {
//                error = "Failed to wait for Contact inquiry / topic drop down";
//                return false;
//            }
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ContactInquiryTopicDropDown()))
            {
                error = "Failed to click on Contact inquiry / topic drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Contact inquiry / topic")))
            {
                error = "Failed to enter Contact inquiry / topic :" + getData("Contact inquiry / topic");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearchCheckBox(getData("Contact inquiry / topic"))))
            {
                error = "Failed to wait for Contact inquiry / topic option :" + getData("Contact inquiry / topic");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.TypeSearchCheckBox(getData("Contact inquiry / topic"))))
            {
                error = "Failed to select Contact inquiry / topic option :" + getData("Business unit option");
                return false;
            }
            narrator.stepPassed("Contact inquiry / topic:" + getData("Contact inquiry / topic"));

//            if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ColapsArrow()))
//            {
//                error = "Failed to wait for colaps arrow";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ColapsArrow()))
//            {
//                error = "Failed to click on colaps arrow";
//                return false;
//            }
            //Responsible person
//            if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ResponsiblePersonDropDown2()))
//            {
//                error = "Failed to wait for Responsible person drop down";
//                return false;
//            }
//
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ResponsiblePersonDropDown2()))
            {
                error = "Failed to click on Responsible person drop down";

                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch(), getData("Responsible person")))
            {
                error = "Failed to enter Responsible person :" + getData("Responsible person");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Responsible person"))))
            {
                error = "Failed to wait for Responsible person drop down option : " + getData("Responsible person");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Responsible person"))))
            {
                error = "Failed to click Responsible person drop down option : " + getData("Responsible person");
                return false;
            }

            narrator.stepPassed("Responsible person:" + getData("Responsible person"));

            //Function of engagement 
//            if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.FunctionOfEngagementDropDown()))
//            {
//                error = "Failed to wait for Function of engagement drop down";
//                return false;
//            }
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.FunctionOfEngagementDropDown()))
            {
                error = "Failed to click on Function of engagement drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Function of engagement")))
            {
                error = "Failed to enter Function of engagement :" + getData("Function of engagement");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearchCheckBox(getData("Function of engagement"))))
            {
                error = "Failed to wait for Function of engagement option :" + getData("Function of engagement");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.TypeSearchCheckBox(getData("Function of engagement"))))
            {
                error = "Failed to select Function of engagement option :" + getData("Business unit option");
                return false;
            }
            narrator.stepPassed("Function of engagement:" + getData("Function of engagement"));

//            if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.FunctionColapsArrow()))
//            {
//                error = "Failed to wait for colaps arrow";
//                return false;
//            }
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.FunctionColapsArrow()))
            {
                error = "Failed to click on colaps arrow";
                return false;
            }

            //Method of engagement
//            if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.MethodOfEngagementDropDown()))
//            {
//                error = "Failed to wait for Method of engagement drop down";
//                return false;
//            }
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.MethodOfEngagementDropDown()))
            {
                error = "Failed to click on Method of engagement drop down";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Method of engagement")))
            {
                error = "Failed to enter Responsible person :" + getData("Method of engagement");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Method of engagement"))))
            {
                error = "Failed to wait for Method of engagement drop down option : " + getData("Method of engagement");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Method of engagement"))))
            {
                error = "Failed to click Method of engagement drop down option : " + getData("Method of engagement");
                return false;
            }
            narrator.stepPassed("Method of engagement :" + getData("Method of engagement"));

            //Geographic location
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.GeographiclocationDropdown()))
            {
                error = "Failed to locate Geographic location Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.GeographiclocationDropdown()))
            {
                error = "Failed to click on Geographic location Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Geographic location text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Geographic location")))
            {
                error = "Failed to wait for Geographic locationt :" + getData("Geographic locationt");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

//
//            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitexpandButton()))
//            {
//                error = "Failed to wait to expand Business Unit";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitexpandButton()))
//            {
//                error = "Failed to expand Business Unit";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Geographic location 1"))))
//            {
//                error = "Failed to wait for Geographic location:" + getData("Business unit 1");
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Geographic location 1"))))
//            {
//                error = "Failed to click Geographic location Option drop down :" + getData("Geographic location 1");
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Geographic location option"))))
//            {
//                error = "Failed to wait for Geographic location drop down option : " + getData("Geographic location option");
//                return false;
//            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Geographic location option"))))
            {
                error = "Failed to click Geographic location drop down option : " + getData("Geographic location option");
                return false;
            }
            narrator.stepPassed("Has the risk been Geographic location option :" + getData("Geographic location option"));

            //Save button
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.EngagementsSaveButton()))
            {
                error = "Failed to wait for Save button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.EngagementsSaveButton()))
            {
                error = "Failed to click Save button";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
            String saved = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            PermitManagement_PageObjects.setRecord_Number(record[2]);
            String record_ = PermitManagement_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);

            narrator.stepPassedWithScreenShot("Successfully saved the record");

        }

        return true;
    }

    public boolean uploadSupportingDocuments()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.SupportingDocuments_Tab()))
        {
            error = "Failed to wait for Supporting Documents tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.SupportingDocuments_Tab()))
        {
            error = "Failed to click on Supporting Documents tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Supporting Documents tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.uploadLinkbox()))
        {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.uploadLinkbox()))
        {
            error = "Failed to click on 'Link box' link.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.LinkURL()))
        {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.LinkURL(), getData("Document Link")))
        {
            error = "Failed to enter Document Link :" + getData("Document Link");
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : " + getData("Document Link"));

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.urlTitle()))
        {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.urlTitle(), getData("Title")))
        {
            error = "Failed to enter Title :" + getData("Title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title"));

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.urlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.urlAddButton()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(PermitManagement_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.IssuesManagementSaveButton()))
        {
            error = "Failed to wait for Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.IssuesManagementSaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        PermitManagement_PageObjects.setRecord_Number(record[2]);
        String record_ = PermitManagement_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Successfully saved the record");

        return true;

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PermitManagemant;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PermitManagement_PageObjects.PermitManagement_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR2- Capture New Permit Main Scenario",
        createNewBrowserInstance = false
)
public class FR2_CaptureNewPermit_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR2_CaptureNewPermit_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureStakeholderAnalysis())
        {
            return narrator.testFailed("Capture New Permit Failed due To :" + error);
        }

        if (getData("Upload Document").equalsIgnoreCase("True"))
        {

            if (!UploadDocument())
            {
                return narrator.testFailed("Failed to upload document: " + error);
            }
        }

        return narrator.finalizeTest("Successfully Capture New Permit");
    }

    public boolean CaptureStakeholderAnalysis()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Permits_Tab()))
        {
            error = "Failed to wait for Permitstab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.Permits_Tab()))
        {
            error = "Failed to click on Permits tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Associated Stakeholder Groups tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.PermitsAdd()))
        {
            error = "Failed to wait for Permits Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.PermitsAdd()))
        {
            error = "Failed to click Members Permits Add Button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Permits Add Button");

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.PermitprocessFlow()))
        {
            error = "Failed to locate process flow";
            return false;
        }

        pause(5000);
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.PermitprocessFlow()))
        {
            error = "Failed to click on process flow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow");

        if (getData("Create from template checkbox").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.CreateFromTemplateCheckbox()))
            {
                error = "Failed to wait for Create from template checkbox";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.CreateFromTemplateCheckbox()))
            {
                error = "Failed to click on Create from template checkbox";
                return false;
            }

            narrator.stepPassedWithScreenShot("Create from template checkbox");

            //Select template from Permit Library
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.PermitLibraryDropdown()))
            {
                error = "Failed to wait for Select template from Permit Library dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.PermitLibraryDropdown()))
            {
                error = "Failed to click Select template from Permit Library dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Select template from Permit Library")))
            {
                error = "Failed to enter Select template from Permit Library :" + getData("Select template from Permit Library");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Select template from Permit Library"))))
            {
                error = "Failed to wait for Select template from Permit Library drop down option : " + getData("Select template from Permit Library");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Select template from Permit Library"))))
            {
                error = "Failed to click Select template from Permit Library drop down option : " + getData("Select template from Permit Library");
                return false;
            }
            narrator.stepPassed("Select template from Permit Library :" + getData("Select template from Permit Library"));

        } else
        {

            //New / approved permit
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.NewApprovedPermitDropdown()))
            {
                error = "Failed to wait for New / approved permit  dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.NewApprovedPermitDropdown()))
            {
                error = "Failed to click New / approved permit  dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for text box";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("New / approved permit"))))
            {
                error = "Failed to wait for New / approved permit drop down option : " + getData("New / approved permit");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("New / approved permit"))))
            {
                error = "Failed to click New / approved permit drop down option : " + getData("New / approved permit");
                return false;
            }

            narrator.stepPassed("New / approved permit :" + getData("New / approved permit"));

            //Title
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Title()))
            {
                error = "Failed to wait for Title text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.Title(), getData("Title")))
            {
                error = "Failed to enter Title :" + getData("Title");
                return false;
            }

            //Description
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Description()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.Description(), getData("Description")))
            {
                error = "Failed to enter Description :" + getData("Description");
                return false;
            }

            //Permit owner
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.PermitOwnerDropdown()))
            {
                error = "Failed to wait for Permit owner dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.PermitOwnerDropdown()))
            {
                error = "Failed to click Permit owner dropdown";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch(), getData("Permit owner")))
            {
                error = "Failed to enter Permit owner :" + getData("Permit owner");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }

            pause(7000);
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Permit owner"))))
            {
                error = "Failed to wait for Permit owner drop down option : " + getData("Permit owner");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Permit owner"))))
            {
                error = "Failed to click Permit owner drop down option : " + getData("Permit owner");
                return false;
            }
            narrator.stepPassed("Permit owner :" + getData("Permit owner"));

            //Permit owner – department
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.PermitOwnerDepartmentDropdown()))
            {
                error = "Failed to locate Permit owner – department Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.PermitOwnerDepartmentDropdown()))
            {
                error = "Failed to click on Permit owner department Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Permit owner department text box.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Business unit"))))
            {
                error = "Failed to wait for Permit owner department drop down option : " + getData("Business unit");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitexpandButton()))
            {
                error = "Failed to wait to expand Permit owner department";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitexpandButton()))
            {
                error = "Failed to expand Permit owner department";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 1"))))
            {
                error = "Failed to wait for Permit owner department:" + getData("Business unit 1");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 1"))))
            {
                error = "Failed to click Permit owner department Option drop down :" + getData("Business unit 1");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 2"))))
            {
                error = "Failed to wait for Permit owner department Option drop down :" + getData("Business unit 2");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 2"))))
            {
                error = "Failed to click  Permit owner department Option drop down :" + getData("Business unit 2");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Business unit option"))))
            {
                error = "Failed to wait for Permit owner department drop down option : " + getData("Business unit option");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Business unit option"))))
            {
                error = "Failed to click Permit owner department drop down option : " + getData("Business unit option");
                return false;
            }

            narrator.stepPassedWithScreenShot("Permit owner department option  :" + getData("Business unit option"));

            //Accountable person
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.AccountablePersonDropdown()))
            {
                error = "Failed to wait for Accountable person dropdown";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.AccountablePersonDropdown()))
            {
                error = "Failed to click Accountable person dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch(), getData("Accountable person")))
            {
                error = "Failed to enter Accountable person :" + getData("Accountable person");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }

            pause(6000);
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Accountable person"))))
            {
                error = "Failed to wait for Accountable person drop down option : " + getData("Accountable person");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Accountable person"))))
            {
                error = "Failed to click Accountable person drop down option : " + getData("Accountable person");
                return false;
            }
            narrator.stepPassed("Accountable person :" + getData("Accountable person"));

            //Permit nature
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.PermitNatureDropdown()))
            {
                error = "Failed to wait for Permit nature dropdown";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.PermitNatureDropdown()))
            {
                error = "Failed to click Permit nature dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Permit nature")))
            {
                error = "Failed to enter Permit nature :" + getData("Permit nature");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Permit nature"))))
            {
                error = "Failed to wait for Permit nature drop down option : " + getData("Permit nature");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Permit nature"))))
            {
                error = "Failed to click Permit nature drop down option : " + getData("Permit nature");
                return false;
            }
            narrator.stepPassed("Permit nature :" + getData("Permit nature"));

        
        String status = getData("Permit nature");

        if (status.equalsIgnoreCase("Other"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.OtherNatureDescription()))
            {
                error = "Failed to wait for Other nature description ";
                return false;

            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.OtherNatureDescription(), getData("Other nature description")))
            {
                error = "Failed to enter Other nature description :" + getData("Other nature description");
                return false;
            }
            narrator.stepPassed("Other nature description :" + getData("Other nature description"));

        }

//            //Does this permit expire
        if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.DoesThisPermitExpire()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.DoesThisPermitExpire()))
            {
                error = "Failed to click Permit nature dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Does this permit expire")))
            {
                error = "Failed to enter Does this permit expire :" + getData("Does this permit expire");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Does this permit expire"))))
            {
                error = "Failed to wait for Does this permit expire drop down option : " + getData("Does this permit expire");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Does this permit expire"))))
            {
                error = "Failed to click Does this permit expire drop down option : " + getData("Does this permit expire");
                return false;
            }
            narrator.stepPassed("Does this permit expire :" + getData("Does this permit expire"));

        }
//
//            // Expiration date
        if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ExpirationDate()))
        {
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.ExpirationDate(), startDate))
            {
                error = "Failed to enter Expiration date:" + startDate;
                return false;
            }
            narrator.stepPassed("Expiration date :" + startDate);

        }
        //Permit status
        if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.PermitStatusDropdown()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.PermitStatusDropdown()))
            {
                error = "Failed to click Permit status group dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Permit status")))
            {
                error = "Failed to enter Permit status :" + getData("Permit status");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }
        }
        if (getData("Permit status").equalsIgnoreCase("Approved"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text5(getData("Permit status"))))
            {
                error = "Failed to wait for Permit status drop down option : " + getData("Permit status");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.Text5(getData("Permit status"))))
            {
                error = "Failed to click Permit status drop down option : " + getData("Permit status");
                return false;
            }

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text4(getData("Permit status"))))
            {
                error = "Failed to wait for Permit status drop down option : " + getData("Permit status");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.Text4(getData("Permit status"))))
            {
                error = "Failed to click Permit status drop down option : " + getData("Permit status");
                return false;
            }

        }

        narrator.stepPassed("Permit status :" + getData("Permit status"));

        if (getData("Permit status").equalsIgnoreCase("Preparation (on track)") || getData("Permit status").equalsIgnoreCase("Pending Approval"))
        {

        } else
        {
            //Does this permit expire
            if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.DoesThisPermitExpire()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.DoesThisPermitExpire()))
                {
                    error = "Failed to click Permit nature dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
                {
                    error = "Failed to wait for text box";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Does this permit expire")))
                {
                    error = "Failed to enter Does this permit expire :" + getData("Does this permit expire");
                    return false;
                }

                if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
                {
                    error = "Failed to press enter";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Does this permit expire"))))
                {
                    error = "Failed to wait for Does this permit expire drop down option : " + getData("Does this permit expire");
                    return false;
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Does this permit expire"))))
                {
                    error = "Failed to click Does this permit expire drop down option : " + getData("Does this permit expire");
                    return false;
                }
                narrator.stepPassed("Does this permit expire :" + getData("Does this permit expire"));

            }

            // Expiration date
            if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ExpirationDate()))
            {
                if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.ExpirationDate(), startDate))
                {
                    error = "Failed to enter Expiration date:" + startDate;
                    return false;
                }
                narrator.stepPassed("Expiration date :" + startDate);

            }

        }

        //Legal stakeholder group
        if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.LegalStakeholderGroupDropdown()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.LegalStakeholderGroupDropdown()))
            {
                error = "Failed to click Legal stakeholder group dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Legal stakeholder group")))
            {
                error = "Failed to enter Legal stakeholder group :" + getData("Legal stakeholder group");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Legal stakeholder group"))))
            {
                error = "Failed to wait for Legal stakeholder group drop down option : " + getData("Legal stakeholder group");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Legal stakeholder group"))))
            {
                error = "Failed to click Legal stakeholder group drop down option : " + getData("Legal stakeholder group");
                return false;
            }
            narrator.stepPassed("Legal stakeholder group :" + getData("Legal stakeholder group"));
        }

        // Granting authority
        if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.GrantingAuthorityDropdown()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.GrantingAuthorityDropdown()))
            {
                error = "Failed to click Granting authority dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch(), getData("Granting authority")))
            {
                error = "Failed to enter Granting authority :" + getData("Granting authority");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Granting authority"))))
            {
                error = "Failed to wait for Granting authority drop down option : " + getData("Granting authority");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Granting authority"))))
            {
                error = "Failed to click Granting authority drop down option : " + getData("Granting authority");
                return false;
            }
            narrator.stepPassed("Granting authority :" + getData("Granting authority"));

        }

        //Inspecting authority
        if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.InspectingAuthorityDropdown()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.InspectingAuthorityDropdown()))
            {
                error = "Failed to click Inspecting authority dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch(), getData("Inspecting authority")))
            {
                error = "Failed to enter Inspecting authority :" + getData("Inspecting authority");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Inspecting authority"))))
            {
                error = "Failed to wait for Inspecting authority drop down option : " + getData("Inspecting authority");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Inspecting authority"))))
            {
                error = "Failed to click Inspecting authority drop down option : " + getData("Inspecting authority");
                return false;
            }
            narrator.stepPassed("Inspecting authority :" + getData("Inspecting authority"));

        }
        
        }//end of else

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.SavePermit()))
        {
            error = "Failed to wait for Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.SavePermit()))
        {
            error = "Failed to click Save button";
            return false;
        }

        pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");

        PermitManagement_PageObjects.setRecord_Number(record[2]);
        String record_ = PermitManagement_PageObjects.getRecord_Number();

        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Successfully saved the record");

        return true;
    }

    public boolean UploadDocument()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.SavePermitSoftCopy()))
        {
            error = "Failed to wait for Save permit soft copy'";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.SavePermitSoftCopy()))
        {
            error = "Failed to click on Save permit soft copy";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Save permit soft copy");

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.linkADoc_button()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.linkADoc_button()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.urlInput_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.urlInput_TextAreaxpath(), getData("Document Link")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.tile_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.tile_TextAreaxpath(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(PermitManagement_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.SaveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        PermitManagement_PageObjects.setRecord_Number(record[2]);
        String record_ = PermitManagement_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;

    }
}

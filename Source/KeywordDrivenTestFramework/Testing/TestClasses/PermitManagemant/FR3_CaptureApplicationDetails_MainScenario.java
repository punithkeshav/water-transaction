/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PermitManagemant;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PermitManagement_PageObjects.PermitManagement_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR3 - Capture Application Details Main Scenario",
        createNewBrowserInstance = false
)
public class FR3_CaptureApplicationDetails_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR3_CaptureApplicationDetails_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureStakeholderAnalysis())
        {
            return narrator.testFailed("Capture Application Details Failed due To :" + error);
        }
        if (getData("Execute Optional").equalsIgnoreCase("True"))
        {
            if (!uploadSupportingDocuments())
            {
                return narrator.testFailed("Failed to upload Supporting Documents: " + error);
            }
        }

        return narrator.finalizeTest("Successfully Capture Application Details");
    }

    public boolean CaptureStakeholderAnalysis()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ApplicationInformation_Tab()))
        {
            error = "Failed to wait for Application Information tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ApplicationInformation_Tab()))
        {
            error = "Failed to click on Application Information tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Application Informationtab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ApplicationInformationAdd()))
        {
            error = "Failed to wait for Application Information Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ApplicationInformationAdd()))
        {
            error = "Failed to click Application Information Add Button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Click Application Information Add Button");

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ApplicationInformationprocessFlow()))
        {
            error = "Failed to locate process flow";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ApplicationInformationprocessFlow()))
        {
            error = "Failed to click on process flow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow");

        //Application date
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ApplicationDate()))
        {
            error = "Failed to wait for Application date text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.ApplicationDate(), startDate))
        {
            error = "Failed to enter Application date :" + startDate;
            return false;
        }

        narrator.stepPassedWithScreenShot("Application date :" + startDate);

        if (getData("Application fee execute").equalsIgnoreCase("True"))
        {

            //Application fee
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ApplicationNotes()))
            {
                error = "Failed to wait for Application fee text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.ApplicationNotes(), "Test"))
            {
                error = "Failed to click Application fee text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.ApplicationFee(), 12 + ""))
            {
                error = "Failed to enter Application fee ";
                return false;
            }

            narrator.stepPassed("Application fee :" + 12);

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ApplicationNotes()))
            {
                error = "Failed to click Application notes text box";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.CurrencyDropDown()))
            {
                error = "Failed to wait for Currency drop down";
                return false;
            }
            pause(3000);
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.CurrencyDropDown()))
            {
                error = "Failed to click on Currency";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Currency")))
            {
                error = "Failed to enter Currency :" + getData("Currency");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Currency"))))
            {
                error = "Failed to wait for Currency drop down option : " + getData("Currency");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Currency"))))
            {
                error = "Failed to click Currency drop down option : " + getData("Currency");
                return false;
            }
            narrator.stepPassed("Currency :" + getData("Currency"));

        }

        //Application notes
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ApplicationNotes()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.ApplicationNotes(), getData("Application notes")))
        {

            error = "Failed to enter Application notes :" + getData("Application notes");
            return false;
        }

        narrator.stepPassed("Application notes :" + getData("Application notes"));

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ApplicationSaveButton()))
        {
            error = "Failed to wait for Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ApplicationSaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        PermitManagement_PageObjects.setRecord_Number(record[2]);
        String record_ = PermitManagement_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Successfully saved the record");
        return true;
    }

    public boolean uploadSupportingDocuments()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.uploadLinkbox()))
        {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.uploadLinkbox()))
        {
            error = "Failed to click on 'Link box' link.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.LinkURL()))
        {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.LinkURL(), getData("Document Link")))
        {
            error = "Failed to enter Document Link :" + getData("Document Link");
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : " + getData("Document Link"));

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.urlTitle()))
        {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.urlTitle(), getData("Title")))
        {
            error = "Failed to enter Title :" + getData("Title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title"));

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.urlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.urlAddButton()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(PermitManagement_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ApplicationSaveButton()))
        {
            error = "Failed to wait for Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ApplicationSaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        PermitManagement_PageObjects.setRecord_Number(record[2]);
        String record_ = PermitManagement_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Successfully saved the record");

        return true;

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PermitManagemant;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PermitManagement_PageObjects.PermitManagement_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR5- Capture Conditions and Commitments Main Scenario",
        createNewBrowserInstance = false
)
public class FR5_ConditionsAndCommitmentsRecord_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR5_ConditionsAndCommitmentsRecord_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureStakeholderAnalysis())
        {
            return narrator.testFailed("View Related Risks and Events Failed due To :" + error);
        }

        return narrator.finalizeTest("Successfully View Related Risks and Events");
    }

    public boolean CaptureStakeholderAnalysis()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ConditionsAndCommitments_Tab()))
        {
            error = "Failed to wait for  Conditions and Commitments tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ConditionsAndCommitments_Tab()))
        {
            error = "Failed to click on  Conditions and Commitments tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to  Conditions and Commitments tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ConditionsAndCommitmentsAddButton()))
        {
            error = "Failed to wait for Conditions and Commitments Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ConditionsAndCommitmentsAddButton()))
        {
            error = "Failed to click Conditions and Commitments Add Button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Clicked Conditions and Commitments");

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ConditionsAndCommitmentsprocessFlow()))
        {
            error = "Failed to locate process flow";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ConditionsAndCommitmentsprocessFlow()))
        {
            error = "Failed to click on process flow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow");

        //Type
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeDropDown()))
        {
            error = "Failed to wait for Type drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.TypeDropDown()))
        {
            error = "Failed to click Type drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Type text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Type")))

        {
            error = "Failed to enter Stakeholder group option :" + getData("Stakeholder Categorie");
            return false;
        }

         if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearchCheckBox(getData("Type"))))
        {
            error = "Failed to wait for Type option :" + getData("Type");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.TypeSearchCheckBox(getData("Type"))))
        {
            error = "Failed to select Type option :" + getData("Type");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.TypeDropDown()))
        {
            error = "Failed to click Type";
            return false;
        }

        narrator.stepPassedWithScreenShot("Type :" + getData("Type"));

        //Category
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.CategoryDropdown()))
        {
            error = "Failed to wait for Approver dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.CategoryDropdown()))
        {
            error = "Failed to click Approver dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Category text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Category")))
        {
            error = "Failed to enter Category:" + getData("Category");
            return false;
        }

      if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Category"))))
        {
            error = "Failed to wait for Category drop down option : " + getData("Category");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Category"))))
        {
            error = "Failed to click Category drop down option : " + getData("Category");
            return false;
        }
        narrator.stepPassed("Category :" + getData("Category"));

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ConditionsAndCommitmentsTitle()))
        {
            error = "Failed to wait for Title text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.ConditionsAndCommitmentsTitle(), getData("Title")))
        {
            error = "Failed to enter Title:" + getData("Title");
            return false;
        }

        if (getData("Category").equalsIgnoreCase("Administrative"))
        {
            //Nature
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.NatureDropdown()))
            {
                error = "Failed to wait for Nature dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.NatureDropdown()))
            {
                error = "Failed to click Nature dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Nature")))
            {
                error = "Failed to enter Nature :" + getData("Nature");
                return false;
            }

           if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Nature"))))
            {
                error = "Failed to wait for Nature drop down option : " + getData("Nature");
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Nature"))))
            {
                error = "Failed to click Nature drop down option : " + getData("Nature");
                return false;
            }
            narrator.stepPassed("Nature :" + getData("Nature"));

            if (getData("Nature").equalsIgnoreCase("For action"))
            {
                //Duration
                if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.DurationDropdown()))
                {
                    error = "Failed to wait for Duration dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.DurationDropdown()))
                {
                    error = "Failed to click Duration dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
                {
                    error = "Failed to wait for text box";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Duration")))
                {
                    error = "Failed to enter Duration :" + getData("Duration");
                    return false;
                }

               if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

                if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Duration"))))
                {
                    error = "Failed to wait for Duration drop down option : " + getData("v");
                    return false;
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Duration"))))
                {
                    error = "Failed to click Duration drop down option : " + getData("Duration");
                    return false;
                }
                narrator.stepPassed("Duration :" + getData("Duration"));

            }

            if (getData("Duration").equalsIgnoreCase("Cyclical"))
            {
                //Frequency
                if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.FrequencyDropdown()))
                {
                    error = "Failed to wait for Frequency dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.FrequencyDropdown()))
                {
                    error = "Failed to click Frequency dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
                {
                    error = "Failed to wait for text box";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Frequency")))
                {
                    error = "Failed to enter Frequency :" + getData("Frequency");
                    return false;
                }

              if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

                if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Frequency"))))
                {
                    error = "Failed to wait for Frequency drop down option : " + getData("Frequency");
                    return false;
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Frequency"))))
                {
                    error = "Failed to click Frequency drop down option : " + getData("Frequency");
                    return false;
                }
                narrator.stepPassed("Frequency :" + getData("Frequency"));

            }

        }

  
        //Owner
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.OwnerDropdown()))
        {
            error = "Failed to wait for Owner dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.OwnerDropdown()))
        {
            error = "Failed to click Owner dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Owner text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch(), getData("Owner")))
        {
            error = "Failed to enter Owner :" + getData("Owner");
            return false;
        }

       if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Owner"))))
        {
            error = "Failed to wait for Owner drop down option : " + getData("Owner");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Owner"))))
        {
            error = "Failed to click Ownerdrop down option : " + getData("Owner");
            return false;
        }
        narrator.stepPassed("Owner :" + getData("Owner"));

        //Approver
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ApproverDropdown()))
        {
            error = "Failed to wait for Approver dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ApproverDropdown()))
        {
            error = "Failed to click Approver dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Approver text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch(), getData("Approver")))
        {
            error = "Failed to enter Approver:" + getData("Approver");
            return false;
        }

      if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Approver"))))
        {
            error = "Failed to wait for Approver drop down option : " + getData("Approver");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Approver"))))
        {
            error = "Failed to click Approver drop down option : " + getData("Approver");
            return false;
        }
        narrator.stepPassed("Approver :" + getData("Approver"));

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.SaveConditionsAndCommitments()))
        {
            error = "Failed to wait for Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.SaveConditionsAndCommitments()))
        {
            error = "Failed to click Save button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        PermitManagement_PageObjects.setRecord_Number(record[2]);
        String record_ = PermitManagement_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Successfully saved the record");
        return true;
    }
}

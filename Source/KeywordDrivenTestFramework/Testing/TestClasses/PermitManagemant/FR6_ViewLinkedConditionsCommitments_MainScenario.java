/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PermitManagemant;

import KeywordDrivenTestFramework.Core.BaseClass;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PermitManagement_PageObjects.PermitManagement_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR6-View Linked Conditions and Commitments Main Scenario",
        createNewBrowserInstance = false
)

/**
 *
 * @author smabe
 */
public class FR6_ViewLinkedConditionsCommitments_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR6_ViewLinkedConditionsCommitments_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!ViewLinkedConditionsCommitments())
        {
            return narrator.testFailed("Failed To Capture Site Details Due To : " + error);
        }

        return narrator.finalizeTest("Successfully Capture Site Details  record");
    }

    public boolean ViewLinkedConditionsCommitments()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.LinkedConditionsAndCommitmentsTab()))
        {
            error = "Failed to wait for Linked Conditions and Commitments";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.LinkedConditionsAndCommitmentsTab()))
        {
            error = "Failed to click on Linked Conditions and Commitments";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Linked Conditions and Commitments");

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ConditionsAndCommitmentsFromRegister()))
        {
            error = "Failed to wait for Conditions and Commitments from Register";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ConditionsAndCommitmentsFromRegister()))
        {
            error = "Failed to click on Conditions and Commitments from Register";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Conditions and Commitments from Register");

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.RecordResults()))
        {
            error = "Failed to wait for record";
            return false;
        }

        String results = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.RecordResults());
        narrator.stepPassedWithScreenShot("Results :" + results);

        return true;

    }

}

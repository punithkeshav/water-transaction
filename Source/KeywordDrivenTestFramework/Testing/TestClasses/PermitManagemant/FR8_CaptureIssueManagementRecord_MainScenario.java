/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PermitManagemant;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PermitManagement_PageObjects.PermitManagement_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR8- Capture Issue Management record Main Scenario",
        createNewBrowserInstance = false
)
public class FR8_CaptureIssueManagementRecord_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR8_CaptureIssueManagementRecord_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureIssueManagementRecord())
        {
            return narrator.testFailed("Capture Issue Management record Failed due To :" + error);
        }
        if (getData("Execute Optional").equalsIgnoreCase("True"))
        {
            if (!uploadSupportingDocuments())
            {
                return narrator.testFailed("Failed to upload Supporting Documents: " + error);
            }
        }

        return narrator.finalizeTest("Successfully Capture Application Details");
    }

    public boolean CaptureIssueManagementRecord()
    {
        for (int i = 0; i < 4; i++)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.NextButton()))
            {
                error = "Failed to wait for next button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.NextButton()))
            {
                error = "Failed to click on next button";
                return false;
            }

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Issues_Tab()))
        {
            error = "Failed to wait for Issues tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.Issues_Tab()))
        {
            error = "Failed to click on Issues tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Issues tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.IssuesAdd()))
        {
            error = "Failed to wait for IssuesAdd Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.IssuesAdd()))
        {
            error = "Failed to click Issues Add Button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Click Issues Add Button");

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.IssuesprocessFlow()))
        {
            error = "Failed to locate process flow";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.IssuesprocessFlow()))
        {
            error = "Failed to click on process flow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow");

        //Issue category
//        if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.IssueCategoryDropDown()))
//        {
//            error = "Failed to wait for Issue category drop down";
//            return false;
//        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.IssueCategoryDropDown()))
        {
            error = "Failed to click on Issue category drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Issue category")))
        {
            error = "Failed to enter Issue category :" + getData("Issue category");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Issue category"))))
        {
            error = "Failed to wait for Issue category drop down option : " + getData("Issue category");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Issue category"))))
        {
            error = "Failed to click Issue category drop down option : " + getData("Issue category");
            return false;
        }
        narrator.stepPassed("Issue category:" + getData("Issue category"));

        //Issue Title
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.IssueTitle()))
        {
            error = "Failed to wait for Issue title text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.IssueTitle(), getData("Issue title")))
        {
            error = "Failed to enter Issue title :" + getData("Issue title");
            return false;
        }

        narrator.stepPassedWithScreenShot("Issue title:" + getData("Issue title"));

//        if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.HasTheRiskBeenControlledDropDown()))
//        {
//            error = "Failed to wait for Has the risk been controlled drop down";
//            return false;
//        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.HasTheRiskBeenControlledDropDown()))
        {
            error = "Failed to click on Has the risk been controlled drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Has the risk been controlled")))
        {
            error = "Failed to enter Currency :" + getData("Has the risk been controlled");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Has the risk been controlled"))))
        {
            error = "Failed to wait for Has the risk been controlled drop down option : " + getData("Has the risk been controlled");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Has the risk been controlled"))))
        {
            error = "Failed to click Has the risk been controlled drop down option : " + getData("Has the risk been controlled");
            return false;
        }
        narrator.stepPassed("Has the risk been controlled :" + getData("Has the risk been controlled"));

        if (getData("Has the risk been controlled").equalsIgnoreCase("No"))
        {
            //What was done in the interim
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.WhatWasDoneInTheInterim()))
            {
                error = "Failed to wait for What was done in the interim fee text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.WhatWasDoneInTheInterim(), getData("What was done in the interim")))
            {
                error = "Failed to enter What was done in the interim :" + getData("What was done in the interim");
                return false;
            }

            narrator.stepPassed("What was done in the interim :" + getData("What was done in the interim"));

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.WhatWasDoneToControlTheRisk()))
            {
                error = "Failed to wait for What was done to control the risk fee text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.WhatWasDoneToControlTheRisk(), getData("What was done to control the risk")))
            {
                error = "Failed to enter What was done to control the risk :" + getData("What was done to control the risk");
                return false;
            }

            narrator.stepPassed("What was done to control the risk :" + getData("What was done to control the risk"));

        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.IssuesManagementSaveButton()))
        {
            error = "Failed to wait for Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.IssuesManagementSaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        PermitManagement_PageObjects.setRecord_Number(record[2]);
        String record_ = PermitManagement_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Successfully saved the record");
        return true;
    }

    public boolean uploadSupportingDocuments()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.SupportingDocuments_Tab()))
        {
            error = "Failed to wait for Supporting Documents tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.SupportingDocuments_Tab()))
        {
            error = "Failed to click on Supporting Documents tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Supporting Documents tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.uploadLinkbox()))
        {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.uploadLinkbox()))
        {
            error = "Failed to click on 'Link box' link.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.LinkURL()))
        {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.LinkURL(), getData("Document Link")))
        {
            error = "Failed to enter Document Link :" + getData("Document Link");
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : " + getData("Document Link"));

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.urlTitle()))
        {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.urlTitle(), getData("Title")))
        {
            error = "Failed to enter Title :" + getData("Title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title"));

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.urlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.urlAddButton()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(PermitManagement_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.IssuesManagementSaveButton()))
        {
            error = "Failed to wait for Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.IssuesManagementSaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        PermitManagement_PageObjects.setRecord_Number(record[2]);
        String record_ = PermitManagement_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Successfully saved the record");

        return true;

    }
}

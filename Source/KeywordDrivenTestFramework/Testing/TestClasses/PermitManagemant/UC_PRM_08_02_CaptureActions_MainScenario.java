/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PermitManagemant;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PermitManagement_PageObjects.PermitManagement_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "UC PRM 08-02: Capture Actions Main Scenario",
        createNewBrowserInstance = false
)
public class UC_PRM_08_02_CaptureActions_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public UC_PRM_08_02_CaptureActions_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!CaptureActions())
        {
            return narrator.testFailed("Capture Actions Due To: " + error);
        }

        return narrator.finalizeTest("Successfully Capture Capture Actions");

    }

    public boolean CaptureActions()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Actions_Tab()))
        {
            error = "Failed to wait for Actions tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.Actions_Tab()))
        {
            error = "Failed to click on Actions tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Actions tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ActionsAdd()))
        {
            error = "Failed to wait for Actions Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ActionsAdd()))
        {
            error = "Failed to click Actions Add Button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Click Actions Add Button");

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ActionsprocessFlow()))
        {
            error = "Failed to locate process flow";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ActionsprocessFlow()))
        {
            error = "Failed to click on process flow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow");

        //Type of action
//        if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeOfActionDropDown()))
//        {
//            error = "Failed to wait for Type of action drop down";
//            return false;
//        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.TypeOfActionDropDown()))
        {
            error = "Failed to click on Type of action drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Type of action")))
        {
            error = "Failed to enter Type of action :" + getData("Type of action");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to wait for Type of action drop down option : " + getData("Type of action");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to click v drop down option : " + getData("Issue category");
            return false;
        }
        narrator.stepPassed("Type of action :" + getData("Type of action"));

        //Action description
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ActionDescription()))
        {
            error = "Failed to wait for Action descriptiontext box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.ActionDescription(), getData("Action description")))
        {
            error = "Failed to enter Action description:" + getData("Action description");
            return false;
        }

        narrator.stepPassedWithScreenShot("Action description " + getData("Action description"));

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.EntityDropdown()))
        {
            error = "Failed to locate Entity Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.EntityDropdown()))
        {
            error = "Failed to click on Entity Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Entity text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to wait for Business Unit :" + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Entity:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  EntityOption drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text5(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.Text5(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Entity option  :" + getData("Business unit option"));

        //Responsible person
//        if (SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ResponsiblePersonDropDown()))
//        {
//            error = "Failed to wait for Responsible person drop down";
//            return false;
//        }
//
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.ResponsiblePersonDropDown()))
        {
            error = "Failed to click on Responsible person drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.TypeSearch(), getData("Responsible person")))
        {
            error = "Failed to enter Responsible person :" + getData("Responsible person");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(PermitManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to wait for Responsible person drop down option : " + getData("Responsible person");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(PermitManagement_PageObjects.Text2(getData("Responsible person"))))
        {
            error = "Failed to click Responsible person drop down option : " + getData("Responsible person");
            return false;
        }
        narrator.stepPassed("Responsible person:" + getData("Responsible person"));

        pause(3000);

//            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.ActionDueDate()))
//            {
//                error = "Failed to wait for Action due date text box";
//                return false;
//            }
        if (!SeleniumDriverInstance.enterTextByXpath(PermitManagement_PageObjects.ActionDueDate(), startDate))
        {
            error = "Failed to enter Action due date :" + startDate;
            return false;
        }

        
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.IssuesManagementSaveButton2()))
        {
            error = "Failed to wait for Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(PermitManagement_PageObjects.IssuesManagementSaveButton2()))
        {
            error = "Failed to click Save button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        String saved = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(PermitManagement_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(PermitManagement_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        PermitManagement_PageObjects.setRecord_Number(record[2]);
        String record_ = PermitManagement_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Successfully saved the record");
        return true;
    }

}

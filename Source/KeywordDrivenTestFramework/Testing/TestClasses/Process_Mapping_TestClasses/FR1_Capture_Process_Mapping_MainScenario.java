/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Process_Mapping_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Process_Mapping_PageObjects.Process_Mapping_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author OMphahlele
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Process Mapping - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Process_Mapping_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Process_Mapping_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToProcessMapping())
        {
            return narrator.testFailed("Navigate To Process Mapping Failed due :" + error);
        }
        if (!CaptureProcessMapping())
        {
            return narrator.testFailed("Capture Process Mapping Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Process Mapping");
    }

    public boolean NavigateToProcessMapping()
    {
//        iFrame
//        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.iframeXpath()))
//        {
//            error = "Failed to switch to frame.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.switchToFrameByXpath(Process_Mapping_PageObjects.iframeXpath()))
//        {
//            error = "Failed to switch to frame.";
//            return false;
//        }
        
        pause(2000);
        //Search Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.search_Tab()))
        {
            error = "Failed to wait for Search Button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.search_Tab()))
        {
            error = "Failed to click on Search Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Search Button.");
        
        pause(4000);
        //Search field
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.search_field()))
        {
            error = "Failed to wait for Search field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.search_field(), getData("Search field")))
        {
            error = "Failed to click on Search field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Process Mapping.");
        
//         //Press Enter
//        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.search_field()))
//        {
//            error = "Failed to wait for 'Process Mapping' search.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.pressEnter_2(Process_Mapping_PageObjects.search_field()))
//        {
//            error = "Failed to press enter 'Process Mapping' search.";
//            return false;
//        }
//        pause(6000);
        
        //Navigate to Process Mapping
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.ProcessMappingTab()))
        {
            error = "Failed to wait for Process Mapping tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.ProcessMappingTab()))
        {
            error = "Failed to click on Process Mapping tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Process Mapping tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.processMapping_AddBtn()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.processMapping_AddBtn()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean CaptureProcessMapping()
    {
        pause(2000);
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.processMapping_processFlow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.processMapping_processFlow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");

        //Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.businessUnit_dropDown()))
        {
            error = "Failed to wait for 'Business Unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.businessUnit_dropDown()))
        {
            error = "Failed to click 'Business Unit' dropdown.";
            return false;
        }
        
        //Business Unit Expand 1
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.businessUnit_expand1()))
        {
            error = "Failed to wait for 'Business Unit' expand1.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.businessUnit_expand1()))
        {
            error = "Failed to click 'Business Unit' expand1.";
            return false;
        }
        
        //Business Unit Expand 2
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.businessUnit_expand2()))
        {
            error = "Failed to wait for 'Business Unit' expand2.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.businessUnit_expand2()))
        {
            error = "Failed to click 'Business Unit' expand2.";
            return false;
        }
        
        //Business Unit Expand 3
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.businessUnit_expand3()))
        {
            error = "Failed to wait for 'Business Unit' expand3.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.businessUnit_expand3()))
        {
            error = "Failed to click 'Business Unit' expand3.";
            return false;
        }
        
        //Business Unit Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.businessUnit_selection()))
        {
            error = "Failed to wait for 'Business Unit' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.businessUnit_selection()))
        {
            error = "Failed to click 'Business Unit' option.";
            return false;
        }
        
        pause(2000);
        //Close Business Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.closeBusinessUnit_dropDown()))
        {
            error = "Failed to wait for 'Business Unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.closeBusinessUnit_dropDown()))
        {
            error = "Failed to click 'Business Unit' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Business Unit.");

        pause(2000);
        //Business Area
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.business_area()))
        {
            error = "Failed to wait for 'Business Area' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.business_area(), getData("Business Area")))
        {
            error = "Failed to enter Business Area :" + getData("Business Area");
            return false;
        }
        narrator.stepPassedWithScreenShot("Sucessfuly entered Business Area :" + getData("Business Area"));
        
        //Process Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.process_description()))
        {
            error = "Failed to wait for 'Process Description' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.process_description(), getData("Process Description")))
        {
            error = "Failed to enter Process Description :" + getData("Process Description");
            return false;
        }
        narrator.stepPassedWithScreenShot("Sucessfuly entered Process Description :" + getData("Process Description"));
        
        //Process Owner
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.processOwner_Dd()))
        {
            error = "Failed to wait for 'Process Owner' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.processOwner_Dd()))
        {
            error = "Failed to click 'Process Owner' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.process_ownerSelection()))
        {
            error = "Failed to wait for 'Process Owner' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.process_ownerSelection()))
        {
            error = "Failed to click 'Process Owner' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a Process Owner.");
        
        //Reference
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.reference()))
        {
            error = "Failed to wait for 'Reference' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.reference(), getData("Reference")))
        {
            error = "Failed to enter Reference :" + getData("Reference");
            return false;
        }
        narrator.stepPassedWithScreenShot("Sucessfuly entered Reference :" + getData("Reference"));
        
        //Lifecycle
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.lifecycle_Dd()))
        {
            error = "Failed to wait for 'lifecycle_Dd' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.lifecycle_Dd()))
        {
            error = "Failed to click 'lifecycle_Dd' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.lifecycle_Selection()))
        {
            error = "Failed to wait for 'lifecycle' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.lifecycle_Selection()))
        {
            error = "Failed to click 'lifecycle' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'lifecycle' option.");
        pause(2000);
        
        //Link To Process
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.linkTo_process()))
        {
            error = "Failed to wait for 'Link To Process' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.linkTo_process(), getData("Link To Process")))
        {
            error = "Failed to enter Link To Process :" + getData("Link To Process");
            return false;
        }
        narrator.stepPassedWithScreenShot("Sucessfuly entered Link To Process :" + getData("Link To Process"));
        
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Process_Mapping_PageObjects.setRecord_Number(record[2]);
        String record_ = Process_Mapping_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}

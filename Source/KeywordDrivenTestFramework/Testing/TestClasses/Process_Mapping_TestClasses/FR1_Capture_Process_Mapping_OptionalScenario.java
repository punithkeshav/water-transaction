/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Process_Mapping_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Process_Mapping_PageObjects.Process_Mapping_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Omphile.Mphahlele
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Process Mapping - Optional Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Process_Mapping_OptionalScenario extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Process_Mapping_OptionalScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToProcessMapping())
        {
            return narrator.testFailed("Navigate To Process Mapping Failed due :" + error);
        }
        if (!UploadDocument())
        {
            return narrator.testFailed("Capture Process Mapping Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Process Mapping");
    }
    
    public boolean NavigateToProcessMapping()
    {
        //Navigate to 
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.ProcessMappingTab()))
        {
            error = "Failed to wait for Process Mapping tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.ProcessMappingTab()))
        {
            error = "Failed to click on Process Mapping tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Process Mapping tab.");

        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.search_Btn()))
        {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.search_Btn()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        pause(8000);
        
        //Record Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.record_Selection()))
        {
            error = "Failed to wait for 'Record'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.record_Selection()))
        {
            error = "Failed to click on 'Record'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Record'.");

        return true;
    }
    
    public boolean UploadDocument()
    {
        pause(4000);
        //Supporting Documents
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to wait for 'Supporting Documents' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.SupportingDocumentsTab()))
        {
            error = "Failed to click on 'Supporting Documents' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clciked the Supporting Documents tab.");
        
        //Link A Document
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.linkToADocument()))
        {
            error = "Failed to wait for 'Link A Document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.linkToADocument()))
        {
            error = "Failed to click on 'Link A Document' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clciked the 'Link A Document' button.");
        
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }
        
        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.UrlInput_TextArea()))
        {
            error = "Failed to wait for 'URL Input' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.UrlInput_TextArea(), getData("Document url")))
        {
            error = "Failed to click on 'URL Input' field";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.tile_TextArea()))
        {
            error = "Failed to wait for 'Title Input' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.tile_TextArea(), getData("Title")))
        {
            error = "Failed to click on 'Title Input' field.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.linkADoc_Add_button()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.linkADoc_Add_button()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Process_Mapping_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

       // SeleniumDriverInstance.pause(4000);

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Process_Mapping_PageObjects.setRecord_Number(record[2]);
        String record_ = Process_Mapping_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        
        return true; 
    }
}

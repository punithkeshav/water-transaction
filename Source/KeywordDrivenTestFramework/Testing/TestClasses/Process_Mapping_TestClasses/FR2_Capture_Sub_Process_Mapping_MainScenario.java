/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Process_Mapping_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Process_Mapping_PageObjects.Process_Mapping_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Omphile.Mphahlele
 */

@KeywordAnnotation(
        Keyword = "FR2-Capture Sub Process Mapping - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Sub_Process_Mapping_MainScenario extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_Sub_Process_Mapping_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToProcessMapping())
        {
            return narrator.testFailed("Navigate To Process Mapping Failed due :" + error);
        }
        if (!CaptureSubProcessMapping())
        {
            return narrator.testFailed("Capture Process Mapping Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Process Mapping");
    }
    
    public boolean NavigateToProcessMapping()
    {
        //Navigate to 
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.ProcessMappingTab()))
        {
            error = "Failed to wait for Process Mapping tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.ProcessMappingTab()))
        {
            error = "Failed to click on Process Mapping tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Process Mapping tab.");

        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.search_Btn()))
        {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.search_Btn()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        pause(8000);
        
        //Record Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.record_Selection()))
        {
            error = "Failed to wait for 'Record'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.record_Selection()))
        {
            error = "Failed to click on 'Record'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Record'.");

        return true;
    }
    
    public boolean CaptureSubProcessMapping()
    {
        pause(2000);
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.processMapping_processFlow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.processMapping_processFlow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");
        
        //Sub Process
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.subProcess_Tab()))
        {
            error = "Failed to wait for 'Sub Process' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.subProcess_Tab()))
        {
            error = "Failed to click on 'Sub Process' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Sub Process' tab.");
        
        //Add Btn
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.subProcess_addBtn()))
        {
            error = "Failed to wait for 'Sub Process' add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.subProcess_addBtn()))
        {
            error = "Failed to click on 'Sub Process' add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Sub Process' add button.");
        
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.subProcess_pFlow()))
        {
            error = "Failed to wait for 'Sub Process' process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.subProcess_pFlow()))
        {
            error = "Failed to click on 'Sub Process' process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Sub Process' process flow button.");
        
        //Sub Process field
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.subProcess_input()))
        {
            error = "Failed to wait for 'Sub Process' input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.subProcess_input(), getData("Sub Process")))
        {
            error = "Failed to enter value on 'Sub Process' field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Sub Process' input field.");
        
        //Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.subProcess_description()))
        {
            error = "Failed to wait for 'Sub Process Description' input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.subProcess_description(), getData("Description")))
        {
            error = "Failed to enter value on 'Description' field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Sub Process Description' input field.");
        
        //Reference
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.subProcess_ref()))
        {
            error = "Failed to wait for 'Sub Process Reference' input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.subProcess_ref(), getData("Reference")))
        {
            error = "Failed to enter value on 'Reference' field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Sub Process Reference' input field.");
        
        //Order
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.subProcess_order()))
        {
            error = "Failed to wait for 'Sub Process Order' input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.subProcess_order(), getData("Order")))
        {
            error = "Failed to enter value on 'Order' field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Sub Process Order' input field.");
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.subProcess_btnSave()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.subProcess_btnSave()))
        {
            error = "Failed to click button save";
            return false;
        }

       // SeleniumDriverInstance.pause(4000);

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Process_Mapping_PageObjects.setRecord_Number(record[2]);
        String record_ = Process_Mapping_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        
        return true;
    }
}

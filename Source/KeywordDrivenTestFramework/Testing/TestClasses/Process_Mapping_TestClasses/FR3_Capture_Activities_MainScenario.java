/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Process_Mapping_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Process_Mapping_PageObjects.Process_Mapping_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Omphile.Mphahlele
 */

@KeywordAnnotation(
        Keyword = "FR3-Capture Activities  - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_Activities_MainScenario extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Capture_Activities_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToProcessMapping())
        {
            return narrator.testFailed("Navigate To Process Mapping Failed due :" + error);
        }
        if (!Activities())
        {
            return narrator.testFailed("Capture Process Mapping Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Process Mapping");
    }
    
    public boolean NavigateToProcessMapping()
    {
        //Navigate to 
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.ProcessMappingTab()))
        {
            error = "Failed to wait for Process Mapping tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.ProcessMappingTab()))
        {
            error = "Failed to click on Process Mapping tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Process Mapping tab.");

        pause(2000);
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.search_Btn()))
        {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.search_Btn()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        pause(8000);
        
        //Record Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.record_Selection()))
        {
            error = "Failed to wait for 'Record'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.record_Selection()))
        {
            error = "Failed to click on 'Record'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Record'.");
        
        pause(2000);
        
        //Sub Process
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.subProcess_Tab()))
        {
            error = "Failed to wait for 'Sub Process' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.subProcess_Tab()))
        {
            error = "Failed to click on 'Sub Process' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Sub Process' tab.");
        
        //Select Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.subProcess_record()))
        {
            error = "Failed to wait for 'Sub Process' record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.subProcess_record()))
        {
            error = "Failed to click on 'Sub Process' record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Sub Process' record.");

        return true;
    }
    
    public boolean Activities()
    {
        pause(2000);
        //Activities
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.activities_tab()))
        {
            error = "Failed to wait for 'Activities' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.activities_tab()))
        {
            error = "Failed to click on 'Activities' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Activities' tab.");
        
        //Activities Add Buttons
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.activities_addBtn()))
        {
            error = "Failed to wait for 'Activities' add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.activities_addBtn()))
        {
            error = "Failed to click on 'Activities' add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Activities' add button.");
        
        pause(2000);
        
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.activities_pFlow()))
        {
            error = "Failed to wait for 'Process Flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.activities_pFlow()))
        {
            error = "Failed to click on 'Process Flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Process Flow' button.");
        
        //Activity field
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.activity_field()))
        {
            error = "Failed to wait for 'Activity' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.activity_field(), getData("Activity")))
        {
            error = "Failed to enter value on 'Activity' field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Activity' input field.");
        
        //Activity Description field
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.activityD_field()))
        {
            error = "Failed to wait for 'Activity Description' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.activityD_field(), getData("Activity Description")))
        {
            error = "Failed to enter value on 'Activity Description' field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Activity Description' input field.");
        
        //Activity Order field
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.activityOrder_field()))
        {
            error = "Failed to wait for 'Activity Order' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.activityOrder_field(), getData("Activity Order")))
        {
            error = "Failed to enter value on 'Activity Order' field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Activity Order' input field.");
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.activity_saveBtn()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.activity_saveBtn()))
        {
            error = "Failed to click button save";
            return false;
        }

       // SeleniumDriverInstance.pause(4000);

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Process_Mapping_PageObjects.setRecord_Number(record[2]);
        String record_ = Process_Mapping_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        
        return true; 
    }
    
}

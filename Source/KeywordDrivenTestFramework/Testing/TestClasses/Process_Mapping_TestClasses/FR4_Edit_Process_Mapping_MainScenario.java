/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Process_Mapping_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Process_Mapping_PageObjects.Process_Mapping_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Omphile.Mphahlele
 */

@KeywordAnnotation(
        Keyword = "FR4-Edit Process Mapping - Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_Edit_Process_Mapping_MainScenario extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_Edit_Process_Mapping_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToProcessMapping())
        {
            return narrator.testFailed("Navigate To Process Mapping Failed due :" + error);
        }
        if (!CaptureProcessMapping())
        {
            return narrator.testFailed("Capture Process Mapping Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Process Mapping");
    }
    
    public boolean NavigateToProcessMapping()
    {
        //Navigate to 
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.ProcessMappingTab()))
        {
            error = "Failed to wait for Process Mapping tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.ProcessMappingTab()))
        {
            error = "Failed to click on Process Mapping tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Process Mapping tab.");

        pause(2000);
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.search_Btn()))
        {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.search_Btn()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        pause(8000);
        
        //Record Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.record_Selection()))
        {
            error = "Failed to wait for 'Record'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.record_Selection()))
        {
            error = "Failed to click on 'Record'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Record'.");

        return true;
    }

    public boolean CaptureProcessMapping()
    {
        pause(2000);
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.processMapping_processFlow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.processMapping_processFlow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");
        
        //Process Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.process_description()))
        {
            error = "Failed to wait for 'Process Description' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.process_description(), getData("Process Description")))
        {
            error = "Failed to enter Process Description :" + getData("Process Description");
            return false;
        }
        narrator.stepPassedWithScreenShot("Sucessfuly entered Process Description :" + getData("Process Description"));
        
        //Reference
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.reference()))
        {
            error = "Failed to wait for 'Reference' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.reference(), getData("Reference")))
        {
            error = "Failed to enter Reference :" + getData("Reference");
            return false;
        }
        narrator.stepPassedWithScreenShot("Sucessfuly entered Reference :" + getData("Reference"));
        
        //Link To Process
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.linkTo_process()))
        {
            error = "Failed to wait for 'Link To Process' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Process_Mapping_PageObjects.linkTo_process(), getData("Link To Process")))
        {
            error = "Failed to enter Link To Process :" + getData("Link To Process");
            return false;
        }
        narrator.stepPassedWithScreenShot("Sucessfuly entered Link To Process :" + getData("Link To Process"));
        
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        pause(4000);
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Process_Mapping_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        Process_Mapping_PageObjects.setRecord_Number(record[2]);
        String record_ = Process_Mapping_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }
}

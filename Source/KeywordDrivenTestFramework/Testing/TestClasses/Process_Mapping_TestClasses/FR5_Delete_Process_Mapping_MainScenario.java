/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Process_Mapping_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Process_Mapping_PageObjects.Process_Mapping_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Omphile.Mphahlele
 */

@KeywordAnnotation(
        Keyword = "FR5-Delete Process Mapping - Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_Delete_Process_Mapping_MainScenario extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR5_Delete_Process_Mapping_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToProcessMapping())
        {
            return narrator.testFailed("Navigate To Process Mapping Failed due :" + error);
        }
        if (!DeleteRecord())
        {
            return narrator.testFailed("Capture Process Mapping Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured Process Mapping");
    }
    
    public boolean NavigateToProcessMapping()
    {
        //Navigate to 
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.ProcessMappingTab()))
        {
            error = "Failed to wait for Process Mapping tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.ProcessMappingTab()))
        {
            error = "Failed to click on Process Mapping tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Process Mapping tab.");

        pause(2000);
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.search_Btn()))
        {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.search_Btn()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        pause(8000);
        
        //Record Selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.record_Selection()))
        {
            error = "Failed to wait for 'Record'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.record_Selection()))
        {
            error = "Failed to click on 'Record'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected a 'Record'.");

        return true;
    }
    
     public boolean DeleteRecord()
    {
        pause(2000);
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.processMapping_processFlow()))
        {
            error = "Failed to wait for process flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.processMapping_processFlow()))
        {
            error = "Failed to click process flow button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow button.");
        
        //Delete Button
         if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.DeleteButton()))
        {
            error = "Failed to wait for delete button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.DeleteButton()))
        {
            error = "Failed click delete button";
            return false;
        }


        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }
        narrator.stepPassedWithScreenShot("");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Process_Mapping_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Process_Mapping_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);
        
        return true;
    }
}

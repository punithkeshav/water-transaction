/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TopEvent_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.AdHocNonComplianceIntervention_PageObjects.AdHocNonComplianceIntervention_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.TopEvent_PageObjects.TopEvent_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR2-View Bowtie Controls Main Scenario",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class FR2_ViewBowtieControls_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_ViewBowtieControls_MainScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!ViewBowtieControls())
        {
            return narrator.testFailed("Failed To View Bowtie Controls Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured View Bowtie Controls");
    }

    public boolean ViewBowtieControls()
    {
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.TopEventAndBTATab()))
        {
            error = "Failed to wait for Top Event and BTA Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.TopEventAndBTATab()))
        {
            error = "Failed to click Top Event and BTA Tab";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Top Event and BTA");

        //Search Option
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.ContainsTextBox()))
        {
            error = "Failed to wait for contains text box";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(TopEvent_PageObjects.ContainsTextBox(), getData("Record")))
        {
            error = "Failed to enter record number in contains";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + getData("Record"));

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.Record(getData("Record"))))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.Record(getData("Record"))))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + getData("Record"));

        pause(4000);
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.ProcessFlowButton()))
        {
            error = "Failed to wait for Process Flow Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.ProcessFlowButton()))
        {
            error = "Failed to click Process Flow Button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record : " + getData("Record"));

        return true;
    }

}

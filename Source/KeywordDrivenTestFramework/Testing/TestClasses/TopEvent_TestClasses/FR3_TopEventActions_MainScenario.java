/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TopEvent_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Engagements_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.AdHocNonComplianceIntervention_PageObjects.AdHocNonComplianceIntervention_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.TopEvent_PageObjects.TopEvent_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR3 Top Event Actions Main Scenario",
        createNewBrowserInstance = false
)

/**
 *
 * @author SMABE
 */
public class FR3_TopEventActions_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_TopEventActions_MainScenario()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!ViewBowtieControls())
        {
            return narrator.testFailed("Failed To View Bowtie Controls Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Captured View Bowtie Controls");
    }

    public boolean ViewBowtieControls()
    {
        //Top Event Actions
        pause(4000);
         if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.TopEventActionsAdd()))
        {
            error = "Failed to wait for Top Event Actions Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.TopEventActionsAdd()))
        {
            error = "Failed to click Top Event Actions Add";
            return false;
        }
           
        
//       if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.ActionsprocessFlow()))
//        {
//            error = "Failed to locate process flow";
//            return false;
//        }
//
//        pause(3000);
//        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.ActionsprocessFlow()))
//        {
//            error = "Failed to click on process flow";
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Process flow");
    
//         if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.TypeOfActionDropDown()))
//        {
//            error = "Failed to click on Type of action drop down";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(TopEvent_PageObjects.TypeSearch2(), getData("Type of action")))
        {
            error = "Failed to enter Type of action :" + getData("Type of action");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(TopEvent_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to wait for Type of action drop down option : " + getData("Type of action");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(TopEvent_PageObjects.Text2(getData("Type of action"))))
        {
            error = "Failed to click v drop down option : " + getData("Issue category");
            return false;
        }
        narrator.stepPassed("Type of action :" + getData("Type of action"));

        //Action description
//        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.ActionDescription()))
//        {
//            error = "Failed to wait for Action descriptiontext box";
//            return false;
//        }
//        if (!SeleniumDriverInstance.enterTextByXpath(TopEvent_PageObjects.ActionDescription(), getData("Action description")))
//        {
//            error = "Failed to enter Action description:" + getData("Action description");
//            return false;
//        }

        narrator.stepPassedWithScreenShot("Action description " + getData("Action description"));
         
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(TopEvent_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(TopEvent_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(TopEvent_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        TopEvent_PageObjects.setRecord_Number(record[2]);
        String record_ = TopEvent_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

        return true;
    }

}

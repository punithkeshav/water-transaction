/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TopEvent_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.TopEvent_PageObjects.TopEvent_PageObjects;


/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR4-Edit Top Event Main Scenario",
        createNewBrowserInstance = false
)
public class FR4_EditTopEvent_MainScenario extends BaseClass
{

    String error = "";

    public FR4_EditTopEvent_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!SearchRecord())
        {
            return narrator.testFailed("Failed to edit record  due to :" + error);
        }

        return narrator.finalizeTest("Successfully edited Water Monitoring record");
    }

    public boolean SearchRecord() throws InterruptedException
    {
        
        
         //Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.BusinessUnitDropDown()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.BusinessUnitDropDown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }
        

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(TopEvent_PageObjects.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to wait for Business Unit option :" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(TopEvent_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 3");
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 4"))))
//        {
//            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 4");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 4"))))
//        {
//            error = "Failed to click Entity Option drop down :" + getData("Business unit 4");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 5"))))
//        {
//            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 5");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 5"))))
//        {
//            error = "Failed to wait for Entity Option drop down:" + getData("Business unit 5");
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        
        

      
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(TopEvent_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(TopEvent_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(TopEvent_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        TopEvent_PageObjects.setRecord_Number(record[2]);
        String record_ = TopEvent_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);

       

        return true;
    }

}

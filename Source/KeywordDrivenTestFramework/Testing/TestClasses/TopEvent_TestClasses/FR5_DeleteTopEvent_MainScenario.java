/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.TopEvent_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.TopEvent_PageObjects.TopEvent_PageObjects;

/**
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR5-Delete Top Event Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_DeleteTopEvent_MainScenario extends BaseClass
{

    String error = "";

    public FR5_DeleteTopEvent_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!DeleteRecord())
        {
            return narrator.testFailed("Failed to delete record :" + error);
        }
        return narrator.finalizeTest("Successfully Deleted Record");
    }

    public boolean DeleteRecord() throws InterruptedException
    {
        
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.TopEventAndBTATab()))
        {
            error = "Failed to wait for Top Event and BTA Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.TopEventAndBTATab()))
        {
            error = "Failed to click Top Event and BTA Tab";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Top Event and BTA");
        
        
         if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.AddButton()))
        {
            error = "Failed to wait for add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.AddButton()))
        {
            error = "Failed to click add button";
            return false;
        }
        
         pause(4000);
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.ProcessFlowButton()))
        {
            error = "Failed to wait for Process Flow Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.ProcessFlowButton()))
        {
            error = "Failed to click Process Flow Button";
            return false;
        }
        
        
        
        

        //Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.BusinessUnitDropDown()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.BusinessUnitDropDown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(TopEvent_PageObjects.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to wait for Business Unit option :" + getData("Business unit option");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(TopEvent_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Entity Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 3");
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 4"))))
//        {
//            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 4");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 4"))))
//        {
//            error = "Failed to click Entity Option drop down :" + getData("Business unit 4");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 5"))))
//        {
//            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 5");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.businessUnitOption1(getData("Business unit 5"))))
//        {
//            error = "Failed to wait for Entity Option drop down:" + getData("Business unit 5");
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Top event description
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.TopEventDescription()))
        {
            error = "Failed to wait for Top event description text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(TopEvent_PageObjects.TopEventDescription(), getData("Top event description")))
        {
            error = "Failed to enter Top event description :" + getData("Top event description");
            return false;
        }

        //Risk owner
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.RiskOwnerDropDown()))
        {
            error = "Failed to wait for Risk owner dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.RiskOwnerDropDown()))
        {
            error = "Failed to click Risk owner dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Risk owner text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(TopEvent_PageObjects.TypeSearch2(), getData("Risk owner")))
        {
            error = "Failed to wait for Risk owner option :" + getData("Risk owner");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(TopEvent_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        
            if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.Text(getData("Risk owner"))))
        {
            error = "Failed to wait for Risk owner drop down option : " + getData("Risk owner");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.Text(getData("Risk owner"))))
        {
            error = "Failed to click Risk owner drop down option : " + getData("Risk owner");
            return false;
        }

        narrator.stepPassedWithScreenShot("Risk owner option  :" + getData("Risk owner"));
        
        
        
        
        
        

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.Button_Save()))
        {
            error = "Failed to wait for button save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";

        if (SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.recordSaved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(TopEvent_PageObjects.recordSaved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(TopEvent_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(TopEvent_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        TopEvent_PageObjects.setRecord_Number(record[2]);
        String record_ = TopEvent_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record number :" + acionRecord);
        
        
        

        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(TopEvent_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");
        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(TopEvent_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter contains to search";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.DeleteButton()))
        {
            error = "Failed to wait for delete button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.DeleteButton()))
        {
            error = "Failed click delete button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(TopEvent_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TopEvent_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);

        return true;
    }

}

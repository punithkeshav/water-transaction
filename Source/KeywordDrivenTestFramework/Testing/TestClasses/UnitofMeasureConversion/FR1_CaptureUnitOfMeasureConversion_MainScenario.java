/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.UnitofMeasureConversion;

import KeywordDrivenTestFramework.Testing.TestClasses.EnergyPhysicalProperties.*;
import KeywordDrivenTestFramework.Testing.TestClasses.CarbonPrice.*;
import KeywordDrivenTestFramework.Testing.TestClasses.EmissionLinking.*;
import KeywordDrivenTestFramework.Testing.TestClasses.BiodiversityMonitoring.*;
import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.EnergyPhysicalProperties.EnergyPhysicalProperties_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.UnitofMeasureConversion_PageObjects.UnitofMeasureConversion_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Unit of Measure Conversion Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_CaptureUnitOfMeasureConversion_MainScenario extends BaseClass
{

    String error = "";

    public FR1_CaptureUnitOfMeasureConversion_MainScenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!CaptureUnitOfMeasureConversion())
        {
            return narrator.testFailed("Failed To Capture Unit of Measure Conversion Due To : " + error);
        }

        return narrator.finalizeTest("Successfully Capture Unit of Measure Conversion");
    }

    public boolean CaptureUnitOfMeasureConversion()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.ECO2ManLabel()))
        {
            error = "Failed to wait for ECO2Man";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.ECO2ManLabel()))
        {
            error = "Failed to click on ECO2Man";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked ECO2ManLabel");

        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.MonitoringMaintence()))
        {
            pause(3000);
            if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.ECO2ManLabel()))
            {

                error = "Failed to wait for Monitoring Maintenance";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.MonitoringMaintence()))
        {
            error = "Failed to click on Monitoring Maintenance";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Monitoring Maintenance");

        //Carbon Price
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.UOMConversionLabel()))
        {
            error = "Failed to wait for UOM Conversion";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.UOMConversionLabel()))
        {
            error = "Failed to click on UOM Conversion";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked UOM Conversion");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.AddButton()))
        {
            error = "Failed to locate add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.AddButton()))
        {
            error = "Failed to click on add button";
            return false;
        }

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow in Add phase");

        //Conversion from
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.ConversionFromDropDown()))
        {
            error = "Failed to wait for Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.ConversionFromDropDown()))
        {
            error = "Failed to click on Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(UnitofMeasureConversion_PageObjects.TypeSearch2(), getData("Conversion from")))
        {
            error = "Failed to enter Conversion from :" + getData("Conversion from");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(UnitofMeasureConversion_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.Text2(getData("Conversion from"))))
        {
            error = "Failed to wait for Conversion from option:" + getData("Conversion from");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.Text2(getData("Conversion from"))))
        {
            error = "Failed to click on Conversion from option :" + getData("Conversion from");
            return false;
        }

        narrator.stepPassedWithScreenShot("Conversion from option  :" + getData("Conversion from"));

        //Conversion to
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.ConversionToDropDown()))
        {
            error = "Failed to wait for Emission source drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.ConversionToDropDown()))
        {
            error = "Failed to click Emission sourcedrop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(UnitofMeasureConversion_PageObjects.TypeSearch2(), getData("Conversion to")))

        {
            error = "Failed to enter Conversion to option :" + getData("Conversion to");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(UnitofMeasureConversion_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.Text2(getData("Conversion to"))))
        {
            error = "Failed to wait for Conversion to drop down option : " + getData("Emission source option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.Text2(getData("Conversion to"))))
        {
            error = "Failed to click Conversion to drop down option : " + getData("Conversion to");
            return false;
        }

        narrator.stepPassedWithScreenShot("Conversion to option  :" + getData("Conversion to"));

        //Unit of measure
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.UnitOfMeasureDropDown()))
        {
            error = "Failed to wait for Unit of measure drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.UnitOfMeasureDropDown()))
        {
            error = "Failed to click Unit of measure drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(UnitofMeasureConversion_PageObjects.TypeSearch2(), getData("Unit of measure")))

        {
            error = "Failed to enter Unit of measure option :" + getData("Unit of measure");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(UnitofMeasureConversion_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.Text2(getData("Unit of measure"))))
        {
            error = "Failed to wait for Unit of measure drop down option : " + getData("Unit of measure");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.Text2(getData("Unit of measure"))))
        {
            error = "Failed to click Unit of measure drop down option : " + getData("Unit of measure");
            return false;
        }

        narrator.stepPassedWithScreenShot("Unit of measure option  :" + getData("Unit of measure"));

        //Convert to
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.ConvertToDropDown()))
        {
            error = "Failed to wait for Convert to drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.ConvertToDropDown()))
        {
            error = "Failed to click Convert to drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(UnitofMeasureConversion_PageObjects.TypeSearch2(), getData("Convert to")))

        {
            error = "Failed to enter Convert to option :" + getData("Convert to");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(UnitofMeasureConversion_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.Text2(getData("Convert to"))))
        {
            error = "Failed to wait for Convert to drop down option : " + getData("Convert to");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.Text2(getData("Convert to"))))
        {
            error = "Failed to click Convert to drop down option : " + getData("");
            return false;
        }

        narrator.stepPassedWithScreenShot("Convert to option  :" + getData("Convert to"));

        //Conversion factor
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.ConversionFactor()))
        {
            error = "Failed to wait for Conversion factor text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(UnitofMeasureConversion_PageObjects.ConversionFactor(), "" + 26))

        {
            error = "Failed to enter Conversion factor :" + 26;
            return false;
        }

        //Unit of measure abbreviation
        if (!SeleniumDriverInstance.enterTextByXpath(UnitofMeasureConversion_PageObjects.UnitOfMeasureAbbreviation(), getData("Unit of measure abbreviation")))

        {
            error = "Failed to enter Unit of measure abbreviation : " + getData("Unit of measure abbreviation");
            return false;
        }

        //Convert to abbreviation
        if (!SeleniumDriverInstance.enterTextByXpath(UnitofMeasureConversion_PageObjects.ConvertToAbbreviation(), getData("Convert to abbreviation")))
        {
            error = "Failed to enter Convert to abbreviation : " + getData("Convert to abbreviation");
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.SaveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }
        //pause();

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(UnitofMeasureConversion_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(UnitofMeasureConversion_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(UnitofMeasureConversion_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        UnitofMeasureConversion_PageObjects.setRecord_Number(record[2]);
        String record_ = UnitofMeasureConversion_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;
    }

}

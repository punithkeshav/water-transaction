/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.UnitofMeasureConversion;

import KeywordDrivenTestFramework.Testing.TestClasses.EnergyPhysicalProperties.*;
import KeywordDrivenTestFramework.Testing.TestClasses.CarbonPrice.*;
import KeywordDrivenTestFramework.Testing.TestClasses.EmissionLinking.*;
import KeywordDrivenTestFramework.Testing.TestClasses.BiodiversityMonitoring.*;
import KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.UnitofMeasureConversion_PageObjects.UnitofMeasureConversion_PageObjects;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR2-Edit Unit of Measure Conversion",
        createNewBrowserInstance = false
)
public class FR2_EditUnitOfMeasureConversion_MainScenario extends BaseClass
{

    String error = "";

    public FR2_EditUnitOfMeasureConversion_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!EditEnergyPhysicalProperties())
        {
            return narrator.testFailed("Failed to Edit Unit of Measure Conversion due to :" + error);
        }

        return narrator.finalizeTest("Successfully edited Unit of Measure Conversion record");
    }

    public boolean EditEnergyPhysicalProperties() throws InterruptedException
    {

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(UnitofMeasureConversion_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");
        
        
        
         //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.SaveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }
        //pause();

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(UnitofMeasureConversion_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(UnitofMeasureConversion_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

//        if (!SeleniumDriverInstance.switchToTabOrWindow())
//        {
//            error = "Failed to switch tab.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.ButtonConfirm()))
//        {
//            error = "Failed to wait for the yes button";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.ButtonConfirm()))
//        {
//            error = "Failed to click the yes button";
//            return false;
//        }
//
//       if (!SeleniumDriverInstance.switchToTabOrWindow())
//        {
//            error = "Failed to switch tab.";
//            return false;
//        }
//       
//        if (!SeleniumDriverInstance.switchToFrameByXpath(UnitofMeasureConversion_PageObjects.iframe()))
//        {
//            error = "Failed to frame";
//            return false;
//        }

        narrator.stepPassedWithScreenShot("Successfully closed current module");

        //contains
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.ContainsTextBox()))
        {
            error = "Failed to wait for contains";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(UnitofMeasureConversion_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }

        pause(6000);
        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }

        //Conversion from
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.ConversionFromDropDown()))
        {
            error = "Failed to wait for Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.ConversionFromDropDown()))
        {
            error = "Failed to click on Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(UnitofMeasureConversion_PageObjects.TypeSearch2(), getData("Conversion from")))
        {
            error = "Failed to enter Conversion from :" + getData("Conversion from");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(UnitofMeasureConversion_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.Text2(getData("Conversion from"))))
        {
            error = "Failed to wait for Conversion from option:" + getData("Conversion from");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.Text2(getData("Conversion from"))))
        {
            error = "Failed to click on Conversion from option :" + getData("Conversion from");
            return false;
        }

        narrator.stepPassedWithScreenShot("Conversion from option  :" + getData("Conversion from"));

        //Conversion to
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.ConversionToDropDown()))
        {
            error = "Failed to wait for Emission source drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.ConversionToDropDown()))
        {
            error = "Failed to click Emission sourcedrop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for search text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(UnitofMeasureConversion_PageObjects.TypeSearch2(), getData("Conversion to")))

        {
            error = "Failed to enter Conversion to option :" + getData("Conversion to");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(UnitofMeasureConversion_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.Text2(getData("Conversion to"))))
        {
            error = "Failed to wait for Conversion to drop down option : " + getData("Emission source option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.Text2(getData("Conversion to"))))
        {
            error = "Failed to click Conversion to drop down option : " + getData("Conversion to");
            return false;
        }

        narrator.stepPassedWithScreenShot("Conversion to option  :" + getData("Conversion to"));

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.SaveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(UnitofMeasureConversion_PageObjects.SaveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

         saved = SeleniumDriverInstance.retrieveTextByXpath(UnitofMeasureConversion_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(UnitofMeasureConversion_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(UnitofMeasureConversion_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(UnitofMeasureConversion_PageObjects.getActionRecord());
        String[] record = acionRecord.split(" ");
        UnitofMeasureConversion_PageObjects.setRecord_Number(record[2]);
        String record_ = UnitofMeasureConversion_PageObjects.getRecord_Number();
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved");

        return true;
    }

}

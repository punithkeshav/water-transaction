/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.WaterTransaction_PageObjects;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR1_Capture_WaterTransaction_MainScenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_WaterTransaction_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;

    public FR1_Capture_WaterTransaction_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!CaptureWaterMonitoring())
        {
            return narrator.testFailed("Failed To Capture Water Monitoring Due To : " + error);
        }

        if (getData("Upload Document").equalsIgnoreCase("True"))
        {

            if (!UploadDocument())
            {
                return narrator.testFailed("Failed to upload document: " + error);
            }
        }
        return narrator.finalizeTest("Successfully added Water Transaction record");
    }

    public boolean CaptureWaterMonitoring()
    {

        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.EnvironmentalSustainabilitLabel()))
        {
            error = "Failed to wait for 'Environmental Sustainabilit'";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.EnvironmentalSustainabilitLabel()))
        {
            error = "Failed to click on 'Environmental Sustainabilit'";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Environmental Sustainabilit'");

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.WaterMonitoringTab()))
        {
            error = "Failed to wait for 'Water Monitoring'";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.WaterMonitoringTab()))
        {
            error = "Failed to click on 'Water Monitoring'";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Water Monitoring");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.addButtonXpath()))
        {
            error = "Failed to locate add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.addButtonXpath()))
        {
            error = "Failed to click on add button";
            return false;
        }

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Process flow in Add phase");

        //Business Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to locate Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.TypeSearch2(), getData("Business unit option")))
        {
            error = "Failed to wait for Business Unit :" + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(WaterTransaction_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        pause(4000);
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait to expand Business Unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to expand Business Unit";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit 1"))))
            {
                error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 2");
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit 1"))))
            {
                error = "Failed to click  Business Unit Option drop down :" + getData("Business unit 2");
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Business Unit Option drop down:" + getData("Business unit 5");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Measurement Type Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.measurementTypeDropdown()))
        {
            error = "Failed to locate Measurement Type Dropdown";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.measurementTypeDropdown()))
        {
            error = "Failed to click on Measurement Type Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.singleSelect(testData.getData("Measurement Type"))))
        {
            error = "Failed to wait for Measurement Type Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.singleSelect(testData.getData("Measurement Type"))))
        {
            error = "Failed to click Measurement Type Dropdown value";
            return false;
        }

        pause(3000);
        //Water Type dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.waterTypeDropdown()))
        {
            error = "Failed to wait for Water Type Dropdown";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.waterTypeDropdown()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.waterTypeDropdown()))
            {
                error = "Failed to click on Water Type Dropdown";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.waterTypeDropdownValue(getData("Water Type"))))
        {
            error = "Failed to locate Water Type Dropdown value chevron";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.waterTypeDropdownValue(getData("Water Type"))))
        {
            error = "Failed to click Water Type Dropdown value chevron";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.waterTypeDropdownChildValue(getData("Water Type value"))))
        {
            error = "Failed to locate Type of Initiative Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.waterTypeDropdownChildValue(getData("Water Type value"))))
        {
            error = "Failed to click Type of Initiative Dropdown value";
            return false;
        }

        if (getData("Measurement Type").equalsIgnoreCase("Quality"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.monitoringPointDropdown()))
            {
                error = "Failed to locate Monitoring point Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.monitoringPointDropdown()))
            {
                error = "Failed to click on Monitoring point Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Text(testData.getData("Monitoring point"))))
            {
                error = "Failed to wait for Monitoring point Dropdown Option :" + getData("Monitoring point");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Text(testData.getData("Monitoring point"))))
            {
                error = "Failed to click Monitoring point Dropdown option :" + getData("Monitoring point");
                return false;
            }

        }

        //Month Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.monthDropdown()))
        {
            error = "Failed to locate Month Dropdown";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.monthDropdown()))
        {
            error = "Failed to click on Month Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.singleSelect(testData.getData("Month"))))
        {
            error = "Failed to locate Month Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.singleSelect(testData.getData("Month"))))
        {
            error = "Failed to click on Month Dropdown value";
            return false;
        }

        //Year Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.yearDropdown()))
        {
            error = "Failed to locate Year Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.yearDropdown()))
        {
            error = "Failed to click on Year Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.singleSelect(testData.getData("Year"))))
        {
            error = "Failed to locate Year Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.singleSelect(testData.getData("Year"))))
        {
            error = "Failed to click on Year Dropdown value";
            return false;
        }

        //Sample taken by
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.SampleTakenByDropDown()))
        {
            error = "Failed to click Responsible person drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Responsible person text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.TypeSearch(), getData("Sample taken by")))
        {
            error = "Failed to enter Sample taken by :" + getData("Sample taken by");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(WaterTransaction_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Text(getData("Sample taken by"))))
        {
            error = "Failed to wait for Sample taken by drop down option : " + getData("Sample taken by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Text(getData("Sample taken by"))))
        {
            error = "Failed to click Sample taken by drop down option : " + getData("Sample taken by");
            return false;
        }

        //Team
//        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.TeamNameDropDown()))
//        {
//            error = "Failed to wait for Team Name drop down :";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.TeamNameDropDown()))
//        {
//            error = "Failed to click Team Name drop down :";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for Team Name text box";
//            return false;
//        }
//        if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.TypeSearch2(), getData("Team Name")))
//        {
//            error = "Failed to enter  Team Name :" + getData("Team Name");
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Text(getData("Team Name"))))
//        {
//            error = "Failed to wait for Team Name drop down option : " + getData("Team Name");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Text(getData("Team Name"))))
//        {
//            error = "Failed to click Team Name drop down option : " + getData("Team Name");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Team Name :" + getData("Team Name"));
        if (getData("Link to projects").equalsIgnoreCase("False") && getData("Link to site obligation").equalsIgnoreCase("False"))
        {

            //Save button
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.saveButton()))
            {
                error = "Failed to locate Save button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.saveButton()))
            {
                error = "Failed to click Save button";
                return false;
            }

            //Check if the record has been Saved
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }

            String saved = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }
            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.getActionRecord());
            narrator.stepPassed("Record number :" + acionRecord);
        }

        pause(3000);
        if (getData("Link to projects").equalsIgnoreCase("True"))
        {
            //Save button
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.saveButton()))
            {
                error = "Failed to locate Save button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.saveButton()))
            {
                error = "Failed to click Save button";
                return false;
            }

            //Check if the record has been Saved
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }

            String saved = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.getActionRecord());
            narrator.stepPassed("Record number :" + acionRecord);
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.LinkToProjects()))
            {
                error = "Failed To Wait For Link To Projects";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.LinkToProjects()))
            {
                error = "Failed To Click On Link To Projects";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.LinkToProjectsDropDown()))
            {
                error = "Failed To Wait For Link To Projects Drop Down";
                return false;
            }
            narrator.stepPassedWithScreenShot("Link To Projects clicked");
        }

        if (getData("Link to site obligation").equalsIgnoreCase("True"))
        {
            //Save button
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.saveButton()))
            {
                error = "Failed to locate Save button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.saveButton()))
            {
                error = "Failed to click Save button";
                return false;
            }

            //Check if the record has been Saved
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }

            String saved = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.getActionRecord());
            narrator.stepPassed("Record number :" + acionRecord);
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.LinkToSite()))
            {
                error = "Failed To Wait For Link To Site Obligation";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.LinkToSite()))
            {
                error = "Failed To Click On Link To Site Obligation Obligation";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.LinkToSiteDropDown()))
            {
                error = "Failed To Wait For Link To Site Obligation Drop Down ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Link To Site Obligation clicked");
        }
        return true;
    }

    public boolean UploadDocument() throws InterruptedException
    {

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.linkADoc_button()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(WaterTransaction_PageObjects.linkADoc_button()))
        {
            error = "Failed to scroll to 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.linkADoc_button()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.urlInput_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.urlInput_TextAreaxpath(), getData("Document Link")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.tile_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.tile_TextAreaxpath(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(WaterTransaction_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        if (getData("Enter Parameter Readings").equalsIgnoreCase("True"))
        {

            List<WebElement> elementList = SeleniumDriverInstance.Driver.findElements(By.xpath(WaterTransaction_PageObjects.ParameterTable()));
            int TableSize = elementList.size();
            int val = 1;
            boolean flag = false;

            while (val <= TableSize)
            {
                for (int i = 1; i < elementList.size(); i++)
                {
                    pause(3000);
                    if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.ParameterTableDeleteButton(i)))
                    {
                        error = "Failed to wait for the delete button";
                        return false;
                    }
                    pause(3000);

                    if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.ParameterTableDeleteButton(i)))
                    {
                        error = "Failed to click delete button";
                        return false;
                    }

                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch tab.";
                        return false;
                    }

                    if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.ButtonConfirm()))
                    {
                        error = "Failed to wait for the yes button";
                        return false;
                    }

                    if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.ButtonConfirm()))
                    {
                        error = "Failed to click the yes button";
                        return false;
                    }

                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch tab.";
                        return false;
                    }

                    if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.ButtonOK()))
                    {
                        error = "Failed to wait for the ok button";
                        return false;
                    }

                    if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.ButtonOK()))
                    {
                        error = "Failed to click the ok button";
                        return false;
                    }
                    if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.iframe()))
                    {
                        error = "Failed to switch to frame.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.switchToFrameByXpath(WaterTransaction_PageObjects.iframe()))
                    {
                        error = "Failed to switch to frame.";
                        return false;
                    }
                    val++;
                }
                flag = true;

            }

            if (flag)
            {
                //Save button
                if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.saveButton()))
                {
                    error = "Failed to locate Save button";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.saveButton()))
                {
                    error = "Failed to click Save button";
                    return false;
                }
            }

        }//end of if statement

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.getActionRecord());
        narrator.stepPassed("Record number :" + acionRecord);

        return true;

    }

}

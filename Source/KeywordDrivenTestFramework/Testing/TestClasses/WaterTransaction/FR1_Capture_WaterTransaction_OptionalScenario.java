/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.WaterTransaction_PageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "FR1_Capture_WaterTransaction_OptionalScenario",
    createNewBrowserInstance = false
)

public class FR1_Capture_WaterTransaction_OptionalScenario extends BaseClass
{

    String error = "";
    private String textbox;
   

  public FR1_Capture_WaterTransaction_OptionalScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!uploadSupportingDocuments())
        {
            return narrator.testFailed("Failed to upload Supporting Documents: " + error);
        }
        return narrator.finalizeTest("Successfully uploaded Supporting Documents");
    }

   
    public boolean uploadSupportingDocuments() throws InterruptedException
    {                   
                
        //Click upload document
        if(!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.uploadDocument())){
            error = "Failed to wait for Supporting documents 'Upload document' link.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.uploadDocument())){
            error = "Failed to click on Supporting documents 'Upload document' link.";
            return false;
        }
        

        
       //Upload hyperlink to Supporting documents
         //click link 
        if(!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.linkToADocument())){
            error = "Failed to wait for Supporting documents 'Link box' link.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.linkToADocument())){
            error = "Failed to click on Supporting documents 'Link box' link.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully clicked Supporting documents 'Upload Hyperlink box'.");
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
        //URL https
        if(!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.LinkURL())){
            error = "Failed to wait for 'URL value' field.";
            return false;
        }      
        if(!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.LinkURL(), getData("Document Link") )){
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");
        
        //Title
        if(!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.urlTitle())){
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.urlTitle(), getData("URL Title"))){
            error = "Failed to enter '" + getData("URL Title") + "' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("URL Title : '" + getData("URL Title") + "'.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.urlAddButton())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.urlAddButton())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("URL Title") + "' document using '" + getData("Document Link") + "' Link.");
        
        
          if (!SeleniumDriverInstance.switchToFrameByXpath(WaterTransaction_PageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
                        return false;

            
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
           
        
        //Save button
         if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }   
      
         if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        } 
        
        pause(15000);
        narrator.stepPassedWithScreenShot("Successfully saved uploaded documents");
         
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.waterTransactionRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
          
        return true;
        
        }

}

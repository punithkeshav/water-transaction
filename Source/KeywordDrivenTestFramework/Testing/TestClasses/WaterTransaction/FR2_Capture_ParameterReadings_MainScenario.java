/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.WaterTransaction_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR2_Capture_ParameterReadings_MainScenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_ParameterReadings_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;

    public FR2_Capture_ParameterReadings_MainScenario()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!addParameterReadingsRecord())
        {
            return narrator.testFailed("Failed to add Parameter Readings record: " + error);
        }

        return narrator.finalizeTest("Successfully added Parameter Readings record");
    }

    public boolean addParameterReadingsRecord() throws InterruptedException
    {
        //Parameter Readings panel
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.parameterReadingsPanel()))
        {
            error = "Failed to wait for Parameter Readings panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.parameterReadingsPanel()))
        {
            error = "Failed to expand Parameter Readings panel";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.parameterReadingsAddButton()))
        {
            error = "Failed to wait for Parameter Readings Add button";
            return false;
        }

        List<WebElement> elementList = SeleniumDriverInstance.Driver.findElements(By.xpath(WaterTransaction_PageObjects.MeasurementTextBox()));
        int TableSize = elementList.size();
        int val = 1;

        while (val < TableSize)
        {
            for (int i = 1; i <= elementList.size(); i++)
            {

                pause(3000);
                //Measurement
                if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.measurementTextbox(i)))
                {
                    error = "Failed to wait for Measurement field";
                    return false;
                }

                if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.measurementTextbox(i), testData.getData("Measurement")))
                {
                    error = "Failed to enter text in Measurement field";
                    return false;
                }

                narrator.stepPassed("Measurement :" + getData("Measurement"));

                //Unit
                if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.UnitDropdown(i)))
                {
                    error = "Failed to wait for Unit Dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.UnitDropdown(i)))
                {
                    error = "Failed to click on Unit Dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Text2(getData("Unit"))))
                {
                    error = "Failed to wait for Unit down option : " + getData("Unit");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Text2(getData("Unit"))))
                {
                    error = "Failed to click Unit drop down option : " + getData("Unit");
                    return false;
                }
                narrator.stepPassed("Unit :" + getData("Unit"));

                //Input/Output
                if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.InputOutDropdown(i)))
                {
                    error = "Failed to wait for input/output Dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.InputOutDropdown(i)))
                {
                    error = "Failed to click on input/output Dropdown";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Text2(getData("Input/Output"))))
                {
                    error = "Failed to wait for Input/Output down option : " + getData("Input/Output");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Text2(getData("Input/Output"))))
                {
                    error = "Failed to click Input/Output drop down option : " + getData("Input/Output");
                    return false;
                }

                narrator.stepPassed("Input/Output :" + getData("Input/Output"));

                //Sample date    
                if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.sampleDateXpath(i), startDate))
                {
                    error = "Failed to enter Sample date";
                    return false;
                }

                //Comments
                if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.commentsTextbox(i)))
                {
                    error = "Failed to wait for Comments field";
                    return false;
                }

                if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.commentsTextbox(i), testData.getData("Comments")))
                {
                    error = "Failed to enter text in Comments field";
                    return false;
                }
                narrator.stepPassed("Comments :" + getData("Comments"));

            }//emd of for loop
            val++;

        }//end of while loop 



        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.getActionRecord());
        narrator.stepPassed("Record number :" + acionRecord);

        return true;

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.WaterTransaction_PageObjects;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR3_Capture_QuantityMeasurements_MainScenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_QuantityMeasurements_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;

    public FR3_Capture_QuantityMeasurements_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!addMeasurementsRecord())
        {
            return narrator.testFailed("Failed To Capture Quantity Measurements Due To: " + error);
        }
        return narrator.finalizeTest("Successfully Captured Quantity Measurements");
    }

    public boolean addMeasurementsRecord() throws InterruptedException
    {
        //Waste Management Findings panel
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.waterTransactionFindingsPanel()))
        {
            error = "Failed to locate  Findings panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.waterTransactionFindingsPanel()))
        {
            error = "Failed to expand  Findings panel";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.findingsAddButton()))
        {
            error = "Failed to wait for Add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.findingsAddButton()))
        {
            error = "Failed to click on Add button.";
            return false;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        String total = "((//div[@id='control_68472F51-EB8F-4C07-8241-B0117B849667']//table//tbody)[2]//tr//i[@class='icon bin icon ten six grid-icon-delete'])";
        String xpath1 = "(//div[@id='control_68472F51-EB8F-4C07-8241-B0117B849667']//table//tbody)[2]//tr[2]//i[@class='icon bin icon ten six grid-icon-delete']";
        String xpath2 = "(//div[@id='control_68472F51-EB8F-4C07-8241-B0117B849667']//table//tbody)[2]//tr[3]//i[@class='icon bin icon ten six grid-icon-delete']";

        List<WebElement> Header = SeleniumDriverInstance.Driver.findElements(By.xpath(total));

        int val_1 = 1, val_2 = 0;
        val_2 = Header.size();

        while (val_1 <= val_2)
        {
            for (int i = 1; i < Header.size(); i++)
            {
                pause(5000);
                if (!SeleniumDriverInstance.waitForElementByXpath(total + "[" + i + "]"))
                {
                    error = "Failed to wait for delete button";
                    return false;
                }
                pause(5000);

                if (!SeleniumDriverInstance.clickElementbyXpath(total + "[" + i + "]"))
                {
                    error = "Failed to click delete button";
                    return false;
                }

                if (!SeleniumDriverInstance.switchToTabOrWindow())
                {
                    error = "Failed to switch tab.";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.ButtonConfirm()))
                {
                    error = "Failed to wait for the yes button";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.ButtonConfirm()))
                {
                    error = "Failed to click the yes button";
                    return false;
                }

                if (!SeleniumDriverInstance.switchToTabOrWindow())
                {
                    error = "Failed to switch tab.";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.ButtonOK()))
                {
                    error = "Failed to wait for the ok button";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.ButtonOK()))
                {
                    error = "Failed to click the ok button";
                    return false;
                }
                if (!SeleniumDriverInstance.switchToFrameByXpath(WaterTransaction_PageObjects.iframe()))
                {
                    error = "Failed to switch frame.";
                    return false;
                }
                val_1++;
            }//end of for loop 

        }//end of while loop

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
        //Waste Management Findings panel
        if (!SeleniumDriverInstance.scrollToElement(WaterTransaction_PageObjects.waterTransactionFindingsPanel()))
        {
            error = "Failed to locate  Findings panel";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.waterTransactionFindingsPanel()))
        {
            error = "Failed to locate  Findings panel";
            return false;
        }
        pause(4000);

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.waterTransactionFindingsPanel()))
        {
            error = "Failed to expand  Findings panel drop down";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.findingsAddButton()))
        {
            error = "Failed to wait for Add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.findingsAddButton()))
        {
            error = "Failed to click on Add button.";
            return false;
        }

        //Process Flow
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.findingsProcessflow()))
        {
            error = "Failed to wait for Process Flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.findingsProcessflow()))
        {
            error = "Failed to click Process Flow button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Processflow in Add phase");

        //Findings Description
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Findings_desc()))
        {
            error = "Failed to wait for Description textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.Findings_desc(), testData.getData("Description")))
        {
            error = "Failed to enter text into Description textarea.";
            return false;
        }

        //Findings Owner dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Findings_owner_dropdown()))
        {
            error = "Failed to wait for Findings Owner dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Findings_owner_dropdown()))
        {
            error = "Failed to click Findings Owner dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Measurement taken by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.TypeSearch(), getData("Findings Owner")))
        {
            error = "Failed to enter  Findings Owner :" + getData("Findings Owner");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(WaterTransaction_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Text2(getData("Findings Owner"))))
        {
            error = "Failed to wait for Findings Owner drop down option : " + getData("Findings Owner");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Text2(getData("Findings Owner"))))
        {
            error = "Failed to click Findings Owner drop down option : " + getData("Findings Owner");
            return false;
        }

        //Risk Source dropdown
//        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Findings_risksource_dropdown()))
//        {
//            error = "Failed to wait for Risk source dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Findings_risksource_dropdown()))
//        {
//            error = "Failed to click Risk source dropdown.";
//            return false;
//        }
//        //Risk Source select option 1
//        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.FindingsRiskSourceSelectAll()))
//        {
//            error = "Failed to wait  Risk Source dropdown options";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.FindingsRiskSourceSelectAll()))
//        {
//            error = "Failed to click Risk Source dropdown option";
//            return false;
//        }
//        //Risk Source select option 2
//        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Findings_risksource_select2(testData.getData("Risk Source 2"))))
//        {
//            error = "Failed to wait for '" + testData.getData("Risk Source 2") + "' in Risk Source dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Findings_risksource_select2(testData.getData("Risk Source 2"))))
//        {
//            error = "Failed to click '" + testData.getData("Risk Source 2") + "' from Risk Source dropdown.";
//            return false;
//        }
//        //Risk Source arrow
//        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Findings_risksource_arrow()))
//        {
//            error = "Failed to wait for Risk source arrow.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Findings_risksource_arrow()))
//        {
//            error = "Failed to click Risk source arrow.";
//            return false;
//        }

        //Findings Classification dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Findings_class_dropdown()))
        {
            error = "Failed to wait for Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Findings_class_dropdown()))
        {
            error = "Failed to click Findings Classification dropdown.";
            return false;
        }
        //Findings Classification select
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Findings_class_select(testData.getData("Findings Classification"))))
        {
            error = "Failed to wait for '" + testData.getData("Findings Classification") + "' in Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Findings_class_select(testData.getData("Findings Classification"))))
        {
            error = "Failed to click '" + testData.getData("Findings Classification") + "' from Findings Classification dropdown.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.SaveButtonFindings()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.SaveButtonFindings()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.getActionRecord());
        narrator.stepPassed("Record number :" + acionRecord);

        return true;
    }

}

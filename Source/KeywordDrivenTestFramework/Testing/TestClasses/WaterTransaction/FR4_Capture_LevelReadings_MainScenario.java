/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.WaterTransaction_PageObjects;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR4_Capture_LevelReadings_MainScenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_LevelReadings_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;

    public FR4_Capture_LevelReadings_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!addLevelReadingsRecord())
        {
            return narrator.testFailed("Failed to add Level Readings record: " + error);
        }
        return narrator.finalizeTest("Successfully added Level Readings record");
    }

    public boolean addLevelReadingsRecord() throws InterruptedException
    {
        //Level Readings panel
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.levelReadingsPanel()))
        {
            error = "Failed to wait for Level Readings panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.levelReadingsPanel()))
        {
            error = "Failed to expand Level Readings panel";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.waterLevelsAddButton()))
        {
            error = "Failed to wait for Water Levels Add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.waterLevelsAddButton()))
        {
            error = "Failed to click on Water Levels Add button";
            return false;
        }

        //Monitoring point Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.WLmonitoringPointDropdown()))
        {
            error = "Failed to wait for Monitoring point Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.WLmonitoringPointDropdown()))
        {
            error = "Failed to click on Monitoring point Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Text2(getData("Monitoring Point"))))
        {
            error = "Failed to wait for Monitoring Point Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Text2(getData("Monitoring Point"))))
        {
            error = "Failed to click Monitoring Point Dropdown value";
            return false;
        }

        //Level Input
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.levelInputTextbox()))
        {
            error = "Failed to wait for Level Input field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.levelInputTextbox(), testData.getData("Level Input")))
        {
            error = "Failed to enter text in Level Input field";
            return false;
        }

        //Level measure	
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.LevelMeasureDropdown()))
        {
            error = "Failed to wait for Level measure Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.LevelMeasureDropdown()))
        {
            error = "Failed to click on Level measure Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.singleSelect(getData("Level measure"))))
        {
            error = "Failed to wait for Level measure Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.singleSelect(getData("Level measure"))))
        {
            error = "Failed to click Level measure Dropdown value";
            return false;
        }
        //Input/output
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.InputOutputDropdown()))
        {
            error = "Failed to wait for Monitoring point Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.InputOutputDropdown()))
        {
            error = "Failed to click on Input/output Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.singleSelect(testData.getData("Input/output"))))
        {
            error = "Failed to wait for Input/output Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.singleSelect(testData.getData("Input/output"))))
        {
            error = "Failed to click Input/output Dropdown value";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.saveButton()))
        {
            error = "Failed to wait for Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.getActionRecord());
        narrator.stepPassed("Record number :" + acionRecord);

        return true;

    }

}

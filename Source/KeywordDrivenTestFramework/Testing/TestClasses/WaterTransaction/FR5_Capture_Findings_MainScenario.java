/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.WaterTransaction_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR5_Capture_Findings_MainScenario",
        createNewBrowserInstance = false
)

public class FR5_Capture_Findings_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR5_Capture_Findings_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!captureFindings())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Captured Findings");
    }

    public boolean captureFindings()
    {

        //Water Transaction Findings panel
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.waterTransactionFindingsPanel()))
        {
            error = "Failed to locate Water Transaction Findings panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.waterTransactionFindingsPanel()))
        {
            error = "Failed to expand Water Transaction Findings panel";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.findingsAddButton()))
        {
            error = "Failed to wait for Add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.findingsAddButton()))
        {
            error = "Failed to click on Add button.";
            return false;
        }

        List<WebElement> elementList = SeleniumDriverInstance.Driver.findElements(By.xpath(WaterTransaction_PageObjects.MeasurementTextBox()));
        int TableSize = elementList.size();
        int val = 1;
        if (TableSize > 0)
        {
            while (val < TableSize)
            {
                for (int i = 1; i <= elementList.size(); i++)
                {

                    pause(3000);
                    //Measurement
                    if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.measurementTextbox(i)))
                    {
                        error = "Failed to wait for Measurement field";
                        return false;
                    }

                    if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.measurementTextbox(i), getData("Measurement")))
                    {
                        error = "Failed to enter text in Measurement field";
                        return false;
                    }

                    narrator.stepPassed("Measurement :" + getData("Measurement"));

                    //Unit
                    if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.UnitDropdown(i)))
                    {
                        error = "Failed to wait for Unit Dropdown";
                        return false;
                    }

                    if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.UnitDropdown(i)))
                    {
                        error = "Failed to click on Unit Dropdown";
                        return false;
                    }

                    if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Text2(getData("Unit"))))
                    {
                        error = "Failed to wait for Unit down option : " + getData("Unit");
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Text2(getData("Unit"))))
                    {
                        error = "Failed to click Unit drop down option : " + getData("Unit");
                        return false;
                    }
                    narrator.stepPassed("Unit :" + getData("Unit"));

                    //Input/Output
                    if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.InputOutDropdown(i)))
                    {
                        error = "Failed to wait for input/output Dropdown";
                        return false;
                    }

                    if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.InputOutDropdown(i)))
                    {
                        error = "Failed to click on input/output Dropdown";
                        return false;
                    }

                    if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Text2(getData("Input/Output"))))
                    {
                        error = "Failed to wait for Input/Output down option : " + getData("Input/Output");
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Text2(getData("Input/Output"))))
                    {
                        error = "Failed to click Input/Output drop down option : " + getData("Input/Output");
                        return false;
                    }

                    narrator.stepPassed("Input/Output :" + getData("Input/Output"));

                    pause(2000);
                    //Sample date    
                    if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.sampleDateXpath(i), startDate))
                    {
                        error = "Failed to enter Sample date";
                        return false;
                    }

                    //Comments
                    if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.commentsTextbox(i)))
                    {
                        error = "Failed to wait for Comments field";
                        return false;
                    }

                    if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.commentsTextbox(i), testData.getData("Comments")))
                    {
                        error = "Failed to enter text in Comments field";
                        return false;
                    }
                    narrator.stepPassed("Comments :" + getData("Comments"));

                }//emd of for loop
                val++;

            }//end of while loop 

            //Add button
            if (!SeleniumDriverInstance.scrollToElement(WaterTransaction_PageObjects.findingsAddButton()))
            {
                error = "Failed to scroll to  Add button.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.findingsAddButton()))
            {
                error = "Failed to wait for Add button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.findingsAddButton()))
            {
                error = "Failed to click on Add button.";
                return false;
            }

        }

        //Process Flow
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.findingsProcessflow()))
        {
            error = "Failed to wait for Process Flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.findingsProcessflow()))
        {
            error = "Failed to click Process Flow button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Processflow in Add phase");

        //Findings Description
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Findings_desc()))
        {
            error = "Failed to wait for Description textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.Findings_desc(), testData.getData("Description")))
        {
            error = "Failed to enter text into Description textarea.";
            return false;
        }

        //Findings Owner dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Findings_owner_dropdown()))
        {
            error = "Failed to wait for Findings Owner dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Findings_owner_dropdown()))
        {
            error = "Failed to click Findings Owner dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Measurement taken by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.TypeSearch(), getData("Findings Owner")))
        {
            error = "Failed to enter  Findings Owner :" + getData("Findings Owner");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(WaterTransaction_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Text2(getData("Findings Owner"))))
        {
            error = "Failed to wait for Findings Owner drop down option : " + getData("Findings Owner");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Text2(getData("Findings Owner"))))
        {
            error = "Failed to click Findings Owner drop down option : " + getData("Findings Owner");
            return false;
        }

        //Risk Source dropdown
//        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Findings_risksource_dropdown()))
//        {
//            error = "Failed to wait for Risk source dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Findings_risksource_dropdown()))
//        {
//            error = "Failed to click Risk source dropdown.";
//            return false;
//        }
        //Risk Source select option 1
//        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.FindingsRiskSourceSelectAll()))
//        {
//            error = "Failed to wait  Risk Source dropdown options";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.FindingsRiskSourceSelectAll()))
//        {
//            error = "Failed to click Risk Source dropdown option";
//            return false;
//        }

        //Findings Description
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Findings_desc()))
        {
            error = "Failed to wait for Description textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.Findings_desc(), getData("Description")))
        {
            error = "Failed to enter text into Description textarea.";
            return false;
        }

        //Findings Classification dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Findings_class_dropdown()))
        {
            error = "Failed to wait for Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Findings_class_dropdown()))
        {
            error = "Failed to click Findings Classification dropdown.";
            return false;
        }
        //Findings Classification select
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Findings_class_select(testData.getData("Findings Classification"))))
        {
            error = "Failed to wait for '" + testData.getData("Findings Classification") + "' in Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Findings_class_select(testData.getData("Findings Classification"))))
        {
            error = "Failed to click '" + testData.getData("Findings Classification") + "' from Findings Classification dropdown.";
            return false;
        }

        //Save to continue
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.SaveButtonFindings()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.SaveButtonFindings()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.getActionRecord());
        narrator.stepPassed("Record number :" + acionRecord);
        return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.WaterTransaction;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteHomePageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.WaterTransaction_PageObjects;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR6_Edit_Water_Monitoring_MainScenario",
        createNewBrowserInstance = false
)
public class FR6_Edit_Water_Monitoring_MainScenario extends BaseClass
{

    String error = "";

    public FR6_Edit_Water_Monitoring_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!SearchRecord())
        {
            return narrator.testFailed("Failed to edit record  due to :" + error);
        }

        return narrator.finalizeTest("Successfully edited Water Monitoring record");
    }

    public boolean SearchRecord() throws InterruptedException
    {

        //Business Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to wait for Business Unit Dropdown";
            return false;
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to close current module";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.WaterMonitoringTab()))
        {
            error = "Failed To Wait For Waste Monitoring Tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.WaterMonitoringTab()))
        {
            error = "Failed Click Waste Monitoring Tab";
            return false;
        }
        pause(3000);

        narrator.stepPassedWithScreenShot("Waste Monitoring Tab Clicked Successfully");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.addButtonXpath()))
        {
            error = "Failed to wait for Waste Management add button";
            return false;
        }

        //Search Option
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.SearchOption()))
        {
            error = "Failed to wait for search options";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.SearchOption()))
        {
            error = "Failed to click search options";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(WaterTransaction_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter record number in contains";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.measurementsPanel()))
        {
            error = "Failed to locate Measurements panel";
            return false;
        }

        pause(3000);
        //Month Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.monthDropdown()))
        {
            error = "Failed to locate Month Dropdown";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.monthDropdown()))
        {
            error = "Failed to click on Month Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.singleSelect(testData.getData("Month"))))
        {
            error = "Failed to locate Month Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.singleSelect(testData.getData("Month"))))
        {
            error = "Failed to click on Month Dropdown value";
            return false;
        }

        narrator.stepPassedWithScreenShot("Month  :" + getData("Month"));

        //Year Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.yearDropdown()))
        {
            error = "Failed to locate Year Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.yearDropdown()))
        {
            error = "Failed to click on Year Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.singleSelect(testData.getData("Year"))))
        {
            error = "Failed to locate Year Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.singleSelect(testData.getData("Year"))))
        {
            error = "Failed to click on Year Dropdown value";
            return false;
        }

        narrator.stepPassedWithScreenShot("Year  :" + getData("Year"));

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        String total = "((//div[@id='control_68472F51-EB8F-4C07-8241-B0117B849667']//table//tbody)[2]//tr//i[@class='icon bin icon ten six grid-icon-delete'])";
        List<WebElement> Header = SeleniumDriverInstance.Driver.findElements(By.xpath(total));

        int val_1 = 1, val_2 = 0;
        val_2 = Header.size();

        if (val_2 > 0)
        {
            while (val_1 <= val_2)
            {
                for (int i = 1; i < Header.size(); i++)
                {
                    pause(5000);
                    if (!SeleniumDriverInstance.waitForElementByXpath(total + "[" + i + "]"))
                    {
                        error = "Failed to wait for delete button";
                        return false;
                    }
                    pause(5000);

                    if (!SeleniumDriverInstance.clickElementbyXpath(total + "[" + i + "]"))
                    {
                        error = "Failed to click delete button";
                        return false;
                    }

                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch tab.";
                        return false;
                    }

                    if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.ButtonConfirm()))
                    {
                        error = "Failed to wait for the yes button";
                        return false;
                    }

                    if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.ButtonConfirm()))
                    {
                        error = "Failed to click the yes button";
                        return false;
                    }

                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch tab.";
                        return false;
                    }

                    if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.ButtonOK()))
                    {
                        error = "Failed to wait for the ok button";
                        return false;
                    }

                    if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.ButtonOK()))
                    {
                        error = "Failed to click the ok button";
                        return false;
                    }
                    if (!SeleniumDriverInstance.switchToFrameByXpath(WaterTransaction_PageObjects.iframe()))
                    {
                        error = "Failed to switch frame.";
                        return false;
                    }
                    val_1++;
                }//end of for loop 

            }//end of while loop

            //Save button
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.saveButton()))
            {
                error = "Failed to locate Save button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(WaterTransaction_PageObjects.saveButton()))
            {
                error = "Failed to click Save button";
                return false;
            }
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WaterTransaction_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WaterTransaction_PageObjects.getActionRecord());
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Recored Edited");
        pause(5000);

        return true;
    }

}

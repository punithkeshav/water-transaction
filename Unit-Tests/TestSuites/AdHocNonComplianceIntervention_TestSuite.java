/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;
import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMABE
 */
public class AdHocNonComplianceIntervention_TestSuite extends BaseClass
{

    static TestMarshall instance;

    public AdHocNonComplianceIntervention_TestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.QA;

        //*******************************************
    }

    @Test
    public void AdHocNonComplianceInterventionRegression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\Regression Ad-Hoc Non-Compliance Intervention.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_AdHocNonComplianceIntervention_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR1-Capture the Ad-Hoc Non-Compliance Intervention Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void UC_NCI_01_02_Capture_AdHocNonComplianceIntervention_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\UC NCI 01-02-Capture  Ad-Hoc Non-Compliance Intervention Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_CaptureNonComplianceInterventionDetails_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR2–Capture Non-Compliance Intervention details Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void FR2_CaptureNonComplianceInterventionDetails_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR2–Capture Non-Compliance Intervention details Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void FR2_CaptureNonComplianceInterventionDetails_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR2–Capture Non-Compliance Intervention details Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void FR2_CaptureNonComplianceInterventionDetails_OptionalScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR2–Capture Non-Compliance Intervention details Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void UC_NCI_02_02_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\UC NCI 02-02 Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      @Test
    public void UC_NCI_02_03_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\UC NCI 02-03 Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void FR3_StopNoteFinalization_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR3–Stop Note Finalization Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void FR3_StopNoteFinalization_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR3–Stop Note Finalization Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void FR3_StopNoteFinalization_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR3–Stop Note Finalization Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
          @Test
    public void FR4_CaptureSection54Information_MainScenario_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR4–Capture Section 54 Information Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void FR4_CaptureSection54Information_MainScenario_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR4–Capture Section 54 Information Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void FR4_CaptureSection54Information_MainScenario_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR4–Capture Section 54 Information Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    
            @Test
    public void FR5_CaptureNonComplianceInterventionActions_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR5–Capture Non-Compliance Intervention Actions Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
             @Test
    public void FR6_EditAnAdHocNonComplianceIntervention_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR6-Edit an Ad-Hoc Non-Compliance Intervention  Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
              @Test
    public void FR7_EditANonComplianceIntervention_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Ad Hoc Non Compliance Intervention\\FR7-Edit a Non-Compliance Intervention Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMABE
 */
public class BiodiversityMonitoringTestSuite extends BaseClass
{

    static TestMarshall instance;

    public BiodiversityMonitoringTestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.QA;

        //*******************************************
    }

    @Test
    public void BiodiversityMonitoringRegression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Biodiversity Monitoring\\Biodiversity Monitoring Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR1- - Main_Scenario
    @Test
    public void FR1_Capture_BiodiversityMonitoring_MainSc1enario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Biodiversity Monitoring\\FR1- Capture Biodiversity Monitoring Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_BiodiversityMonitoring_OptionalScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Biodiversity Monitoring\\FR1- Capture Biodiversity Monitoring Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
       @Test
    public void FR1_Capture_BiodiversityMonitoring_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Biodiversity Monitoring\\FR1- Capture Biodiversity Monitoring Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void FR1_Capture_BiodiversityMonitoring_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Biodiversity Monitoring\\FR1- Capture Biodiversity Monitoring Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    

    @Test
    public void FR2_Capture_BiodiversityMeasurement_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Biodiversity Monitoring\\FR2-Capture Biodiversity Measurement - Flora Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

   
    @Test
    public void FR3_CaptureBiodiversityMeasurement_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Biodiversity Monitoring\\FR3-Capture Biodiversity Measurement Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR3_CaptureBiodiversityMeasurement_AlternateScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Biodiversity Monitoring\\FR3-Capture Biodiversity Measurement Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }


    @Test
    public void FR4_CaptureFindings_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Biodiversity Monitoring\\FR4-Capture Findings Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

   

    @Test
    public void FR5_EditBiodiversityMonitoring_OptionalScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Biodiversity Monitoring\\FR5-Edit Biodiversity Monitoring Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR5_DeleteBiodiversityMonitoring_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Biodiversity Monitoring\\FR5-Delete Biodiversity Monitoring  Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMABE
 */
public class CarbonPriceTestSuite extends BaseClass
{

    static TestMarshall instance;

    public CarbonPriceTestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.QA;

        //*******************************************
    }

    @Test
    public void CarbonPriceRegression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Carbon Price\\Carbon Price Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_CarbonPrice_MainSc1enario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Carbon Price\\FR1-Capture Carbon Price  Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_CarbonPrice_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Carbon Price\\FR2 - Edit Carbon Price Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

  

    @Test
    public void FR3_DeleteCarbonPrice_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Carbon Price\\FR3 - Delete Carbon Price   Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

   


}

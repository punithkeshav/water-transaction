/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author OMphahlele
 */
public class Commitments_Register_TestSuite extends BaseClass
{

    static TestMarshall instance;

    public Commitments_Register_TestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
       // TestMarshall.currentEnvironment = Enums.Environment.MTN;
        //*******************************************
    }

    @Test
    public void Commitments_Register_Regression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Commitments Register\\Commitments Register Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    
    }

//    @Test
//    public void FR1_Capture_Commitments_Register_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Commitments Register\\FR1-Capture Commitments Register - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
//
//    @Test
//    public void FR1_Capture_Commitments_Register_AlternateScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Commitments Register\\FR1-Capture Commitments Register - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
//
//    @Test
//    public void FR2_Capture_Commitments_and_Conditions_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Commitments Register\\FR2-Capture Commitments and Conditions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
//
//    @Test
//    public void FR3_View_Conditions_and_Commitments_from_a_Permit_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Commitments Register\\FR3-View Conditions and Commitments from a Permit - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
    
//    @Test
//    public void FR5_Edit_Commitments_Register_Record_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Commitments Register\\FR5-Edit Commitments Register record - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
    
//    @Test
//    public void FR6_Delete_Commitments_Register_Record_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Commitments Register\\FR6-Delete Commitments Register record - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMABE
 */
public class ComplianceAssessmentTestSuite extends BaseClass
{

    static TestMarshall instance;

    public ComplianceAssessmentTestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.QA;

        //*******************************************
    }

    @Test
    public void ComplianceAssessmentRegression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Compliance Assessment\\Compliance Assessment Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_ComplianceAssessmentRecord_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Compliance Assessment\\FR1- Capture Compliance Assessment record Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }


    @Test
    public void FR2_CaptureConditionsAndCommitmentsAssessmentRating_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Compliance Assessment\\FR2-Capture Conditions and Commitments Assessment Rating Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void FR2_CaptureConditionsAndCommitmentsAssessmentRating_AlternateScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Compliance Assessment\\FR2-Capture Conditions and Commitments Assessment Rating Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void FR2_CaptureConditionsAndCommitmentsAssessmentRating_OptionalScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Compliance Assessment\\FR2-Capture Conditions and Commitments Assessment Rating Optional Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void FR2_CaptureConditionsAndCommitmentsAssessmentRating_OptionalScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Compliance Assessment\\FR2-Capture Conditions and Commitments Assessment Rating Optional Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

   
    

}

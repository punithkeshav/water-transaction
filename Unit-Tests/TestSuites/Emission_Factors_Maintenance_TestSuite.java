/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author OMphahlele
 */
public class Emission_Factors_Maintenance_TestSuite extends BaseClass
{

    static TestMarshall instance;

    public Emission_Factors_Maintenance_TestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        //TestMarshall.currentEnvironment = Enums.Environment.MTN;
        //*******************************************
    }

    @Test
    public void Emission_Factors_Maintenance_Regression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Emission Factors Maintenance\\Emission Factors Maintenance Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    
    }

//    @Test
//    public void FR1_Capture_Emission_Factors_Maintenance_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Emission Factors Maintenance\\FR1- Capture Emission Factors Maintenance - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }

//    @Test
//    public void FR2_Capture_Emission_Factors_Maintenance__Values_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Emission Factors Maintenance\\FR2- Emission Factor Maintenance Values - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
//
//    @Test
//    public void FR3_Edit_Emission_Factor_Maintenance_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Emission Factors Maintenance\\FR3-Edit Emission Factor Maintenance - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
//
    @Test
    public void FR4_Delete_Emission_Factor_Maintenance_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Emission Factors Maintenance\\FR4-Delete Emission Factor Maintenance - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}

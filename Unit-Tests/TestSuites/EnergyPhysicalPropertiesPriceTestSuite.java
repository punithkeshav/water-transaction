/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMABE
 */
public class EnergyPhysicalPropertiesPriceTestSuite extends BaseClass
{

    static TestMarshall instance;

    public EnergyPhysicalPropertiesPriceTestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.QA;

        //*******************************************
    }

    @Test
    public void EnergyPhysicalPropertiesRegression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Energy Physical Properties\\Energy Physical Properties Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_EnergyPhysicalProperties_MainSc1enario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Energy Physical Properties\\FR1-Capture Energy Physical Properties.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_EditEnergyPhysicalProperties_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Energy Physical Properties\\FR2-Edit Energy Physical Properties.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

  

    @Test
    public void FR3_DeleteEnergyPhysicalProperties_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Energy Physical Properties\\FR3-Delete Energy Physical Properties.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

   


}

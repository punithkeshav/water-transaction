/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author OMphahlele
 */
public class Hazard_Categories_TestSuite extends BaseClass
{

    static TestMarshall instance;

    public Hazard_Categories_TestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
       // TestMarshall.currentEnvironment = Enums.Environment.MTN;
        //*******************************************
    }

    @Test
    public void Hazard_Categories_Regression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Hazard Categories\\Hazard Categories Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    
    }

//    @Test
//    public void FR1_Capture_Hazard_Categories_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Hazard Categories\\FR1-Capture Hazard Categories - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
//
//    @Test
//    public void FR2_Capture_Hazard_Categories_Level_2_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Hazard Categories\\FR2-Capture Hazard Categories Level 2- Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
//
//    @Test
//    public void FR2_Capture_Hazard_Categories_Level_2_AlternateScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Hazard Categories\\FR2-Capture Hazard Categories Level 2- Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
//
//    @Test
//    public void FR3_Capture_Hazard_Categories_Level_3_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Hazard Categories\\FR3-Capture Hazard Categories Level 3- Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
    
//    @Test
//    public void FR3_Capture_Hazard_Categories_Level_3_AlternateScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Hazard Categories\\FR3-Capture Hazard Categories Level 3- Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
    
//    @Test
//    public void FR4_Capture_Hazard_Categories_Level_4_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Hazard Categories\\FR4-Capture Hazard Categories Level 4- Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
//    
//    @Test
//    public void FR4_Capture_Hazard_Categories_Level_4_AlternateScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Hazard Categories\\FR4-Capture Hazard Categories Level 4- Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
    
//      @Test
//      public void FR5_Capture_Hazard_Categories_Level_5_AlternateScenario() throws FileNotFoundException
//      {
//          Narrator.logDebug("Isometrix - V4 - Test Pack");
//          instance = new TestMarshall("TestPacks\\Hazard Categories\\FR5-Capture Hazard Categories Level 5- Main Scenario.xlsx", Enums.BrowserType.Chrome);
//          instance.runKeywordDrivenTests();
//      }
    
//      @Test
//      public void FR5_Capture_Hazard_Categories_Level_5_AlternateScenario() throws FileNotFoundException
//      {
//          Narrator.logDebug("Isometrix - V4 - Test Pack");
//          instance = new TestMarshall("TestPacks\\Hazard Categories\\FR5-Capture Hazard Categories Level 5- Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
//          instance.runKeywordDrivenTests();
//      }
      
//      @Test
//      public void FR6_Capture_Obligations_MainScenario() throws FileNotFoundException
//      {
//          Narrator.logDebug("Isometrix - V4 - Test Pack");
//          instance = new TestMarshall("TestPacks\\Hazard Categories\\FR6-Capture Hazard Categories Obligations- Main Scenario.xlsx", Enums.BrowserType.Chrome);
//          instance.runKeywordDrivenTests();
//      }
      
//      @Test
//      public void FR7_Edit_Obligations_MainScenario() throws FileNotFoundException
//      {
//          Narrator.logDebug("Isometrix - V4 - Test Pack");
//          instance = new TestMarshall("TestPacks\\Hazard Categories\\FR7-Edit Hazard Categories - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//          instance.runKeywordDrivenTests();
//      }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author OMphahlele
 */
public class Monitoring_Points_TestSuite extends BaseClass
{

    static TestMarshall instance;

    public Monitoring_Points_TestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        //TestMarshall.currentEnvironment = Enums.Environment.MTN;
        //*******************************************
    }

    @Test
    public void Monitoring_Points_Regression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Monitoring Points\\Monitoring Points Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    
    }

    @Test
    public void FR1_Capture_Monitoring_Points_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Monitoring Points\\FR1-Capture Monitoring Points - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
//
    @Test
    public void FR1_Capture_Monitoring_Points_OptionalScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Monitoring Points\\FR1-Capture Monitoring Points - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_Capture_Points_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Monitoring Points\\FR2-Capture Points - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR3_Edit_Monitoring_Points_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Monitoring Points\\FR3-Edit Monitoring Points - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void FR3_Capture_Activities_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Monitoring Points\\FR4-Delete Monitoring Points - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author OMphahlele
 */
public class Process_Mapping_TestSuite extends BaseClass
{

    static TestMarshall instance;

    public Process_Mapping_TestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
//        TestMarshall.currentEnvironment = Enums.Environment.MTN;
        //*******************************************
    }

//    @Test
//    public void Emission_Factors_Maintenance_Regression() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Process Mapping\\Process Mapping Regression.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    
//    }

    @Test
    public void FR1_Capture_Process_Mapping_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Process Mapping\\FR1-Capture Process Mapping - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
//
//    @Test
//    public void FR2_Capture_Emission_Factors_Maintenance__Values_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Process Mapping\\\\FR1-Capture Process Mapping - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
//
//    @Test
//    public void FR2_Capture_Sub_Process_Mapping_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Process Mapping\\FR2-Capture Sub Process Mapping - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
//
//    @Test
//    public void FR2_Capture_Sub_Process_Mapping_OptionalScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Process Mapping\\FR2-Capture Sub Process Mapping - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
    
//    @Test
//    public void FR3_Capture_Activities_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Process Mapping\\FR3-Capture Activities  - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
    
//    @Test
//    public void FR3_Capture_Activities_OptionalScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Process Mapping\\FR3-Capture Activities  - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
    
//    @Test
//    public void FR4_Edit_Process_Mapping_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Process Mapping\\FR4-Edit Process Mapping - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
    
//    @Test
//    public void FR5_Delete_Process_Mapping_MainScenario() throws FileNotFoundException
//    {
//        Narrator.logDebug("Isometrix - V4 - Test Pack");
//        instance = new TestMarshall("TestPacks\\Process Mapping\\FR5-Delete Process Mapping - Main Scenario.xlsx", Enums.BrowserType.Chrome);
//        instance.runKeywordDrivenTests();
//    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMABE
 */
public class TopEvent_TestSuite extends BaseClass
{

    static TestMarshall instance;

    public TopEvent_TestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.QA;
    }

    @Test
    public void TopEventRegression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Top Event\\Top Event Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void TopEventRegression_QA() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Top Event\\Top Event Regression - QA.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_UpdateTopEvent_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Top Event\\FR1- Update Top Event Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void FR2_ViewBowtieControls_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Top Event\\FR2-View Bowtie Controls  Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR3_TopEventActions_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Top Event\\FR3- Top Event Actions Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void FR4_EditTopEvent_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Top Event\\FR4-Edit Top Event Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void FR5_DeleteTopEventMainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Test Pack");
        instance = new TestMarshall("TestPacks\\Top Event\\FR5-Delete Top Event Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMABE
 */
public class UnitOfMeasureConversionTestSuite extends BaseClass
{

    //Unit Of Measure Conversion TestSuite
    static TestMarshall instance;

    public UnitOfMeasureConversionTestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.QA;

        //*******************************************
    }

    @Test
    public void UnitOfMeasureConversionRegression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Unit Of Measure Conversion\\Unit Of Measure Conversion Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_UnitOfMeasureConversion_MainSc1enario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Unit Of Measure Conversion\\FR1-Capture Unit Of Measure Conversion.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void FR1_Capture_UnitOfMeasureConversion_Alternate() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix Test Pack");
        instance = new TestMarshall("TestPacks\\Unit Of Measure Conversion\\FR1-Capture Unit Of Measure Conversion Alternate.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_EditUnitOfMeasureConversion_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Unit Of Measure Conversion\\FR2-Edit Unit Of Measure Conversion.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

  

    @Test
    public void FR3_DeleteUnitOfMeasureConversion_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Unit Of Measure Conversion\\FR3-Delete Unit Of Measure Conversion.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

   


}

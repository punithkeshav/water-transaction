/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMABE
 */
public class WaterTransactionTestSuite  extends BaseClass
{
    static TestMarshall instance;

    public WaterTransactionTestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.QA;

        //*******************************************
    }
    
     @Test
    public void WaterTransaction_SAPROD() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\WaterTransaction_Regression SAPROD.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }    
    
     @Test
    public void WaterTransaction() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\WaterTransaction_Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }  
    
      @Test
    public void WaterTransaction_Ch05() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Water Transaction Regression Ch05.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
    //FR1- - Main_Scenario
    @Test
    public void FR1_Capture_Water_Monitoring__MainSc1enario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Water Transaction\\FR1_Capture_WaterTransaction_MainScenario.xlsx", Enums.BrowserType.Chrome);                          
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void FR1_Capture_Water_Monitoring_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Water Transaction\\FR1_Capture_WaterTransaction_AlternateScenario1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      @Test
    public void FR1_Capture_Water_Monitoring_AlternateScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Water Transaction\\FR1_Capture_WaterTransaction_AlternateScenario2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      @Test
    public void FR1_Capture_Water_Monitoring_AlternateScenario3() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Water Transaction\\FR1_Capture_WaterTransaction_AlternateScenario3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
      @Test
    public void FR1_Capture_Water_Monitoring_AlternateScenario4() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Water Transaction\\FR1_Capture_WaterTransaction_AlternateScenario4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      @Test
    public void FR1_Capture_Water_Monitoring_OptionalScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Water Transaction\\FR1_Capture_WaterTransaction_OptionalScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
 
      @Test
    public void FR2_Capture_Parameter_Readings_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Water Transaction\\FR2_Capture_ParameterReadings_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      @Test
    public void FR3_Capture_Quantity_Measurements_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Water Transaction\\FR3_Capture_QuantityMeasurements_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      @Test
    public void FR4_Capture_Level_Readings_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Water Transaction\\FR4_Capture_LevelReadings_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    
      @Test
    public void FR5_Capture_Findings_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Water Transaction\\FR5_Capture_Findings_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void FR6_Edit_Water_Monitoring_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Water Transaction\\FR6_Edit Water Monitoring_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
       @Test
    public void FR7_Delete_Water_Monitoring_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Water Transaction\\FR7_Delete Water Monitoring _MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
   
    
}
